-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 06 Okt 2018 pada 13.19
-- Versi server: 10.1.34-MariaDB
-- Versi PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_pohonbicara2`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `body`, `created_at`, `updated_at`) VALUES
(1, 'Copywriting Bebas', 'Dua opening untuk halaman depan.', ' ', '2018-06-03 23:48:34', '2018-06-03 23:48:34'),
(2, 'Teknologi', 'Empat highlights teknologi dan sejenisnya.', ' ', '2018-06-03 23:48:34', '2018-06-03 23:48:34'),
(3, 'Screenshots', 'Enam sample screenshots.', ' ', '2018-06-03 23:48:34', '2018-06-03 23:48:34');

-- --------------------------------------------------------

--
-- Struktur dari tabel `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `content_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `companies`
--

CREATE TABLE `companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reg` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `province` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timezone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `currency` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `companies`
--

INSERT INTO `companies` (`id`, `name`, `tax`, `reg`, `phone`, `fax`, `address1`, `address2`, `city`, `province`, `zip`, `country`, `logo`, `timezone`, `currency`, `created_at`, `updated_at`) VALUES
(1, 'PT Bintang Kelabu', '02.130.0367009', '180/7009/SIUP-K/341.23/2015', '022650982001', '022650982001', 'Jl. Siliwangi 24', '', 'Bandung', 'Jawa Barat', '40102', 'Indonesia', '', 'GMT+7', 'Rp.', '2018-06-03 23:48:34', '2018-06-03 23:48:34');

-- --------------------------------------------------------

--
-- Struktur dari tabel `contents`
--

CREATE TABLE `contents` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `quote` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `offline_writer_id` int(11) NOT NULL,
  `offline_writer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `contents`
--

INSERT INTO `contents` (`id`, `name`, `description`, `quote`, `body`, `user_id`, `offline_writer_id`, `offline_writer`, `category_id`, `topic_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Pantau Terus Kondisi Tumbuhan Setiap Harinya', ' ', ' ', '<p>Kamu bisa melihat berbagai perubahan pada Tanaman&nbsp; seperti suhu, kadar air, ph tanah menggunakan IoT Smart Urban Farming</p>', 1, 0, ' ', 1, 0, 'PUBLISHED', '2018-06-03 23:48:34', '2018-06-04 00:44:44'),
(2, 'Pantau kondisi tumbuhan dengan IoT Smart Farming', ' ', ' ', 'IoT smart Farming akan memudahkan pemantauan Tanaman dengan berbagai sensor yang di tanam seperti sensor suhu, kelembapan, cahaya dan ph tanah', 1, 0, ' ', 1, 0, 'PUBLISHED', '2018-06-03 23:48:34', '2018-06-09 00:49:22'),
(3, 'Monitoring', ' ', ' ', '<p>Monitoring Tanaman Dimana Saja</p>', 1, 0, ' ', 2, 0, 'PUBLISHED', '2018-06-03 23:48:34', '2018-09-29 10:04:34'),
(4, 'Sensor Suhu', ' ', ' ', '<p>Pantau Apakah Suhu Tanaman Sudah Sesuai</p>', 1, 0, ' ', 2, 0, 'PUBLISHED', '2018-06-03 23:48:34', '2018-09-29 10:09:44'),
(5, 'Sensor Kelempaban', ' ', ' ', '<p>Cek Kelembapan yang sesuai dengan&nbsp;Tanaman&nbsp;</p>', 1, 0, ' ', 2, 0, 'PUBLISHED', '2018-06-03 23:48:34', '2018-09-29 10:10:21'),
(6, 'Sensor Ph Tanah', ' ', ' ', '<p>Jaga ph Tanah yang sesuai dengan&nbsp;Tanaman</p>', 1, 0, ' ', 2, 0, 'PUBLISHED', '2018-06-03 23:48:34', '2018-09-29 10:11:10'),
(7, 'Semua Informasi ada dalam Cloud', ' ', ' ', '<p>Cloud Arsitektur</p>', 1, 0, ' ', 3, 0, 'PUBLISHED', '2018-06-03 23:48:34', '2018-09-29 10:12:42'),
(8, 'Alat Sensor Terhubung dengan Antares', ' ', ' ', '<p>Antares Support</p>', 1, 0, ' ', 3, 0, 'PUBLISHED', '2018-06-03 23:48:34', '2018-09-29 10:13:57'),
(9, 'Controller Arduino Untuk Tanaman', ' ', ' ', '<p>Arduino Ready</p>', 1, 0, ' ', 3, 0, 'PUBLISHED', '2018-06-03 23:48:34', '2018-09-29 10:16:22'),
(10, 'Website Administrator', ' ', ' ', '<p>Layanan Website Untuk Mengelola Tanaman</p>', 1, 0, ' ', 3, 0, 'PUBLISHED', '2018-06-03 23:48:34', '2018-09-29 10:18:41'),
(11, 'Smartphone Monitoring', ' ', ' ', '<p>Monitor Tanaman dengan Smartphone</p>', 1, 0, ' ', 3, 0, 'PUBLISHED', '2018-06-03 23:48:34', '2018-09-29 10:20:01'),
(12, 'Pantau Tanaman Dengan Mudah', ' ', ' ', '<p>Informasi Suhu, Kelembapan dan PH Tanah Tersedia</p>', 1, 0, ' ', 3, 0, 'PUBLISHED', '2018-06-03 23:48:34', '2018-09-29 10:22:10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelola_tanamen`
--

CREATE TABLE `kelola_tanamen` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `jenis_tanaman` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama_tanaman` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitute` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `perangkat_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `foto_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `foto_file_size` int(11) DEFAULT NULL,
  `foto_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `foto_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `kelola_tanamen`
--

INSERT INTO `kelola_tanamen` (`id`, `user_id`, `jenis_tanaman`, `nama_tanaman`, `longitute`, `latitude`, `perangkat_id`, `created_at`, `updated_at`, `foto_file_name`, `foto_file_size`, `foto_content_type`, `foto_updated_at`) VALUES
(3, 15, 'Tanaman Jagung', 'Jagung Blonde', '13245221', '122345564', 1, '2018-06-17 01:27:07', '2018-06-19 08:30:00', 'fe28115510799.5602318b4edd4.jpg', 169193, 'image/jpeg', '2018-06-17 03:17:55'),
(6, 15, 'Matahari', 'Matahari Abadi', '13245221', '1187266653', 2, '2018-06-17 03:15:53', '2018-06-19 08:30:42', 'phc-plant.png', 60689, 'image/png', '2018-06-17 03:15:53');

-- --------------------------------------------------------

--
-- Struktur dari tabel `logsystems`
--

CREATE TABLE `logsystems` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ipaddress` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2015_11_30_191713_create_user_meta_table', 1),
(4, '2015_11_30_215038_create_roles_table', 1),
(5, '2015_11_30_215040_create_role_user_table', 1),
(6, '2015_12_04_155900_create_teams_table', 1),
(7, '2015_12_04_155900_create_teams_users_table', 1),
(8, '2016_11_24_161729_create_companies_table', 1),
(9, '2016_12_07_181951_create_logsystems_table', 1),
(10, '2017_02_13_151043_add_avatar_fields_to_user_meta_table', 1),
(11, '2017_02_16_054402_create_topics_table', 1),
(12, '2017_02_16_054514_create_categories_table', 1),
(13, '2017_02_16_054817_create_offlinewriters_table', 1),
(14, '2017_02_16_055021_create_comments_table', 1),
(15, '2017_02_16_055809_create_contents_table', 1),
(16, '2018_06_08_065918_create_data_sensors_table', 2),
(17, '2018_06_08_083058_create_pendaftarans_table', 3),
(18, '2018_06_10_080732_create_kekola_lahans_table', 4),
(19, '2018_06_10_081755_create_kekola_tanamen_table', 5),
(20, '2018_06_10_082104_create_kelola_tanamen_table', 5),
(21, '2018_06_17_080032_add_foto_fields_to_kelola_tanamen_table', 6),
(22, '2018_06_17_134950_create_sensors_table', 7),
(23, '2018_06_19_083511_create_perangkats_table', 8);

-- --------------------------------------------------------

--
-- Struktur dari tabel `offlinewriters`
--

CREATE TABLE `offlinewriters` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendaftarans`
--

CREATE TABLE `pendaftarans` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `pendaftarans`
--

INSERT INTO `pendaftarans` (`id`, `name`, `email`, `alamat`, `phone`, `gender`, `status`, `created_at`, `updated_at`) VALUES
(3, 'Reza Gunawan', 'rezagunawan999@gmail.com', 'Jalan Cibiru Kota Bandung, Jalan Cibiru Kota Bandung', '0988898989', 'Laki-laki', 'Terverifikasi', '2018-06-08 21:58:50', '2018-06-10 00:05:21'),
(4, 'ulfah choirun nissa', 'ulfahcn@gmail.com', 'Jalan Cibiru Kota Bandung,', '0988898989', 'Perempuan', 'Terverifikasi', '2018-06-10 00:08:56', '2018-06-10 00:35:42'),
(5, 'rendy', 'rendy@gmail.com', 'Jalan Cibiru Kota Bandung,', '0988898989', 'Laki-laki', 'Terverifikasi', '2018-06-11 00:45:49', '2018-06-11 00:48:00'),
(6, 'Taufik Albari', 'taufik@gmail.com', 'Jl. Cimahi', '09877667899', 'Laki - Laki', '0', '2018-09-25 21:08:31', '2018-09-25 21:09:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `perangkats`
--

CREATE TABLE `perangkats` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `platform` text COLLATE utf8_unicode_ci NOT NULL,
  `origin` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tanaman_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `perangkats`
--

INSERT INTO `perangkats` (`id`, `user_id`, `platform`, `origin`, `content`, `keterangan`, `created_at`, `updated_at`, `tanaman_id`) VALUES
(1, 15, 'https://platform.antares.id:8443/~/antares-cse/antares-id/GardenCanggih/Temperature/la', '40459b3e0e98f6c3:65773a486d3e1c28', 'application/json', 'Tanaman Jagung', '2018-06-19 02:10:42', '2018-06-19 02:23:11', 3),
(2, 16, 'https://platform.antares.id:8443/~/antares-cse/antares-id/GardenCanggih/Temperature', 'abe3bc416aba1fa8:98a65739c6c3f58f', 'application/json', 'Bunga Matahari', '2018-06-19 10:22:19', '2018-06-19 10:22:21', 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `name`, `label`, `permissions`) VALUES
(1, 'member', 'Member', NULL),
(2, 'admin', 'Admin', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 2),
(2, 2),
(15, 1),
(9, 1),
(16, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sensors`
--

CREATE TABLE `sensors` (
  `id` int(10) UNSIGNED NOT NULL,
  `tanggal` date NOT NULL,
  `waktu` datetime NOT NULL,
  `ph_tanah` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kelembapan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `suhu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cahaya` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tanaman_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `perangkat_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `sensors`
--

INSERT INTO `sensors` (`id`, `tanggal`, `waktu`, `ph_tanah`, `kelembapan`, `suhu`, `cahaya`, `tanaman_id`, `user_id`, `created_at`, `updated_at`, `perangkat_id`) VALUES
(1, '2018-06-17', '2018-03-31 11:29:16', '7', '20', '21', '30', 3, 15, '2018-06-17 07:05:47', '2018-06-17 07:12:39', 1),
(2, '2018-06-17', '2018-01-31 11:29:16', '3', '30', '19', '10', 3, 15, '2018-06-17 07:07:19', '2018-06-17 07:07:19', 1),
(3, '2018-06-17', '2018-03-31 11:29:16', '5', '23', '26', '20', 3, 15, '2018-06-17 07:07:19', '2018-06-17 07:07:19', 1),
(4, '2018-06-17', '2018-05-31 11:29:16', '8', '15', '22', '26', 6, 15, '2018-06-17 07:07:19', '2018-06-17 07:07:19', 1),
(5, '2018-06-17', '2018-03-31 11:29:16', '6', '25', '18', '19', 6, 15, '2018-06-17 07:07:19', '2018-06-17 07:07:19', 1),
(6, '2018-06-17', '2018-03-31 11:29:16', '7', '26', '28', '25', 6, 15, NULL, NULL, 1),
(7, '2018-06-19', '2018-06-19 00:00:00', '7', '20', '27', '50', 2, 16, '2018-06-18 17:00:00', '2018-06-18 17:00:00', 1),
(8, '2018-06-21', '2018-06-21 14:23:17', '6', '23', '28', '46', 3, 15, '2018-06-21 07:23:35', '2018-06-21 07:23:38', 1),
(9, '2018-09-05', '2018-09-05 11:39:21', '4', '81.00%', '25.00', '17', 3, 15, '2018-09-29 18:09:55', '2018-09-29 18:09:55', 1),
(10, '2018-09-05', '2018-09-05 11:39:21', '4', '81.00%', '25.00', '17', 3, 15, '2018-09-29 18:10:34', '2018-09-29 18:10:34', 1),
(11, '2018-09-05', '2018-09-05 11:39:21', '4', '81.00%', '25.00', '17', 3, 15, '2018-09-29 18:14:24', '2018-09-29 18:14:24', 1),
(12, '2018-09-05', '2018-09-05 11:39:21', '4', '81.00%', '25.00', '17', 3, 15, '2018-09-29 18:24:37', '2018-09-29 18:24:37', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `teams`
--

CREATE TABLE `teams` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `team_user`
--

CREATE TABLE `team_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `team_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `topics`
--

CREATE TABLE `topics` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@admin.com', '$2y$10$k9rDCRI4x8/lh182q5iUFeYzDSEW2tYKyym0WAjO858lEj3NHFbkC', 'azz2y4HbpowKtCOjhhISAifpsiVALfQ6jhaszTqmf24JUfKvDsnrgJsUDwbr', '2018-06-03 23:48:08', '2018-06-03 23:48:08'),
(2, 'Member', 'member@member.com', '$2y$10$ODs9uYrjOodEkoRJEYJzpOs2Bb5.wuuEZMWfSR24PjI/4D76hiW56', NULL, '2018-06-03 23:48:34', '2018-06-03 23:48:34'),
(9, 'Reza Gunawan', 'rezagunawan999@gmail.com', '$2y$10$.jqzIBkUWGR5mDP31/sSZOt61gmhRlmhCt8FqjlXI4uwyLvtrr1WS', 'oGsrh8vhnnntTnPjOvF46c2X043koxbaSiXpFjUI2J72MEPa726Xm5h9iWKy', '2018-06-10 00:05:21', '2018-06-10 00:05:21'),
(15, 'ulfah choirun nissa', 'ulfahcn@gmail.com', '$2y$10$ioktGEow.DUKtxGpEpcS5O5Xuyyk.8DbseZM0RvjyD23x0NDrGcdy', 'xkgfVXo5k1uuGRBJC21KPD2w4o1QbTzl4eG0XHMfj84FeC1r0CIHdqGhaurQ', '2018-06-10 00:35:40', '2018-06-10 00:35:40'),
(16, 'rendy', 'rendy@gmail.com', '$2y$10$/ujfUB2Jwi841oiHLknGleypPkURTlxWOLY5DXOoonG6cE0MUtLa.', 'kCFCoiohvr1ROEytvUhand1WqafU5UBga17UkJQsw9cszoex0BTe2TLV5AQT', '2018-06-11 00:47:58', '2018-06-11 00:47:58');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_meta`
--

CREATE TABLE `user_meta` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `activation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marketing` tinyint(1) NOT NULL DEFAULT '0',
  `terms_and_cond` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `avatar_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_file_size` int(11) DEFAULT NULL,
  `avatar_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `user_meta`
--

INSERT INTO `user_meta` (`id`, `user_id`, `phone`, `is_active`, `activation_token`, `marketing`, `terms_and_cond`, `created_at`, `updated_at`, `avatar_file_name`, `avatar_file_size`, `avatar_content_type`, `avatar_updated_at`) VALUES
(1, 1, NULL, 1, NULL, 0, 1, '2018-06-03 23:48:34', '2018-09-29 10:30:34', 'LaravelLogo.png', 54365, 'image/png', '2018-09-29 10:30:34'),
(3, 2, NULL, 1, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 15, '0988898989', 1, NULL, 0, 1, '2018-06-10 00:35:42', '2018-06-10 00:35:42', NULL, NULL, NULL, NULL),
(14, 16, '0988898989', 1, NULL, 0, 1, '2018-06-11 00:47:59', '2018-06-11 00:47:59', NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `contents`
--
ALTER TABLE `contents`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kelola_tanamen`
--
ALTER TABLE `kelola_tanamen`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `logsystems`
--
ALTER TABLE `logsystems`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `offlinewriters`
--
ALTER TABLE `offlinewriters`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indeks untuk tabel `pendaftarans`
--
ALTER TABLE `pendaftarans`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `perangkats`
--
ALTER TABLE `perangkats`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `role_user`
--
ALTER TABLE `role_user`
  ADD KEY `role_user_user_id_index` (`user_id`),
  ADD KEY `role_user_role_id_index` (`role_id`);

--
-- Indeks untuk tabel `sensors`
--
ALTER TABLE `sensors`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `team_user`
--
ALTER TABLE `team_user`
  ADD KEY `team_user_user_id_index` (`user_id`),
  ADD KEY `team_user_team_id_index` (`team_id`);

--
-- Indeks untuk tabel `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indeks untuk tabel `user_meta`
--
ALTER TABLE `user_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_meta_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `contents`
--
ALTER TABLE `contents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `kelola_tanamen`
--
ALTER TABLE `kelola_tanamen`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `logsystems`
--
ALTER TABLE `logsystems`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT untuk tabel `offlinewriters`
--
ALTER TABLE `offlinewriters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `pendaftarans`
--
ALTER TABLE `pendaftarans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `perangkats`
--
ALTER TABLE `perangkats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `sensors`
--
ALTER TABLE `sensors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `topics`
--
ALTER TABLE `topics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `user_meta`
--
ALTER TABLE `user_meta`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `team_user`
--
ALTER TABLE `team_user`
  ADD CONSTRAINT `team_user_team_id_foreign` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `team_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `user_meta`
--
ALTER TABLE `user_meta`
  ADD CONSTRAINT `user_meta_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
