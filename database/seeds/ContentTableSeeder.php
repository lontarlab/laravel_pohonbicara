<?php

use App\Models\Content;
use Illuminate\Database\Seeder;

class ContentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Content::create([
          'name' => 'Radish shall never surrender!',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Will you have enough courage to fight side by side with brave daikon (white radish) soldiers? Join the Daikon Squad, help them to win their battle against evil fruit soldiers by playing varies mini games.',
          'user_id' => 1,
          'category_id' => 1,
      ]);
      Content::create([
          'name' => 'Endless Fun and Varied Gameplay',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'A variety of cute daikon characters in series of mini-games. Memory games, trivias, casual platformer games and many more.',
          'user_id' => 1,
          'category_id' => 1,
      ]);
      Content::create([
          'name' => 'HTML5',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'HTML5 games, you only need a common modern browser.',
          'user_id' => 1,
          'category_id' => 2,
      ]);
      Content::create([
          'name' => 'Mobile',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Built for mobile. Lite, fun and addictive.',
          'user_id' => 1,
          'category_id' => 2,
      ]);
      Content::create([
          'name' => 'Points',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Collectible and competitive rewards (depend on VAS/Telco).',
          'user_id' => 1,
          'category_id' => 2,
      ]);
      Content::create([
          'name' => 'Play Now!',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'No need to be installed like other (app-based) games.',
          'user_id' => 1,
          'category_id' => 2,
      ]);
      Content::create([
          'name' => 'Sample Portfolio 1',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Sample #1',
          'user_id' => 1,
          'category_id' => 3,
      ]);
      Content::create([
          'name' => 'Sample Portfolio 2',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Sample #2',
          'user_id' => 1,
          'category_id' => 3,
      ]);
      Content::create([
          'name' => 'Sample Portfolio 3',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Sample #3',
          'user_id' => 1,
          'category_id' => 3,
      ]);
      Content::create([
          'name' => 'Sample Portfolio 4',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Sample #4',
          'user_id' => 1,
          'category_id' => 3,
      ]);
      Content::create([
          'name' => 'Sample Portfolio 5',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Sample #5',
          'user_id' => 1,
          'category_id' => 3,
      ]);
      Content::create([
          'name' => 'Sample Portfolio 6',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Sample #6',
          'user_id' => 1,
          'category_id' => 3,
      ]);
    }
}
