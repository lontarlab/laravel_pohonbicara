<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerangkatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perangkats', function (Blueprint $table) {
            $table->increments('id');
		$table->integer('user_id');
		$table->text('platform');
		$table->text('origin');
		$table->text('content');
		$table->string('keterangan');
        $table->integer('tanaman_id');
        $table->integer('notifikasi');
        $table->integer('sent_chatbot');
        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perangkats');
    }
}
