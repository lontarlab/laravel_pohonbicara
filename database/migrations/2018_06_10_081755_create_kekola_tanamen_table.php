<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKekolaTanamenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kekola_tanamen', function (Blueprint $table) {
            $table->increments('id');
		$table->integer ('id_user');
		$table->string('jenis_tanaman');
		$table->string('nama_tanaman');
		$table->string('longitute');
		$table->string('latitude');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kekola_tanamen');
    }
}
