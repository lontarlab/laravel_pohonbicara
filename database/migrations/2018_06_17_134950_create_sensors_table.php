<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSensorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sensors', function (Blueprint $table) {
        $table->increments('id');
		$table->date('tanggal');
		$table->time('waktu');
		$table->string('ph_tanah');
		$table->string('kelembapan');
		$table->string('suhu');
		$table->string('cahaya');
		$table->integer('perangkat_id');
		$table->integer('user_id');

        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sensors');
    }
}
