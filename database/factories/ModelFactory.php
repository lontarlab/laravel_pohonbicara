<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

/*
|--------------------------------------------------------------------------
| UserMeta Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\UserMeta::class, function (Faker\Generator $faker) {
    return [
        'user_id' => 1,
        'phone' => $faker->phoneNumber,
        'marketing' => 1,
        'terms_and_cond' => 1,
    ];
});

$factory->define(App\Models\Role::class, function (Faker\Generator $faker) {
    return [
        'name' => 'member',
        'label' => 'Member',
    ];
});

/*
|--------------------------------------------------------------------------
| Team Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Team::class, function (Faker\Generator $faker) {
    return [
        'user_id' => 1,
        'name' => $faker->name
    ];
});

/*
|--------------------------------------------------------------------------
| Position Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Position::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'title' => 'laravel',
		'description' => 'laravel',


    ];
});

/*
|--------------------------------------------------------------------------
| Department Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Department::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'title' => 'laravel',
		'description' => 'laravel',


    ];
});

/*
|--------------------------------------------------------------------------
| Company Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Company::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'tax' => 'laravel',
		'reg' => 'laravel',
		'phone' => 'laravel',
		'fax' => 'laravel',
		'address1' => 'laravel',
		'address2' => 'laravel',
		'city' => 'laravel',
		'province' => 'laravel',
		'zip' => 'laravel',
		'country' => 'laravel',
		'logo' => 'laravel',
		'timezone' => 'laravel',
		'currency' => 'laravel',


    ];
});

/*
|--------------------------------------------------------------------------
| Logsystem Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Logsystem::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'ipaddress' => 'laravel',
		'user_id' => '1',


    ];
});

/*
|--------------------------------------------------------------------------
| Timezone Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Timezone::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'company_id' => '1',


    ];
});

/*
|--------------------------------------------------------------------------
| Education Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Education::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'company_id' => '1',


    ];
});

/*
|--------------------------------------------------------------------------
| Language Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Language::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'company_id' => '1',


    ];
});

/*
|--------------------------------------------------------------------------
| Skill Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Skill::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'description' => 'laravel',
		'company_id' => '1',


    ];
});

/*
|--------------------------------------------------------------------------
| Employmentstatus Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Employmentstatus::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'company_id' => '1',


    ];
});

/*
|--------------------------------------------------------------------------
| Jobtitle Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Jobtitle::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'company_id' => '1',


    ];
});

/*
|--------------------------------------------------------------------------
| Salarycomponent Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Salarycomponent::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'default' => 'laravel',
		'description' => 'laravel',
		'company_id' => '1',


    ];
});

/*
|--------------------------------------------------------------------------
| Leavetype Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Leavetype::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'company_id' => '1',


    ];
});

/*
|--------------------------------------------------------------------------
| Documenttype Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Documenttype::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'company_id' => '1',


    ];
});

/*
|--------------------------------------------------------------------------
| Topic Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Topic::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'description' => 'laravel',
		'body' => 'I am Batman',
		'user_id' => '1',
		'status' => 'laravel',


    ];
});

/*
|--------------------------------------------------------------------------
| Topic Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Topic::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'description' => 'laravel',
		'body' => 'I am Batman',
		'user_id' => '1',
		'status' => 'laravel',


    ];
});

/*
|--------------------------------------------------------------------------
| Topic Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Topic::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'description' => 'laravel',
		'body' => 'I am Batman',
		'user_id' => '1',
		'status' => 'laravel',


    ];
});

/*
|--------------------------------------------------------------------------
| Topic Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Topic::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'description' => 'laravel',
		'body' => 'I am Batman',
		'user_id' => '1',
		'status' => 'laravel',


    ];
});

/*
|--------------------------------------------------------------------------
| Category Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Category::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'description' => 'laravel',
		'body' => 'I am Batman',


    ];
});

/*
|--------------------------------------------------------------------------
| OfflineWriter Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\OfflineWriter::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'description' => 'laravel',
		'twitter' => 'laravel',
		'email' => 'laravel',
		'phone' => 'laravel',


    ];
});

/*
|--------------------------------------------------------------------------
| Comment Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Comment::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'content_id' => 'laravel',
		'name' => 'laravel',
		'email' => 'laravel',
		'body' => 'I am Batman',
		'status' => 'laravel',


    ];
});

/*
|--------------------------------------------------------------------------
| Content Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Content::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'description' => 'laravel',
		'quote' => 'laravel',
		'body' => 'I am Batman',
		'user_id' => '1',
		'offline_writer_id' => '1',
		'offline_writer' => 'laravel',
		'category_id' => '1',
		'topic_id' => '1',
		'status' => 'laravel',


    ];
});

/*
|--------------------------------------------------------------------------
| Message Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Message::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'message' => 'I am Batman',
		'sender_id' => '1',
		'receiver_id' => '1',
		'status' => 'laravel',


    ];
});

/*
|--------------------------------------------------------------------------
| Spam Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Spam::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'kode' => 'laravel',
		'name' => 'laravel',
		'polygon' => 'laravel',


    ];
});

/*
|--------------------------------------------------------------------------
| Dma Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Dma::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'zona_id' => '1',
		'kode' => '1',
		'name' => 'laravel',
		'polygon' => 'laravel'
	];
});


/*
|--------------------------------------------------------------------------
| Zona Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Zona::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'spam_id' => '1',
		'kode' => 'laravel',
		'name' => 'laravel',
		'polygon' => 'laravel',


    ];
});

/*
|--------------------------------------------------------------------------
| Brand Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Brand::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',


    ];
});

/*
|--------------------------------------------------------------------------
| Location Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Location::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',


    ];
});

/*
|--------------------------------------------------------------------------
| Station Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Station::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'brand_id' => '1',
		'location_id' => '1',
		'name' => 'laravel',
		'instalation_date' => '2017-12-06 05:13:35',
		'latitude' => 'laravel',
		'longitude' => 'laravel',
		'v_spam_kode_id' => '1',
		'kode' => 'laravel',
		'chart_url' => 'laravel',
		'data_url' => 'laravel',


    ];
});



/*
|--------------------------------------------------------------------------
| Device Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Device::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		'name' => 'laravel',
		'station_id' => '1',
		'brand_id' => '1',
		'measurement_id' => '1',
		'label' => 'laravel',
		'unit' => 'laravel',


    ];
});

/*
|--------------------------------------------------------------------------
| Waterbalance Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Waterbalance::class, function (Faker\Generator $faker) {
    return [

        'id' => '1',
		' dma_id' => 'laravel',
		' konsumsi_resmi' => 'laravel',


    ];
});

/*
|--------------------------------------------------------------------------
| Sensor Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Data\Sensor::class, function (Faker\Generator $faker) {
    return [
        '=id_sensor' => '1',
		' tanggal' => '2018-06-08',
		' waktu' => '06:59:18',
		' ph_tanah' => 'et',
		'kelembapan' => 'omnis',
		'suhu' => 'nostrum',
		'cahaya' => 'illum',
		'id_tanaman' => '1',
		' id_user' => '1',
    ];
});

/*
|--------------------------------------------------------------------------
| Pendaftaran Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Pendaftaran::class, function (Faker\Generator $faker) {
    return [
        '=id' => '1',
		'name' => 'quo',
		' email' => 'rerum',
		'alamat' => 'quia',
		' phone' => 'illo',
		' gender' => 'sit',
		' status' => 'aut',
    ];
});

/*
|--------------------------------------------------------------------------
| Lahan Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Kekola\Lahan::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'id_user' => '1',
		'jenis_tanaman' => 'mollitia',
		'nama_tanaman' => 'quia',
		'longitute' => 'consequuntur',
		'latitude' => 'eligendi',
    ];
});

/*
|--------------------------------------------------------------------------
| KekolaTanaman Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\KekolaTanaman::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'id_user' => '1',
		'jenis_tanaman' => 'consequatur',
		'nama_tanaman' => 'ut',
		'longitute' => 'nam',
		'latitude' => 'blanditiis',
    ];
});

/*
|--------------------------------------------------------------------------
| KelolaTanaman Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\KelolaTanaman::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'id_user' => '1',
		'jenis_tanaman' => 'fugit',
		'nama_tanaman' => 'vero',
		'longitute' => 'quia',
		'latitude' => 'delectus',
    ];
});

/*
|--------------------------------------------------------------------------
| Sensor Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Sensor::class, function (Faker\Generator $faker) {
    return [
        '=id' => '1',
		'tanggal' => '2018-06-17',
		'waktu' => '01:49:49',
		'ph_tanah' => 'sapiente',
		'kelembapan' => 'magnam',
		'suhu' => 'atque',
		'cahaya' => 'laboriosam',
		'antares_id' => '1',
		'user_id' => '1',
    ];
});

/*
|--------------------------------------------------------------------------
| Perangkat Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Perangkat::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'user_id' => '1',
		'platform' => 'ut ea quia in',
		'origin' => 'assumenda et ut repellat',
		'content' => 'omnis excepturi velit rerum',
		'keterangan' => 'earum',
    ];
});

/*
|--------------------------------------------------------------------------
| RangeSuhu Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\RangeSuhu::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'rendah' => '1',
		'sedang' => '1',
		'tinggi' => '1',
		'keterangan' => 'tenetur',
    ];
});

/*
|--------------------------------------------------------------------------
| RangeKelembapan Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\RangeKelembapan::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'rendah' => '1',
		'sedang' => '1',
		'tinggi' => '1',
		'keterangan' => 'quis',
    ];
});

/*
|--------------------------------------------------------------------------
| RangePhtanah Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\RangePhtanah::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'rendah' => '1',
		'sedang' => '1',
		'tinggi' => '1',
		'keterangan' => 'natus',
    ];
});

/*
|--------------------------------------------------------------------------
| RangeCahaya Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\RangeCahaya::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'rendah' => '1',
		'sedang' => '1',
		'tinggi' => '1',
		'keterangan' => 'dolores',
    ];
});

/*
|--------------------------------------------------------------------------
| NlpStopword Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\NlpStopword::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'kata_stopwords' => 'dolore',
    ];
});

/*
|--------------------------------------------------------------------------
| NlpStemquestion Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\NlpStemquestion::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'stem_question' => 'illo',
		'jawaban' => 'atque',
		'kategori' => 'harum',
    ];
});

/*
|--------------------------------------------------------------------------
| NlpPertanyaanuser Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\NlpPertanyaanuser::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'pertanyaan' => 'et',
		'jawaban' => 'et',
		'kategori' => 'quae',
    ];
});

/*
|--------------------------------------------------------------------------
| NlpKatadasar Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\NlpKatadasar::class, function (Faker\Generator $faker) {
    return [
        'id_katadasar' => '1',
		'nlp_katadasar' => 'laudantium',
		'tipe_katadasar' => 'rerum',
    ];
});

/*
|--------------------------------------------------------------------------
| SensorChat Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\SensorChat::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'tanggal' => '2018-12-24',
		'waktu' => '07:08:29',
		'kelembapan_tanah' => 'et',
		'kelembapan' => 'amet',
		'suhu' => 'voluptatem',
		'cahaya' => 'ducimus',
		'perangkat_id' => '1',
		'user_id' => '1',
    ];
});
