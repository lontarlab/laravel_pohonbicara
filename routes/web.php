<?php

use Illuminate\Support\Facades\Route;

// Non protected/public routes
Route::get('/', 'WelcomeController@index');

Route::get('antares', 'AntaresController@index');

Route::get('getantares/{id}', 'KelolaTanamenController@antares');

// Login Logout
Route::get('login', 'Auth\LoginController@showLoginForm');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout');
// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
// Regisration
Route::get('register', 'Auth\RegisterController@showRegistrationForm');
Route::post('register', 'Auth\RegisterController@register');
Route::get('activate', 'Auth\ActivateController@showActivate');
Route::get('activate/send-token', 'Auth\ActivateController@sendToken');
Route::get('activate/token/{token}', 'Auth\ActivateController@activate');

// HARUS login / Authenticated Routes
Route::group(['middleware' => ['auth', 'active']], function () {
    // User stuff
    Route::get('/users/switch-back', 'Admin\UserController@switchUserBack');
    Route::group(['prefix' => 'user', 'namespace' => 'User'], function () {
        Route::get('settings', 'SettingsController@settings');
        Route::post('settings', 'SettingsController@update');
        Route::post('editmeta/{id}', 'SettingsController@updatemeta');
        Route::get('password', 'PasswordController@password');
        Route::post('password', 'PasswordController@update');
    });
    // Dashboard
    Route::get('/dashboard', 'PagesController@dashboard');

    // Team
    Route::get('team/{name}', 'TeamController@showByName');
    Route::resource('teams', 'TeamController', ['except' => ['show']]);
    Route::post('teams/search', 'TeamController@search');
    Route::post('teams/{id}/invite', 'TeamController@inviteMember');
    Route::get('teams/{id}/remove/{userId}', 'TeamController@removeMember');


    // HARUS login, HARUS admin atau member
    Route::group(['middleware' => 'roles:admin|member'], function () {
        Route::resource('contents', 'ContentsController');
        Route::post('contents/search', [
            'as' => 'contents.search',
            'uses' => 'ContentsController@search'
        ]);
    });

    // HARUS login, HARUS admin
    Route::group(['middleware' => 'roles:admin'], function () {
      // Organization & References
      Route::resource('companies', 'CompaniesController');
      Route::post('companies/search', [
          'as' => 'companies.search',
          'uses' => 'CompaniesController@search'
      ]);

      // CMS
      Route::resource('topics', 'TopicsController');
      Route::post('topics/search', [
          'as' => 'topics.search',
          'uses' => 'TopicsController@search'
      ]);
      Route::resource('categories', 'CategoriesController');
      Route::post('categories/search', [
          'as' => 'categories.search',
          'uses' => 'CategoriesController@search'
      ]);
      Route::resource('offlinewriters', 'OfflineWritersController');
      Route::post('offlinewriters/search', [
          'as' => 'offlinewriters.search',
          'uses' => 'OfflineWritersController@search'
      ]);
      Route::resource('comments', 'CommentsController');
      Route::post('comments/search', [
          'as' => 'comments.search',
          'uses' => 'CommentsController@search'
      ]);
      // Log System
      Route::resource('logsystems', 'LogsystemsController');
      Route::post('logsystems/search', [
          'as' => 'logsystems.search',
          'uses' => 'LogsystemsController@search'
      ]);

    });

    // HARUS login, HARUS admin dan controllernya HARUS di /admin
    Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'admin'], function () {
        // User
        Route::resource('users', 'UserController', ['except' => ['create', 'show']]);
        Route::post('users/search', 'UserController@search');
        Route::get('users/search', 'UserController@index');
        Route::get('users/invite', 'UserController@getInvite');
        Route::get('users/switch/{id}', 'UserController@switchToUser');
        Route::post('users/invite', 'UserController@postInvite');
        // Roles
        Route::resource('roles', 'RoleController', ['except' => ['show']]);
        Route::post('roles/search', 'RoleController@search');
        Route::get('roles/search', 'RoleController@index');

});

      //khusus READ ADMIN
        Route::get('/tanaman', 'KelolaTanamenController@tanaman');
        Route::get('/sensor', 'SensorsController@sensor');
        Route::get('/admingrafik', 'SensorsController@admingrafik');
        Route::get('/perangkat', 'perangkatsController@perangkat');
        Route::get('/manajemenpohonbicara', 'KelolaTanamenController@manajemenpohonbicara');
        Route::get('/showadmin', 'KelolaTanamenController@showadmin');;

// Pendaftaran online JerbeeUmroh
Route::get('/pendaftarans/register',[
    'as' => 'pendaftarans.register',
    'uses' => 'PendaftaransController@register'
]);
Route::post('pendaftarans/registerstore', 'PendaftaransController@registerstore');
/*
|--------------------------------------------------------------------------
| Pendaftaran Routes
|--------------------------------------------------------------------------
*/
 Route::resource('pendaftarans', 'PendaftaransController', ['except' => ['show']]);
        Route::post('pendaftarans/search', [
              'as' => 'pendaftarans.search',
              'uses' => 'PendaftaransController@search'
        ]);
        Route::get('/pendaftarans/{id}/verify',[
                'as' => 'pendaftarans.verify',
                'uses' => 'PendaftaransController@verify'
        ]);
        Route::post('pendaftarans/store_verify', 'PendaftaransController@store_verify');
  });


Route::resource('kelola_tanamen', 'KelolaTanamenController');
Route::post('kelola_tanamen/search', [
    'as' => 'kelola_tanamen.search',
    'uses' => 'KelolaTanamenController@search'
]);
Route::get('pohonbicara', 'KelolaTanamenController@pohonbicara');

Route::resource('sensors', 'SensorsController', ['except' => ['show']]);
Route::post('sensors/search', [
    'as' => 'sensors.search',
    'uses' => 'SensorsController@search'
]);
Route::get('grafik', 'SensorsController@grafik');
Route::get('grafik_waktu', 'SensorsController@grafik_waktu');
Route::get('grafikhari', 'SensorsController@grafikhari');
Route::get('grafikhari2', 'SensorsController@grafikhari2');
Route::get('grafikbulan', 'SensorsController@grafikbulan');
Route::get('grafikbulan2', 'SensorsController@grafikbulan2');
Route::get('grafiktahun', 'SensorsController@grafiktahun');
Route::get('grafiktahun2', 'SensorsController@grafiktahun2');

Route::resource('perangkats', 'PerangkatsController');
Route::post('perangkats/search', [
    'as' => 'perangkats.search',
    'uses' => 'PerangkatsController@search'
]);

/*
|--------------------------------------------------------------------------
| RangeSuhu Routes
|--------------------------------------------------------------------------
*/

Route::resource('range_suhus', 'RangeSuhusController', ['except' => ['show']]);
Route::post('range_suhus/search', [
    'as' => 'range_suhus.search',
    'uses' => 'RangeSuhusController@search'
]);

/*
|--------------------------------------------------------------------------
| RangeKelembapan Routes
|--------------------------------------------------------------------------
*/

Route::resource('range_kelembapans', 'RangeKelembapansController', ['except' => ['show']]);
Route::post('range_kelembapans/search', [
    'as' => 'range_kelembapans.search',
    'uses' => 'RangeKelembapansController@search'
]);

/*
|--------------------------------------------------------------------------
| RangePhtanah Routes
|--------------------------------------------------------------------------
*/

Route::resource('range_phtanahs', 'RangePhtanahsController', ['except' => ['show']]);
Route::post('range_phtanahs/search', [
    'as' => 'range_phtanahs.search',
    'uses' => 'RangePhtanahsController@search'
]);

/*
|--------------------------------------------------------------------------
| RangeCahaya Routes
|--------------------------------------------------------------------------
*/

Route::resource('range_cahayas', 'RangeCahayasController', ['except' => ['show']]);
Route::post('range_cahayas/search', [
    'as' => 'range_cahayas.search',
    'uses' => 'RangeCahayasController@search'
]);

/*
|--------------------------------------------------------------------------
| NlpStopword Routes
|--------------------------------------------------------------------------
*/

Route::resource('nlp_stopwords', 'NlpStopwordsController', ['except' => ['show']]);
Route::post('nlp_stopwords/search', [
    'as' => 'nlp_stopwords.search',
    'uses' => 'NlpStopwordsController@search'
]);

/*
|--------------------------------------------------------------------------
| NlpStemquestion Routes
|--------------------------------------------------------------------------
*/

Route::resource('nlp_stemquestions', 'NlpStemquestionsController', ['except' => ['show']]);
Route::post('nlp_stemquestions/search', [
    'as' => 'nlp_stemquestions.search',
    'uses' => 'NlpStemquestionsController@search'
]);

/*
|--------------------------------------------------------------------------
| NlpPertanyaanuser Routes
|--------------------------------------------------------------------------
*/

Route::resource('nlp_pertanyaanusers', 'NlpPertanyaanusersController', ['except' => ['show']]);
Route::post('nlp_pertanyaanusers/search', [
    'as' => 'nlp_pertanyaanusers.search',
    'uses' => 'NlpPertanyaanusersController@search'
]);

/*
|--------------------------------------------------------------------------
| NlpKatadasar Routes
|--------------------------------------------------------------------------
*/

Route::resource('nlp_katadasars', 'NlpKatadasarsController', ['except' => ['show']]);
Route::post('nlp_katadasars/search', [
    'as' => 'nlp_katadasars.search',
    'uses' => 'NlpKatadasarsController@search'
]);

/*
|--------------------------------------------------------------------------
| SensorChat Routes
|--------------------------------------------------------------------------
*/

Route::resource('sensor_chats', 'SensorChatsController', ['except' => ['show']]);
Route::post('sensor_chats/search', [
    'as' => 'sensor_chats.search',
    'uses' => 'SensorChatsController@search'
]);
