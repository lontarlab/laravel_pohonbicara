<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
    // Route::get('listtanaman','Apiv1Controller@listtanaman');
    // Route::get('listdepartment','Apiv1Controller@listdepartment');
    // Route::get('sensornewest/{id}','Apiv1Controller@sensornewest');
    // Route::get('sensorhistory/{id}','Apiv1Controller@sensorhistory');
    // Route::post('postsensor', 'Apiv1Controller@postsensor');
    // Route::get('getsensor', 'Apiv1Controller@getsensor');

    Route::get('listtanaman','Apiv1Controller@listtanaman');
    Route::get('listdepartment','Apiv1Controller@listdepartment');
    Route::get('sensornewest/{id}','Apiv1Controller@sensornewest');
    Route::get('sensorhistory/{id}','Apiv1Controller@sensorhistory');
    Route::post('postsensor', 'Apiv1Controller@postsensor');
    Route::post('postsensorchat', 'Apiv1Controller@postsensorchat');

    Route::get('sensornew/{id}','Apiv1Controller@sensornew');
    Route::post('getsuhu/{id}','Apiv1Controller@getsuhu');
    Route::post('getkelembapan/{id}','Apiv1Controller@getkelembapan');
    Route::post('getkelembapantanah/{id}','Apiv1Controller@getkelembapantanah');
    Route::post('getcahaya/{id}','Apiv1Controller@getcahaya');

    Route::get('getplant/{id}','Apiv1Controller@getplant');
    Route::get('getperangkat/{id}','Apiv1Controller@getperangkat');

    Route::get('userline/{id}','Apiv1Controller@userline');
    Route::post('registerline','Apiv1Controller@registerline');

    Route::post('aktifnotif', 'Apiv1Controller@aktifnotif');

    Route::post('postthreshold', 'Apiv1Controller@postthreshold');

    Route::post('postlabel01', 'Apiv1Controller@postlabel01');
    Route::post('postlabel02', 'Apiv1Controller@postlabel02');
    Route::post('postlabel03', 'Apiv1Controller@postlabel03');

    Route::post('postsaklar01', 'Apiv1Controller@postsaklar01');
    Route::post('postsaklar02', 'Apiv1Controller@postsaklar02');
    Route::post('postsaklar03', 'Apiv1Controller@postsaklar03');

    //nlp API
    Route::get('getkatadasar/{katadasar}','Apiv1Controller@getkatadasar');
    Route::get('getstopword/{kata_stopwords}','Apiv1Controller@getstopword');
    Route::get('getstemquestion/{kata}','Apiv1Controller@getstemquestion');
    Route::get('getpertanyaanuser/{pertanyaan}','Apiv1Controller@getpertanyaanuser');
    Route::post('postpertanyaanuser', 'Apiv1Controller@postpertanyaanuser');
    Route::post('poststemuser', 'Apiv1Controller@poststemuser');

    Route::get('notifon','Apiv1Controller@notifon');
    Route::get('notifoff','Apiv1Controller@notifoff');
    
    
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::get('refresh', 'AuthController@refresh');
        Route::group(['prefix' => 'user'], function () {
          Route::get('profile', 'UserController@getProfile');
          Route::post('profile', 'UserController@postProfile');

          
        });
    });
});
