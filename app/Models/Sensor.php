<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Perangkat;


class Sensor extends Model
{
    public $table = "sensors";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'id',
		'tanggal',
		'waktu',
		'ph_tanah',
		'kelembapan',
		'suhu',
		'cahaya',
		'tanaman_id',
		'user_id',
        'perangkat_id'
    
];

    public static $rules = [
        // create rules
    ];
    // Sensor 
    public function user()
    {
        return $this->belongsTo(User::class);
    }

     public function perangkat()
    {
        return $this->belongsTo(Perangkat::class);
    }

}
