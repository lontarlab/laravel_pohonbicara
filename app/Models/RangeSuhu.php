<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RangeSuhu extends Model
{
    public $table = "range_suhus";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'id',
		'rendah',
		'sedang',
		'tinggi',
		'keterangan',
    
];

    public static $rules = [
        // create rules
    ];
    // RangeSuhu 
}
