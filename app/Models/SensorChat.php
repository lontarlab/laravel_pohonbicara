<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SensorChat extends Model
{
    public $table = "sensor_chats";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'id',
		'tanggal',
		'waktu',
		'kelembapan_tanah',
		'kelembapan',
		'suhu',
		'cahaya',
		'perangkat_id',
		'user_id',
		'has_sent'
    
];

    public static $rules = [
        // create rules
    ];
    // SensorChat 
}
