<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Sensor;
use App\Models\User;

class Perangkat extends Model
{
    public $table = "perangkats";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'id',
		'user_id',
		'platform',
		'origin',
		'content',
		'keterangan',
        'tanaman_id',
        'notifikasi',
        'sent_chatbot',
        'last_answer',
        'notifikasi_suhu',
        'notifikasi_tanah',
        'notifikasi_cahaya',
        'notifikasi_kelembapan'
    
];

    public static $rules = [
        // create rules
    ];
   
   public function kelolaSensor()
    {
        return $this->hasMany(Sensor::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }


}
