<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NlpStemquestion extends Model
{
    public $table = "nlp_stemquestions";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'id',
		'stem_question',
		'jawaban',
		'kategori',
    
];

    public static $rules = [
        // create rules
    ];
    // NlpStemquestion 
}
