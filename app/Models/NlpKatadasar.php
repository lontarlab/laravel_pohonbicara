<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NlpKatadasar extends Model
{
    public $table = "nlp_katadasars";

    public $primaryKey = "id";
    // public $primaryKey = "id_katadasar";

    public $timestamps = true;

    public $fillable = [
		// 'id_katadasar',
		'katadasar',
		'tipe_katadasar',
    
];

    public static $rules = [
        // create rules
    ];
    // NlpKatadasar 
}
