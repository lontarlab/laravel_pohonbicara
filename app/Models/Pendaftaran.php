<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pendaftaran extends Model
{
    public $table = "pendaftarans";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'id',
		'name',
		'email',
		'alamat',
		'phone',
		'gender',
		'status',
    
];

    public static $rules = [
        // create rules
    ];
    // Pendaftaran 
}
