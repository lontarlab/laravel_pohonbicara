<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RangeKelembapan extends Model
{
    public $table = "range_kelembapans";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'id',
		'rendah',
		'sedang',
		'tinggi',
		'keterangan',
    
];

    public static $rules = [
        // create rules
    ];
    // RangeKelembapan 
}
