<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RangeCahaya extends Model
{
    public $table = "range_cahayas";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'id',
		'rendah',
		'sedang',
		'tinggi',
		'keterangan',
    
];

    public static $rules = [
        // create rules
    ];
    // RangeCahaya 
}
