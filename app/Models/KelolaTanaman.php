<?php

namespace App\Models;
use App\Models\User;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;
use App\Models\Sensor;
use App\Models\KelolaTanaman;
use App\Models\Perangkat;


use Illuminate\Database\Eloquent\Model;

class KelolaTanaman extends Model  implements StaplerableInterface
{
    use EloquentTrait;
    public $table = "kelola_tanamen";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'id',
		'user_id',
		'jenis_tanaman',
		'nama_tanaman',
		'longitute',
		'latitude',
        'perangkat_id',
        'foto',
    ];

    public function __construct(array $getAttributes  = array()) {
        $this->hasAttachedFile('foto', [
            'styles' => [
                'medium' => '300x300',
                'thumb' => '100x100', 
            ]
        ]);

        parent::__construct($getAttributes);
    }
 
     public function getAttributes()
    {
        return parent::getAttributes();
    }


    public static $rules = [
        // create rules
    ];
    // KelolaTanaman 

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function sensors()
    {
        return $this->hasMany(Sensor::class, 'tanaman_id', 'id');
    }

    public function perangkat()
    {
        return $this->hasOne(Perangkat::class, 'tanaman_id', 'id');
    }

}
