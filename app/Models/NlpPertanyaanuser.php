<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NlpPertanyaanuser extends Model
{
    public $table = "nlp_pertanyaanusers";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'id',
		'pertanyaan',
		'jawaban',
		'kategori',
    
];

    public static $rules = [
        // create rules
    ];
    // NlpPertanyaanuser 
}
