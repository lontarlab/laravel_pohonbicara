<?php

namespace App\Models;
use App\Models\Pendaftaran;
use Illuminate\Database\Eloquent\Model;

class UserStore extends Model{
//Models
protected  $table = "users";

public $primaryKey = "id";

public $timestamps = true;

protected $fillable = [
'pendaftaran_id',
'name',
'email',
'password',
];

public static $rules = [
    // create rules
];
public function pendaftarans()
{
    return $this->belongsToMany(Pendaftaran::class);
}


}
