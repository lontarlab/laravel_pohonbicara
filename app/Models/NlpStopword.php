<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NlpStopword extends Model
{
    public $table = "nlp_stopwords";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'id_stopwords',
		'kata_stopwords',
    
];

    public static $rules = [
        // create rules
    ];
    // NlpStopword 
}
