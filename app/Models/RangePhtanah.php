<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RangePhtanah extends Model
{
    public $table = "range_phtanahs";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'id',
		'rendah',
		'sedang',
		'tinggi',
		'keterangan',
    
];

    public static $rules = [
        // create rules
    ];
    // RangePhtanah 
}
