<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use File;

class cleanStarter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cleanstart';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleaning JerbeeDashboard to blank JerbeeStarter.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Bersihkan modul JerbeeDashboard
        $moduls = array(
          "Brand",
          "Device",
          "Dma",
          "Logger",
          "Measurement",
          "Pelanggan",
          "PelangganDkd",
          "PelangganDma",
          "PelangganDrd",
          "PelangganGolongan",
          "PelangganKondisipasang",
          "PelangganKotapelayanan",
          "PublicDashboard",
          "Position",
          "Nrw",
          "Spam",
          "Waterbalance",
          "Zona",
        );
        foreach ($moduls as $modul) {
          File::delete(File::glob('app/Models/'.$modul.'.php'));
          File::delete(File::glob('app/Services/'.$modul.'Service.php'));
          File::delete(File::glob('app/Http/Requests/'.$modul.'*Request.php'));
          File::delete(File::glob('app/Http/Controllers/'.$modul.'sController.php'));
          File::delete(File::glob('app/Http/Controllers/'.$modul.'Controller.php'));
          File::delete(File::glob('database/migrations/*_'.strtolower($modul).'s_*'));
          File::delete(File::glob('database/migrations/old/*_'.strtolower($modul).'s_*'));
          File::delete(File::glob('database/migrations/old/*_'.strtolower($modul).'s.*'));
          File::delete(File::glob('database/seeds/'.$modul.'*Table*'));
          File::deleteDirectory('resources/views/'.strtolower($modul).'s/');
          File::deleteDirectory('resources/views/'.strtolower($modul).'/');
          File::delete(File::glob('tests/acceptance/'.$modul.'Acceptance*'));
          File::delete(File::glob('tests/integration/'.$modul.'Service*'));
        }

        // Hapus Git
        File::deleteDirectory('.git/');
        // Refresh .env and files
        File::copy(base_path('documentation/starter/readme.md'),base_path('readme.md'));
        File::copy(base_path('documentation/starter/.env'),base_path('.env'));
        File::copy(base_path('documentation/starter/web.php'),base_path('routes/web.php'));
        // Refresh database and migrations
        File::deleteDirectory('database');
        File::copyDirectory(base_path('documentation/starter/database/'),base_path('database'));
        // Refresh assets
        File::delete(File::glob('public/img/payroll*'));
        File::delete(File::glob('public/img/tr*'));
        File::copyDirectory(base_path('documentation/starter/portfolio/'),base_path('public/img/portfolio'));
        File::copy(base_path('documentation/starter/header.jpeg'),base_path('public/img/header.jpeg'));
        File::copyDirectory(base_path('documentation/starter/avatars/'),base_path('public/upload/user/avatars'));
        // Refresh backend Home
        File::copyDirectory(base_path('documentation/starter/dashboard/'),base_path('resources/views/dashboard'));


        // Cleaning Home's navigation bar
        $content = file_get_contents('resources/views/welcome/index.blade.php');
        $search = "/<li>.*(<a class=\"page-scroll\".*>Dashboard Publik<\/a>).*<\/li>/Ums";
        $replace = "";
        $content = preg_replace($search, $replace, $content);
        file_put_contents('resources/views/welcome/index.blade.php', $content);
        // Cleaning Home's front gallery
        $content = file_get_contents('resources/views/welcome/index.blade.php');
        $search = "/<a href=\"http:\/\/.*\" class=\"portfolio-box\">/Ums";
        $replace = "<a href=\"img/portfolio/thumbnails/4.jpg\" class=\"portfolio-box\">";
        $content = preg_replace($search, $replace, $content);
        $search = "/PDAM Tirta Raharja Kabupaten Bandung/Ums";
        $replace = "";
        $content = preg_replace($search, $replace, $content);
        file_put_contents('resources/views/welcome/index.blade.php', $content);
        // Cleaning Home's CSS colors
        $content = file_get_contents('public/css/creative.css');
        $search1 = "/#0f69d7/Ums";
        $replace1 = "#f05f40";
        $content = preg_replace($search1, $replace1, $content);
        $search2 = "/#23527c/Ums";
        $replace2 = "#af3819";
        $content = preg_replace($search2, $replace2, $content);
        $search3 = "/15, 105, 215, 0.8/Ums";
        $replace3 = "240, 95, 64, 0.8";
        $content = preg_replace($search3, $replace3, $content);
        file_put_contents('public/css/creative.css', $content);
        // Cleaning Backend's CSS colors
        $content = file_get_contents('public/css/app.css');
        $search1 = "/#0f69d7/Ums";
        $replace1 = "#f05f40";
        $content = preg_replace($search1, $replace1, $content);
        $search2 = "/#23527c/Ums";
        $replace2 = "#af3819";
        $content = preg_replace($search2, $replace2, $content);
        $search3 = "/15, 105, 215, 0.8/Ums";
        $replace3 = "240, 95, 64, 0.8";
        $content = preg_replace($search3, $replace3, $content);
        file_put_contents('public/css/app.css', $content);

        echo "Clean JerbeeStarter now ready"."\n";
    }
}
