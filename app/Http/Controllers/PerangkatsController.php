<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PerangkatService;
use App\Http\Requests\PerangkatCreateRequest;
use App\Http\Requests\PerangkatUpdateRequest;

class PerangkatsController extends Controller
{
    public function __construct(PerangkatService $perangkat_service)
    {
        $this->service = $perangkat_service;
    }

    public function index(Request $request)
    {
        $perangkats = $this->service->paginated();
        return view('perangkats.index')->with('perangkats', $perangkats);
    }

    //fungsi untuk admin
    public function perangkat(Request $request)
    {
        $perangkats = $this->service->paginated();
        return view('perangkats.index')->with('perangkats', $perangkats);
    }


   
    public function search(Request $request)
    {
        $perangkats = $this->service->search($request->search);
        return view('perangkats.index')->with('perangkats', $perangkats);
    }


    public function create()
    {
        return view('perangkats.create');
    }

    
    public function store(PerangkatCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('perangkats.index'))->with('message', 'Successfully created');
        }

        return redirect(route('perangkats.index'))->with('message', 'Failed to create');
    }

    
    public function show($id)
    {
        $perangkat = $this->service->find($id);
        return view('perangkats.show')->with('perangkat', $perangkat);
    }

   
    public function edit($id)
    {
        $perangkat = $this->service->find($id);
        return view('perangkats.edit')->with('perangkat', $perangkat);
    }

  
    public function update(PerangkatUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
             return redirect(route('perangkats.index'))->with('message', 'Successfully update');
        }

        return back()->with('message', 'Failed to update');
    }

  
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('perangkats.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('perangkats.index'))->with('message', 'Failed to delete');
    }
}
