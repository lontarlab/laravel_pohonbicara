<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\RangeCahayaService;
use App\Http\Requests\RangeCahayaCreateRequest;
use App\Http\Requests\RangeCahayaUpdateRequest;

class RangeCahayasController extends Controller
{
    public function __construct(RangeCahayaService $range_cahaya_service)
    {
        $this->service = $range_cahaya_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $range_cahayas = $this->service->paginated();
        return view('range_cahayas.index')->with('range_cahayas', $range_cahayas);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $range_cahayas = $this->service->search($request->search);
        return view('range_cahayas.index')->with('range_cahayas', $range_cahayas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('range_cahayas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\RangeCahayaCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RangeCahayaCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('range_cahayas.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('range_cahayas.index'))->with('message', 'Failed to create');
    }

    /**
     * Display the range_cahaya.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $range_cahaya = $this->service->find($id);
        return view('range_cahayas.show')->with('range_cahaya', $range_cahaya);
    }

    /**
     * Show the form for editing the range_cahaya.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $range_cahaya = $this->service->find($id);
        return view('range_cahayas.edit')->with('range_cahaya', $range_cahaya);
    }

    /**
     * Update the range_cahayas in storage.
     *
     * @param  \Illuminate\Http\RangeCahayaUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RangeCahayaUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->with('message', 'Failed to update');
    }

    /**
     * Remove the range_cahayas from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('range_cahayas.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('range_cahayas.index'))->with('message', 'Failed to delete');
    }
}
