<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PendaftaranService;
use App\Http\Requests\PendaftaranCreateRequest;
use App\Http\Requests\RegisterCreateRequest;
use App\Http\Requests\PendaftaranUpdateRequest;
use App\Models\UserStore;
use App\Models\User;
use App\Models\UserMeta;
use App\Models\Pendaftaran;
use App\Models\RoleUser;

class PendaftaransController extends Controller
{
    public function __construct(PendaftaranService $pendaftaran_service)
    {
        $this->service = $pendaftaran_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pendaftarans = $this->service->paginated();
        return view('pendaftarans.index')->with('pendaftarans', $pendaftarans);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $pendaftarans = $this->service->search($request->search);
        return view('pendaftarans.index')->with('pendaftarans', $pendaftarans);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pendaftarans.create');
    }

    public function register()
    {
      
        return view('pendaftarans.register');
    }
    
    public function registerstore(RegisterCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));
        if ($result) {
            return view('pendaftarans.terimakasih')->with('message', 'Successfully created');
        }

        return view('pendaftarans.terimakasih')->with('message', 'Failed to create');
    }

    public function verify($id)
    {
        $pendaftaran = $this->service->find($id); 
        return view('pendaftarans.verify',compact('pendaftaran'));
    }

   
    public function store(PendaftaranCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('pendaftarans.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('pendaftarans.index'))->with('message', 'Failed to create');
    }

     public function store_verify(Request $request)
     {
    
        $result =  UserStore::create([
             'name' =>  $request->input('name'),
             'email' => $request->input('email'),
             'password' => bcrypt('member'),
        ]);
        $result2 =  UserMeta::create([
              'user_id' =>  $result->id,
              'phone' => $request->input('phone'),
              'gender' => $request->input('gender'),
              'is_active' => 1,
        ]);
        $result3 =  RoleUser::create([
              'user_id' =>  $result->id,
              'role_id' => 1,
        ]);
        
         $result4 = Pendaftaran::where('id', $request->pendaftaran_id)->update(['status' => 'Terverifikasi']);
        if($result) {     
        return redirect(route('pendaftarans.index'))->with('message', 'Verified Successfully');
        }
        return redirect(route('pendaftarans.index'))->with('message', 'Failed to verify');
    }

  
    public function show($id)
    {
        $pendaftaran = $this->service->find($id);
        return view('pendaftarans.show')->with('pendaftaran', $pendaftaran);
    }

    /**
     * Show the form for editing the pendaftaran.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pendaftaran = $this->service->find($id);
        return view('pendaftarans.edit')->with('pendaftaran', $pendaftaran);
    }

    /**
     * Update the pendaftarans in storage.
     *
     * @param  \Illuminate\Http\PendaftaranUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PendaftaranUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->with('message', 'Failed to update');
    }

    /**
     * Remove the pendaftarans from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('pendaftarans.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('pendaftarans.index'))->with('message', 'Failed to delete');
    }
}
