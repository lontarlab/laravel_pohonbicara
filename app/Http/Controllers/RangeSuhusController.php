<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\RangeSuhuService;
use App\Http\Requests\RangeSuhuCreateRequest;
use App\Http\Requests\RangeSuhuUpdateRequest;

class RangeSuhusController extends Controller
{
    public function __construct(RangeSuhuService $range_suhu_service)
    {
        $this->service = $range_suhu_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $range_suhus = $this->service->paginated();
        return view('range_suhus.index')->with('range_suhus', $range_suhus);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $range_suhus = $this->service->search($request->search);
        return view('range_suhus.index')->with('range_suhus', $range_suhus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('range_suhus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\RangeSuhuCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RangeSuhuCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('range_suhus.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('range_suhus.index'))->with('message', 'Failed to create');
    }

    /**
     * Display the range_suhu.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $range_suhu = $this->service->find($id);
        return view('range_suhus.show')->with('range_suhu', $range_suhu);
    }

    /**
     * Show the form for editing the range_suhu.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $range_suhu = $this->service->find($id);
        return view('range_suhus.edit')->with('range_suhu', $range_suhu);
    }

    /**
     * Update the range_suhus in storage.
     *
     * @param  \Illuminate\Http\RangeSuhuUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RangeSuhuUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->with('message', 'Failed to update');
    }

    /**
     * Remove the range_suhus from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('range_suhus.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('range_suhus.index'))->with('message', 'Failed to delete');
    }
}
