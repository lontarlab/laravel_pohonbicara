<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\NlpStopwordService;
use App\Http\Requests\NlpStopwordCreateRequest;
use App\Http\Requests\NlpStopwordUpdateRequest;

class NlpStopwordsController extends Controller
{
    public function __construct(NlpStopwordService $nlp_stopword_service)
    {
        $this->service = $nlp_stopword_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $nlp_stopwords = $this->service->paginated();
        return view('nlp_stopwords.index')->with('nlp_stopwords', $nlp_stopwords);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $nlp_stopwords = $this->service->search($request->search);
        return view('nlp_stopwords.index')->with('nlp_stopwords', $nlp_stopwords);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('nlp_stopwords.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\NlpStopwordCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NlpStopwordCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('nlp_stopwords.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('nlp_stopwords.index'))->with('message', 'Failed to create');
    }

    /**
     * Display the nlp_stopword.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $nlp_stopword = $this->service->find($id);
        return view('nlp_stopwords.show')->with('nlp_stopword', $nlp_stopword);
    }

    /**
     * Show the form for editing the nlp_stopword.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $nlp_stopword = $this->service->find($id);
        return view('nlp_stopwords.edit')->with('nlp_stopword', $nlp_stopword);
    }

    /**
     * Update the nlp_stopwords in storage.
     *
     * @param  \Illuminate\Http\NlpStopwordUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NlpStopwordUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->with('message', 'Failed to update');
    }

    /**
     * Remove the nlp_stopwords from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('nlp_stopwords.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('nlp_stopwords.index'))->with('message', 'Failed to delete');
    }
}
