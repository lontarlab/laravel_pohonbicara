<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\RangeKelembapanService;
use App\Http\Requests\RangeKelembapanCreateRequest;
use App\Http\Requests\RangeKelembapanUpdateRequest;

class RangeKelembapansController extends Controller
{
    public function __construct(RangeKelembapanService $range_kelembapan_service)
    {
        $this->service = $range_kelembapan_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $range_kelembapans = $this->service->paginated();
        return view('range_kelembapans.index')->with('range_kelembapans', $range_kelembapans);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $range_kelembapans = $this->service->search($request->search);
        return view('range_kelembapans.index')->with('range_kelembapans', $range_kelembapans);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('range_kelembapans.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\RangeKelembapanCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RangeKelembapanCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('range_kelembapans.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('range_kelembapans.index'))->with('message', 'Failed to create');
    }

    /**
     * Display the range_kelembapan.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $range_kelembapan = $this->service->find($id);
        return view('range_kelembapans.show')->with('range_kelembapan', $range_kelembapan);
    }

    /**
     * Show the form for editing the range_kelembapan.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $range_kelembapan = $this->service->find($id);
        return view('range_kelembapans.edit')->with('range_kelembapan', $range_kelembapan);
    }

    /**
     * Update the range_kelembapans in storage.
     *
     * @param  \Illuminate\Http\RangeKelembapanUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RangeKelembapanUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->with('message', 'Failed to update');
    }

    /**
     * Remove the range_kelembapans from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('range_kelembapans.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('range_kelembapans.index'))->with('message', 'Failed to delete');
    }
}
