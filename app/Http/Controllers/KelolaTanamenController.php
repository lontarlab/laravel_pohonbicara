<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\KelolaTanamanService;
use App\Http\Requests\KelolaTanamanCreateRequest;
use App\Http\Requests\KelolaTanamanUpdateRequest;
use App\Models\KelolaTanaman;
use App\Models\Sensor;
use App\Models\Perangkat;
use App\Models\RangeSuhu;
use App\Models\RangeKelembapan;
use App\Models\RangePhtanah;
use App\Models\RangeCahaya;
use App\Models\User;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class KelolaTanamenController extends Controller
{
    public function __construct(KelolaTanamanService $kelola_tanaman_service)
    {
        $this->service = $kelola_tanaman_service;
    }

    public function index(Request $request)
    {
        $user = $request->user()->id;
        $kelola_tanamen  = User::find($user)->tanaman()->get();
        return view('kelola_tanamen.index',compact('kelola_tanamen'));
    }

    //fungsi untuk admin
    public function tanaman(Request $request)
    {
        $user = $request->user();
        $kelola_tanamen = $this->service->paginated();
        return view('kelola_tanamen.index',compact('kelola_tanamen','user'));
    }

    
    public function search(Request $request)
    {
        $kelola_tanamen = $this->service->search($request->search);
        return view('kelola_tanamen.index')->with('kelola_tanamen', $kelola_tanamen);
    }

    
    public function create()
    {
        $perangkats = Perangkat::all();
        return view('kelola_tanamen.create')->with('perangkats', $perangkats);
    }

   
    public function store(KelolaTanamanCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('kelola_tanamen.index', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('kelola_tanamen.index'))->with('message', 'Failed to create');
    }

    //Manajemen Pohon Bicara
    public function pohonbicara(Request $request)
    {
        $user = $request->user()->id;
        $kelola_tanamen  = User::find($user)->kelolatanaman()->get();
        return view('kelola_tanamen.pohonbicara',compact('kelola_tanamen'));
    }

    //manajemen pohon bicara admin
     public function manajemenpohonbicara(Request $request)
    {
       $kelola_tanamen = $this->service->paginated();
        return view('kelola_tanamen.pohonbicara',compact('kelola_tanamen'));
    }
    //biar lebih slime harus nya pake elequent!!!!
    public function show(Request $request, $id)
    {
        $kelola_tanaman = $this->service->find($id);
        $user = $request->user()->id;
        $id = $id;
        
        //$foto = User::find($user)->kelolatanaman()->get();
        $datas = \DB::table('users')->join('kelola_tanamen', 'users.id', '=', 'kelola_tanamen.user_id')->join('perangkats', 'perangkats.id', '=', 'kelola_tanamen.perangkat_id' )->where('users.id', '=', $user)->where('kelola_tanamen.id', '=', $id)->get();

        //menampilkan semua data sensor sesuai perangkat user_id dan tanaman
        $sensors = \DB::table('users')->join('kelola_tanamen', 'users.id', '=', 'kelola_tanamen.user_id')->join('sensors','sensors.tanaman_id', '=' , 'kelola_tanamen.id')->join('perangkats', 'perangkats.id', '=', 'kelola_tanamen.perangkat_id')->where('users.id', '=', $user)->where('kelola_tanamen.id', '=', $id)->get();
            
        //menampilkan data sensor yang paling baru dengan limit 1
        $sensors1 = \DB::table('users')->join('kelola_tanamen', 'users.id', '=', 'kelola_tanamen.user_id')->join('sensors','sensors.tanaman_id', '=' , 'kelola_tanamen.id')->join('perangkats', 'perangkats.id', '=', 'kelola_tanamen.perangkat_id')->where('users.id', '=', $user)->where('kelola_tanamen.id', '=', $id)->orderby('sensors.id', 'DESC')->limit(1)->get(); 
             $kreteria = RangeSuhu::all();
            $kreteria_kelembapan = RangeKelembapan::all();
            $kreteria_phtanah = RangePhtanah::all();
            $kreteria_cahaya = RangeCahaya::all();

        if($request->user()->roles->first()->name = 'admin'){
             $datas = \DB::table('users')->join('kelola_tanamen', 'users.id', '=', 'kelola_tanamen.user_id')->join('perangkats', 'perangkats.id', '=', 'kelola_tanamen.perangkat_id' )->where('kelola_tanamen.id', '=', $id)->get();
             $datas = \DB::table('users')->join('kelola_tanamen', 'users.id', '=', 'kelola_tanamen.user_id')->join('perangkats', 'perangkats.id', '=', 'kelola_tanamen.perangkat_id' )->where('kelola_tanamen.id', '=', $id)->get();
            //menampilkan semua data sensor sesuai perangkat user_id dan tanaman
            $sensors = \DB::table('users')->join('kelola_tanamen', 'users.id', '=', 'kelola_tanamen.user_id')->join('sensors','sensors.tanaman_id', '=' , 'kelola_tanamen.id')->join('perangkats', 'perangkats.id', '=', 'kelola_tanamen.perangkat_id')->where('kelola_tanamen.id', '=', $id)->get();
            //menampilkan data sensor yang paling baru dengan limit 1
            $sensors1 = \DB::table('users')->join('kelola_tanamen', 'users.id', '=', 'kelola_tanamen.user_id')->join('sensors','sensors.tanaman_id', '=' , 'kelola_tanamen.id')->join('perangkats', 'perangkats.id', '=', 'kelola_tanamen.perangkat_id')->where('kelola_tanamen.id', '=', $id)->orderby('sensors.id', 'DESC')->limit(1)->get(); 
            //menampilkan data kreteria
            $kreteria = RangeSuhu::all();
            $kreteria_kelembapan = RangeKelembapan::all();
            $kreteria_phtanah = RangePhtanah::all();
            $kreteria_cahaya = RangeCahaya::all();

        }

        return view('kelola_tanamen.show', compact('datas','sensors','sensors1', 'id','kreteria',
            'kreteria_kelembapan','kreteria_phtanah','kreteria_cahaya'));
    }


    // get data sensor tanaman terbaru dari antares
    public function antares($id)
    {
        // cari tanaman berdasarkan id
        $tanaman = KelolaTanaman::find($id);
        $perangkat = KelolaTanaman::find($id)->perangkat;

        // cari link antares berdasarkan sensor

        // dapatkan data sensor di antares
            $client_aws = new Client();
                
           // get newest data from device

            $get_aws = $client_aws->get($perangkat->platform, [
                    'http_errors' => FALSE,
                    'headers'  => [
                        'X-M2M-Origin' => $perangkat->origin,
                        'Content-Type' => 'application/json;ty=4',
                        'accept' => 'application/json'
                    ]
            ]);

            $parsed_aws = json_decode($get_aws->getBody());
            $respon_aws = $get_aws->getStatusCode();
        // dd($parsed_aws);

            if ($respon_aws == 200) {
                $tahun = substr($parsed_aws->{'m2m:cin'}->{'ct'}, 0, 4);
                $bulan = substr($parsed_aws->{'m2m:cin'}->{'ct'}, 4, 2);
                $hari = substr($parsed_aws->{'m2m:cin'}->{'ct'}, 6, 2);
                $jam = substr($parsed_aws->{'m2m:cin'}->{'ct'}, 9, 2);
                $menit = substr($parsed_aws->{'m2m:cin'}->{'ct'}, 11, 2);
                $detik = substr($parsed_aws->{'m2m:cin'}->{'ct'}, 13, 2);
                $tgl_aws = $hari."/".$bulan."/".$tahun." ".$jam.":".$menit.":".$detik;
                $tgl = $tahun.'-'.$bulan.'-'.$hari.' '.$jam.':'.$menit.':'.$detik;
                //$aws = json_decode($json_aws); 
                $datas = json_decode($parsed_aws->{'m2m:cin'}->{'con'});
            
                $sensor = Sensor::orderBy('id', 'desc')->first();
            
                if ($sensor->waktu == $tgl) {
                    return redirect('kelola_tanamen/'.$id)->with('message', 'Belum ada data baru');
                }else{
                    // simpan ke database
                    $sensor = new Sensor;
                    $sensor->perangkat_id = $perangkat->id;
                    $sensor->tanaman_id = $id;
                    $sensor->user_id = $tanaman->user->id;
                    $sensor->ph_tanah = 4;
                    $sensor->kelembapan = $datas->Humidity;
                    $sensor->suhu = $datas->Temperature;
                    $sensor->cahaya = 17;
                    $sensor->tanggal = $tahun.'-'.$bulan.'-'.$hari;
                    $sensor->waktu = $tahun.'-'.$bulan.'-'.$hari.' '.$jam.':'.$menit.':'.$detik;

                    $sensor->save(); 
                    // kemballi ke kelola tanaman
                    return redirect('kelola_tanamen/'.$id)->with('message', 'data baru update');
                   

                }
               

            } 

            return redirect('kelola_tanamen/'.$id)->with('message', 'Belum ada data baru');
         
        
    }

    //END Manajemen Pohon Bicara

    public function edit($id)
    {
        $kelola_tanaman = $this->service->find($id);
        $perangkats = Perangkat::all();
        return view('kelola_tanamen.edit',compact('kelola_tanaman','perangkats'));
    }

    
    public function update(KelolaTanamanUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
             return redirect(route('kelola_tanamen.index'))->with('message', 'Sucsess  to create');
        }

        return back()->with('message', 'Failed to update');
    }

   
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('kelola_tanamen.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('kelola_tanamen.index'))->with('message', 'Failed to delete');
    }
}
