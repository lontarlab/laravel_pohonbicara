<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\NlpStemquestionService;
use App\Http\Requests\NlpStemquestionCreateRequest;
use App\Http\Requests\NlpStemquestionUpdateRequest;

class NlpStemquestionsController extends Controller
{
    public function __construct(NlpStemquestionService $nlp_stemquestion_service)
    {
        $this->service = $nlp_stemquestion_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $nlp_stemquestions = $this->service->paginated();
        return view('nlp_stemquestions.index')->with('nlp_stemquestions', $nlp_stemquestions);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $nlp_stemquestions = $this->service->search($request->search);
        return view('nlp_stemquestions.index')->with('nlp_stemquestions', $nlp_stemquestions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('nlp_stemquestions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\NlpStemquestionCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NlpStemquestionCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('nlp_stemquestions.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('nlp_stemquestions.index'))->with('message', 'Failed to create');
    }

    /**
     * Display the nlp_stemquestion.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $nlp_stemquestion = $this->service->find($id);
        return view('nlp_stemquestions.show')->with('nlp_stemquestion', $nlp_stemquestion);
    }

    /**
     * Show the form for editing the nlp_stemquestion.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $nlp_stemquestion = $this->service->find($id);
        return view('nlp_stemquestions.edit')->with('nlp_stemquestion', $nlp_stemquestion);
    }

    /**
     * Update the nlp_stemquestions in storage.
     *
     * @param  \Illuminate\Http\NlpStemquestionUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NlpStemquestionUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->with('message', 'Failed to update');
    }

    /**
     * Remove the nlp_stemquestions from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('nlp_stemquestions.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('nlp_stemquestions.index'))->with('message', 'Failed to delete');
    }
}
