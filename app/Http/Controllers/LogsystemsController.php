<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\LogsystemService;
use App\Http\Requests\LogsystemRequest;

class LogsystemsController extends Controller
{
    public function __construct(LogsystemService $logsystemService)
    {
        $this->service = $logsystemService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $logsystems = $this->service->paginated();
        return view('logsystems.index')->with('logsystems', $logsystems);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $logsystems = $this->service->search($request->search);
        return view('logsystems.index')->with('logsystems', $logsystems);
    }

    /**
     * Remove the logsystems from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('logsystems.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('logsystems.index'))->with('message', 'Failed to delete');
    }
}
