<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\SensorService;
use App\Services\PerangkatService;
use App\Http\Requests\SensorCreateRequest;
use App\Http\Requests\SensorUpdateRequest;
use App\Models\User;
use App\Models\Sensor;
use App\Models\KelolaTanaman;
use App\Models\Perangkat;
use App\Models\RangeSuhu;
use App\Models\RangeKelembapan;
use App\Models\RangePhtanah;
use App\Models\RangeCahaya;
use Carbon\Carbon;

class SensorsController extends Controller
{
    public function __construct(SensorService $sensor_service)
    {
        $this->service = $sensor_service;
    }

   
    public function index(Request $request)
    {   $user = $request->user()->id;
        $sensors = User::find($user)->kelolaSensor()->get();
        return view('sensors.index')->with('sensors', $sensors);
    }
    //fungsi untuk admin
    public function sensor(Request $request)
    {
       
          $sensors = Sensor::all();
          $kreteria = RangeSuhu::all();
          $kreteria_kelembapan = RangeKelembapan::all();
          $kreteria_phtanah = RangePhtanah::all();
          $kreteria_cahaya = RangeCahaya::all();
        foreach ($sensors as $sensor) {
            $jam1   = substr($sensor->waktu, 10, 3) * 3600;
            $menit1 = substr($sensor->waktu, 14, 2) * 60;
            $detik1 = substr($sensor->waktu, 17, 2);
            $waktu1 = $jam1 + $menit1 + $detik1; 
            $jam2  = substr($sensor->created_at, 10, 3) * 3600;
            $menit2 = substr($sensor->created_at, 14, 2) * 60;
            $detik2 = substr($sensor->created_at, 17, 2);
            $waktu2 = $jam2 + $menit2 + $detik2;

            $totalDetik = $waktu2 - $waktu1;
            $result4 = Sensor::where('id', $sensor->id)->update(['delay' => $totalDetik]);
       }
    
        return view('sensors.index', compact('sensors','kreteria',
            'kreteria_kelembapan','kreteria_phtanah','kreteria_cahaya'));
    }

    public function grafik(Request $request)
    {    
        $user = $request->user()->id;
        $sensors = User::find($user)->kelolaSensor()->get();
       return view('sensors.grafik')->with('sensors', $sensors);
    }

      public function grafik_waktu(Request $request)
    {    
        $sensors = Sensor::all();
    
       return view('sensors.grafik_waktu', compact('sensors'));
    }

    //grafik admin
    public function admingrafik(Request $request)
    {        
      $sensors = Sensor::all();
      return view('sensors.grafik')->with('sensors', $sensors);
    }

    //grafik perhari
    private function dateRange($time)
    {
        switch ($time) {
            case "day":
                $time = [Carbon::now()->subHours(24)->toDateTimeString(), Carbon::now()->toDateTimeString()];
                break;
            case "week":
                $time = [Carbon::now()->subDays(7)->toDateTimeString(), Carbon::now()->toDateTimeString()];
                break;
            case "month":
                $time = [Carbon::now()->subDays(30)->toDateTimeString(), Carbon::now()->toDateTimeString()];
                break;
            case "year":
                $time = [Carbon::now()->subDays(365)->toDateTimeString(), Carbon::now()->toDateTimeString()];
                break;
            default:
                $time = null;
        }
        return $time;
    }


    public function grafikhari(Request $request)
    {
        $range = $this->dateRange("day");
       // $user = $request->user()->id;
        $sensors = Sensor::whereBetween('created_at', $range)->orderBy('created_at','asc')->get();
        return view('sensors.grafik')->with('sensors', $sensors);
    }

     public function grafikhari2(Request $request)
    {
        $range = $this->dateRange("day");
       // $user = $request->user()->id;
        $sensors = Sensor::whereBetween('created_at', $range)->orderBy('created_at','asc')->get();
        return view('sensors.grafik_waktu')->with('sensors', $sensors);
    }

    public function grafikbulan(Request $request)
    {
        $range = $this->dateRange("month");
       // $user = $request->user()->id;
        $sensors =Sensor::whereBetween('created_at', $range)->orderBy('created_at','asc')->get();
        return view('sensors.grafik')->with('sensors', $sensors);
    }

    public function grafikbulan2(Request $request)
    {
        $range = $this->dateRange("month");
       // $user = $request->user()->id;
        $sensors =Sensor::whereBetween('created_at', $range)->orderBy('created_at','asc')->get();
        return view('sensors.grafik_waktu')->with('sensors', $sensors);
    }

    public function grafiktahun(Request $request)
    {
        $range = $this->dateRange("year");
       // $user = $request->user()->id;
        $sensors =Sensor::whereBetween('created_at', $range)->orderBy('created_at','asc')->get();
        return view('sensors.grafik')->with('sensors', $sensors);
    }

     public function grafiktahun2(Request $request)
    {
        $range = $this->dateRange("year");
       // $user = $request->user()->id;
        $sensors =Sensor::whereBetween('created_at', $range)->orderBy('created_at','asc')->get();
        return view('sensors.grafik_waktu')->with('sensors', $sensors);
    }

    public function search(Request $request)
    {
        $sensors = $this->service->search($request->search);
        return view('sensors.index')->with('sensors', $sensors);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sensors.create');
    }

    public function store(SensorCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('sensors.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('sensors.index'))->with('message', 'Failed to create');
    }


    public function show($id)
    {
        $sensor = $this->service->find($id);
        return view('sensors.show')->with('sensor', $sensor);
    }

    
    public function edit($id)
    {
        $sensor = $this->service->find($id);
        return view('sensors.edit')->with('sensor', $sensor);
    }

   
    public function update(SensorUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->with('message', 'Failed to update');
    }

    
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('sensors.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('sensors.index'))->with('message', 'Failed to delete');
    }
}
