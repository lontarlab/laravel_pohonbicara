<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\NlpPertanyaanuserService;
use App\Http\Requests\NlpPertanyaanuserCreateRequest;
use App\Http\Requests\NlpPertanyaanuserUpdateRequest;

class NlpPertanyaanusersController extends Controller
{
    public function __construct(NlpPertanyaanuserService $nlp_pertanyaanuser_service)
    {
        $this->service = $nlp_pertanyaanuser_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $nlp_pertanyaanusers = $this->service->paginated();
        return view('nlp_pertanyaanusers.index')->with('nlp_pertanyaanusers', $nlp_pertanyaanusers);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $nlp_pertanyaanusers = $this->service->search($request->search);
        return view('nlp_pertanyaanusers.index')->with('nlp_pertanyaanusers', $nlp_pertanyaanusers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('nlp_pertanyaanusers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\NlpPertanyaanuserCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NlpPertanyaanuserCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('nlp_pertanyaanusers.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('nlp_pertanyaanusers.index'))->with('message', 'Failed to create');
    }

    /**
     * Display the nlp_pertanyaanuser.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $nlp_pertanyaanuser = $this->service->find($id);
        return view('nlp_pertanyaanusers.show')->with('nlp_pertanyaanuser', $nlp_pertanyaanuser);
    }

    /**
     * Show the form for editing the nlp_pertanyaanuser.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $nlp_pertanyaanuser = $this->service->find($id);
        return view('nlp_pertanyaanusers.edit')->with('nlp_pertanyaanuser', $nlp_pertanyaanuser);
    }

    /**
     * Update the nlp_pertanyaanusers in storage.
     *
     * @param  \Illuminate\Http\NlpPertanyaanuserUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NlpPertanyaanuserUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->with('message', 'Failed to update');
    }

    /**
     * Remove the nlp_pertanyaanusers from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('nlp_pertanyaanusers.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('nlp_pertanyaanusers.index'))->with('message', 'Failed to delete');
    }
}
