<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use App\Models\Department;
use App\Models\KelolaTanaman;
use App\Models\Sensor;
use App\Models\User;
use App\Models\UserMeta;
use App\Models\Perangkat;
use App\Models\NlpKatadasar;
use App\Models\NlpStopword;
use App\Models\NlpStemquestion;
use App\Models\NlpPertanyaanuser;


class Apiv1Controller extends Controller
{
  public function __construct() {
    $this->middleware('jwt.auth',['except'=>['listtanaman', 'sensornewest', 'sensorhistory', 'postsensor', 'sensornew', 'getsuhu', 'getkelembapan', 'getkelembapantanah', 'getcahaya', 'sensorhistoryuser', 'userline', 'registerline', 'registerplant', 'registerperangkat', 'getplant', 'getperangkat', 'aktifnotif','postthreshold', 'getkatadasar', 'getstopword', 'getstemquestion', 'getpertanyaanuser', 'postpertanyaanuser', 'poststemuser', 'notifon', 'notifoff']]);
  }

  public function listdepartment()
  {


    $departments = Department::all();
    return $departments;
  }

  public function listtanaman()
  {


    $tanamans = KelolaTanaman::all();
    // return $tanamans;
    return response()->json(compact('tanamans'));
  }

  // Berdasarkan tanaman (buat handling 1 user banyak tanaman(device))
  public function sensornewest($id)
  {
    $sensors = Sensor::where('tanaman_id', $id)->orderBy('id', 'desc')->first();
    //return $sensors;
    return response()->json(compact('sensors'));
  }

  // Berdasarkan User (karena 1 user 1 tanaman)
  public function sensornew($id)
  {
    $sensor = Sensor::where('user_id', $id)->orderBy('id', 'desc')->first();
    $spk = "tidak ada";;

          // Tanaman Normal
      if($sensor->suhu > 20 &&  $sensor->suhu < 35 && $sensor->kelembapan >= 70 && $sensor->kelembapan < 85 && $sensor->cahaya > 700)
          $spk = 'Keadaan bercahaya, suhu dan kelembapan normal';
      elseif($sensor->suhu > 18 &&  $sensor->suhu < 40 && $sensor->kelembapan >= 60 && $sensor->kelembapan < 85 && $sensor->cahaya < 700)
            $spk = 'Keadaan tidak bercahaya, suhu dan kelembapan normal';
           // <!-- Jangan Siram Tanaman   -->
      elseif( $sensor->suhu > 18 && $sensor->kelembapan < 65)
          $spk ='suhu meningkat dan kelembapan tanah kering, segera siram tanaman';
          // <!-- Siram Tanaman  -->
      elseif($sensor->kelembapan > 85)
           $spk = 'Kelembapan tanah masih sangat basah, disarankan tidak untuk menyiram tanaman';

    //return $sensors;
    return response()->json(compact('sensor', 'spk'));
  }

  // get suhu
  public function getsuhu($id)
  {
    $sensor = Sensor::select('suhu')->where('user_id', $id)->orderBy('id', 'desc')->first();
    $spk = "tidak ada";

    // spk suhu
    if($sensor->suhu > 0 && $sensor->suhu <= 5)
      $spk = 'Suhu Sangat Rendah'; 
    elseif($sensor->suhu > 5 && $sensor->suhu <= 10)
      $spk = 'Suhu Lumanyan Rendah'; 
    elseif($sensor->suhu > 10 && $sensor->suhu <= 15)
      $spk = 'Suhu Mulai Rendah'; 
    elseif($sensor->suhu > 15 && $sensor->suhu  <= 20 )
      $spk = 'Suhu Mulai sedang'; 
    elseif($sensor->suhu > 20 && $sensor->suhu <= 25)
      $spk = 'Suhu Sedang';
    elseif($sensor->suhu  > 25 && $sensor->suhu <= 30)
      $spk = 'Suhu mulai panas';
    elseif($sensor->suhu  > 30 && $sensor->suhu <= 35)
      $spk = 'Suhu Panas';
    elseif($sensor->suhu > 35 && $sensor->suhu <= 40)
      $spk = 'Suhu sangat panas';


    //return $sensors;
    return response()->json(compact('sensor', 'spk'));
  }

  // get kelembaban udara
  public function getkelembapan($id)
  {
    $sensor = Sensor::select('kelembapan')->where('user_id', $id)->orderBy('id', 'desc')->first();
    $spk = "tidak ada";

    // spk kelembapan
    if($sensor->kelembapan > 0 && $sensor->kelembapan <= 20)
      $spk = 'Kelembapan Udara Sangat Kering';
    elseif($sensor->kelembapan > 20 && $sensor->kelembapan <= 30)
      $spk = 'Kelembapan Udara  Kering';
    elseif($sensor->kelembapan > 30 && $sensor->kelembapan <= 50)
      $spk = 'Kelembapan Udara Agak Kering';
    elseif($sensor->kelembapan > 50 && $sensor->kelembapan  <= 70 )
      $spk = 'Kelembapan Udara Sedikit Basah';
    elseif($sensor->kelembapan > 70 && $sensor->kelembapan <= 85)
      $spk = 'Kelembapan Udara Basah';
    elseif($sensor->kelembapan  > 85 && $sensor->kelembapan <= 100)
      $spk = 'Kelembapan Udara Sangat  Basah';
    

    //return $sensors;
    return response()->json(compact('sensor', 'spk'));
  }

  // get kelembaban tanah
    public function getkelembapantanah($id)
  {
    $sensor = Sensor::select('ph_tanah')->where('user_id', $id)->orderBy('id', 'desc')->first();

    $spk = "tidak ada";

    // spk kelembapan
    if($sensor->ph_tanah > 0 && $sensor->ph_tanah <= 20)
      $spk = 'Kelembapan Tanah Sangat Kering';
    elseif($sensor->ph_tanah > 20 && $sensor->ph_tanah <= 30)
      $spk = 'Kelembapan Tanah  Kering';
    elseif($sensor->ph_tanah > 30 && $sensor->ph_tanah <= 50)
      $spk = 'Kelembapan Tanah Agak Kering';
    elseif($sensor->ph_tanah > 50 && $sensor->ph_tanah  <= 70 )
      $spk = 'Kelembapan Tanah Sedikit Basah';
    elseif($sensor->ph_tanah > 70 && $sensor->ph_tanah <= 85)
      $spk = 'Kelembapan Tanah Basah';
    elseif($sensor->ph_tanah  > 85 && $sensor->ph_tanah <= 100)
      $spk = 'Kelembapan Tanah Sangat  Basah';
    

    //return $sensors;
    return response()->json(compact('sensor', 'spk'));
  }

  // get keadaan cahaya
    public function getcahaya($id)
  {
    $sensor = Sensor::select('cahaya')->where('user_id', $id)->orderBy('id', 'desc')->first();
    $spk = "tidak ada";
    if ($sensor->cahaya == 1) {
      $spk = "Keadaan Ruangan Terang";
    }else{
      $spk = "Keadaan Ruangan Gelap";
    }

    //return $sensors;
    return response()->json(compact('sensor', 'spk'));
  }

  public function sensorhistory($id)
  {
    
    $sensors = Sensor::where('tanaman_id', $id)->orderBy('id', 'desc')->get();
    //return $sensors;
    return response()->json(compact('sensors'));
  }

  public function sensorhistoryuser($id)
  {
    

    $sensors = Sensor::where('user_id', $id)->orderBy('id', 'desc')->get();
    //return $sensors;
    return response()->json(compact('sensors'));
  }

  public function postsensor(Request $request)
  {

      $sensor = new Sensor;
  
      $sensor->ph_tanah = $request->ph_tanah;
      $sensor->kelembapan = $request->kelembapan;
      $sensor->suhu = $request->suhu;
      $sensor->cahaya = $request->cahaya;
      $sensor->waktu = $request->waktu;
      // $sensor->tanggal = $request->tanggal;

      $sensor->tanaman_id = 1;
      $sensor->user_id = 15;
      $sensor->perangkat_id = 1;
      // belum di sensornya kirim sesuai id line

      // delay 
        // substr($sensor->created_at ,10) 
        // $jam1   = substr($sensor->waktu, 10, 3) * 3600;
        // $menit1 = substr($sensor->waktu, 14, 2) * 60;
        // $detik1 = substr($sensor->waktu, 17, 2);
        // $waktu1 = $jam1 + $menit1 + $detik1; 

        // $jam2  = substr($sensor->created_at, 10, 3) * 3600;
        // $menit2 = substr($sensor->created_at, 14, 2) * 60;
        // $detik2 = substr($sensor->created_at, 17, 2);
        // $waktu2 = $jam2 + $menit2 + $detik2;
        // $totalDetik = abs($waktu2 - $waktu1);
                        
        // $jam = abs($totalDetik / 3600);
        // $modMenit = $totalDetik % 3600;
        // $menit    = abs($modMenit / 60);
        // $detik   = abs($menit / 60);

        // number_format($jam, 0) // jam 
        // number_format($menit, 0)  // menit
        // number_format($detik, 0)  // Detik 
      //

      $sensor->save();

      

      if($request->suhu > 20 &&  $request->suhu < 35 && $request->kelembapan >= 70 && $request->kelembapan < 85 && $sensor->cahaya > 0){
        $spk = 'Keadaan bercahaya, suhu dan kelembapan normal';
      }
      elseif($request->suhu > 18 &&  $request->suhu < 40 && $request->kelembapan >= 60 && $request->kelembapan < 85 && $sensor->cahaya < 1){
        $spk = 'Keadaan tidak bercahaya, suhu dan kelembapan normal';
      }
      elseif( $request->suhu > 18 && $request->kelembapan < 65){
         // send to api CI Linebot
          $client_aws = new Client();

          $isi = array(
                  'suhu' => $request->suhu,
                  'kelembapan' => $request->kelembapan,
                  'ph_tanah' => $request->ph_tanah,
                  'cahaya' => $request->cahaya,
                  'waktu'  => $request->waktu,
                  'spk'    => $spk
                );
          $isi = json_encode($isi);
     
          $get_aws = $client_aws->post('https://pohonbicara.herokuapp.com/index.php/users/pushmessage', [
                    'http_errors' => FALSE,
                    'body' => $isi,
                    'headers'  => [
                        // 'X-M2M-Origin' => '40459b3e0e98f6c3:65773a486d3e1c28',
                        'Content-Type' => 'application/json;ty=4',
                        'accept' => 'application/json'
                    ],
                      
          ]);
          $spk ='suhu meingkat dan kelembapan tanah kering, segera siram tanaman';
      }
      elseif($request->kelembapan > 85){
        $spk = 'Kelembapan tanah masih sangat basah, disarankan tidak untuk menyiram tanaman';
      }
      
      $perangkat = Perangkat::find(1);

      if ($perangkat->notifikasi == 1) {
        $client_aws = new Client();
            $cahaya = "Gelap";

            if ($request->cahaya == 0) {
              $cahaya = "Gelap";
            }else{
              $cahaya = "Terang";
            }

            $isi = array(
                    'suhu' => $request->suhu,
                    'kelembapan' => $request->kelembapan,
                    'ph_tanah' => $request->ph_tanah,
                    'cahaya' => $cahaya,
                    'waktu'  => $request->waktu,
                    'spk'    => $spk 
                  );
            $isi = json_encode($isi);
       
            $get_aws = $client_aws->post('https://pohonbicara.herokuapp.com/index.php/users/pushmessage', [
                      'http_errors' => FALSE,
                      'body' => $isi,
                      'headers'  => [
                          // 'X-M2M-Origin' => '40459b3e0e98f6c3:65773a486d3e1c28',
                          'Content-Type' => 'application/json;ty=4',
                          'accept' => 'application/json'
                      ],
                        
            ]);
      }

        // kirim push message ketika spk terlalu tinggi atau rendah
          // Keseluruhan
          // suhu
          // kelembapan
          // kelembapan tanah
          // cahaya

        // kirim push message sesuai jadwal

  }

  public function postsensorchat(Request $request)
  {

      $perangkat = Perangkat::find(1);

      if ($perangkat->sent_chatbot == 1) {
            $perangkat = Perangkat::find(1);
            $perangkat->sent_chatbot = 0;
            $perangkat->save();


            $client_aws = new Client();
            $cahaya = "Gelap";

            if ($request->cahaya == 0) {
              $cahaya = "Gelap";
            }else{
              $cahaya = "Terang";
            }

            $isi = array(
                    'suhu' => $request->suhu,
                    'kelembapan' => $request->kelembapan,
                    'ph_tanah' => $request->ph_tanah,
                    'cahaya' => $cahaya,
                    'waktu'  => $request->waktu,
                    'spk'    => $spk 
                  );
            $isi = json_encode($isi);
       
            $get_aws = $client_aws->post('https://pohonbicara.herokuapp.com/index.php/users/pushmessage', [
                      'http_errors' => FALSE,
                      'body' => $isi,
                      'headers'  => [
                          // 'X-M2M-Origin' => '40459b3e0e98f6c3:65773a486d3e1c28',
                          'Content-Type' => 'application/json;ty=4',
                          'accept' => 'application/json'
                      ],
                        
            ]);

            $sensor = new SensorChat;
  
            $sensor->kelembapan_tanah = $request->ph_tanah;
            $sensor->kelembapan = $request->kelembapan;
            $sensor->suhu = $request->suhu;
            $sensor->cahaya = $request->cahaya;
            $sensor->waktu = $request->waktu;
            // $sensor->tanggal = $request->tanggal;

            $sensor->tanaman_id = 1;
            $sensor->user_id = 15;
            $sensor->perangkat_id = 1;
            // belum di sensornya kirim sesuai id line

            // delay 
              // substr($sensor->created_at ,10) 
              // $jam1   = substr($sensor->waktu, 10, 3) * 3600;
              // $menit1 = substr($sensor->waktu, 14, 2) * 60;
              // $detik1 = substr($sensor->waktu, 17, 2);
              // $waktu1 = $jam1 + $menit1 + $detik1; 

              // $jam2  = substr($sensor->created_at, 10, 3) * 3600;
              // $menit2 = substr($sensor->created_at, 14, 2) * 60;
              // $detik2 = substr($sensor->created_at, 17, 2);
              // $waktu2 = $jam2 + $menit2 + $detik2;
              // $totalDetik = abs($waktu2 - $waktu1);
                              
              // $jam = abs($totalDetik / 3600);
              // $modMenit = $totalDetik % 3600;
              // $menit    = abs($modMenit / 60);
              // $detik   = abs($menit / 60);

              // number_format($jam, 0) // jam 
              // number_format($menit, 0)  // menit
              // number_format($detik, 0)  // Detik 
            
            $sensor->save();

            

            if($request->suhu > 20 &&  $request->suhu < 35 && $request->kelembapan >= 70 && $request->kelembapan < 85 && $sensor->cahaya > 0){
              $spk = 'Keadaan bercahaya, suhu dan kelembapan normal';
            }
            elseif($request->suhu > 18 &&  $request->suhu < 40 && $request->kelembapan >= 60 && $request->kelembapan < 85 && $sensor->cahaya < 1){
              $spk = 'Keadaan tidak bercahaya, suhu dan kelembapan normal';
            }
            elseif( $request->suhu > 18 && $request->kelembapan < 65){
               // send to api CI Linebot
                $client_aws = new Client();

                $isi = array(
                        'suhu' => $request->suhu,
                        'kelembapan' => $request->kelembapan,
                        'ph_tanah' => $request->ph_tanah,
                        'cahaya' => $request->cahaya,
                        'waktu'  => $request->waktu,
                        'spk'    => $spk
                      );
                $isi = json_encode($isi);
           
                $get_aws = $client_aws->post('https://pohonbicara.herokuapp.com/index.php/users/pushmessage', [
                          'http_errors' => FALSE,
                          'body' => $isi,
                          'headers'  => [
                              // 'X-M2M-Origin' => '40459b3e0e98f6c3:65773a486d3e1c28',
                              'Content-Type' => 'application/json;ty=4',
                              'accept' => 'application/json'
                          ],
                            
                ]);
                $spk ='suhu meingkat dan kelembapan tanah kering, segera siram tanaman';
            }
            elseif($request->kelembapan > 85){
              $spk = 'Kelembapan tanah masih sangat basah, disarankan tidak untuk menyiram tanaman';
            }
      }

        // kirim push message ketika spk terlalu tinggi atau rendah
          // Keseluruhan
          // suhu
          // kelembapan
          // kelembapan tanah
          // cahaya

        // kirim push message sesuai jadwal

  }

  // get user line
  public function userline($id)
  {
    $user = User::where('id_line', $id)->first();
    return response()->json(compact('user'));
  }

  // register via line (belum per satu (apakah disimpan dulu di codeigniter ?))
  public function registerline(Request $request)
  {
      $user = new User;
  
      $user->name = $request->name;
      $user->email = $request->email;
      $user->id_line = $request->id_line;
      $user->password = bcrypt($request->password);
     
      $user->save();
    
      $userreg = User::where('id_line', $user->id_line)->first();

      $usermeta = new UserMeta;

      $usermeta->user_id = $userreg->id;
      $usermeta->is_active = 1;
      $usermeta->marketing = 1;
      $usermeta->terms_and_cond = 1;

      $usermeta->save();
  }

  // daftar tanaman baru
  public function registerplant(Request $request)
  {
    $tanaman = new KelolaTanaman;
    // pake id line
    $tanaman->user_id = $request->user_id;
    $tanaman->jenis_tanaman = $request->jenis_tanaman;
    $tanaman->nama_tanaman = $request->nama_tanaman;
    $tanaman->perangkat_id = $request->perangkat_id;

    $tanaman->save();

  }

  // get plant per user
  public function getplant($id)
  {
    $tanaman = KelolaTanaman::where('user_id', $id)->orderBy('id', 'desc')->first();
    //return $sensors;
    return response()->json(compact('tanaman'));
  }

  // daftar perangkat baru
  public function registerperangkat(Request $request)
  {
    $perangkat = new Perangkat;
    // pake id line
    $perangkat->user_id = $request->user_id;
    $perangkat->platform = $request->platform;
    $perangkat->origin = $request->origin;
    $perangkat->content = $request->content;
    $perangkat->keterangan = $request->keterangan;
    $perangkat->tanaman_id = $request->tanaman_id;
    $perangkat->save();
  }

  // get sensor(perangkat)
  public function getperangkat($id) 
  {
    $perangkat = Perangkat::where('user_id', $id)->orderBy('id', 'desc')->first();
    //return $sensors;
    return response()->json(compact('perangkat'));
  }

  // aktif non aktif notifikasi
  public function aktifnotif(Request $request)
  {
    $user = User::where('id_line', $request->id_line)->first();

    $user->notifikasi = $request->notifikasi;

    $user->save();
  }

  // get Kata Dasar
  public function getkatadasar($katadasar='')
  {
    $katadasar = NlpKatadasar::where('katadasar', $katadasar)->orderBy('id', 'desc')->first();
    //return $sensors;
    
    if($katadasar != NULL){
      // return response()->json(compact('katadasar'));
      return 1;
    }else{
      // $katadasar = FALSE;
      // return response()->json(compact('katadasar'));
      return 0;
    } 
  }

  //get stopwords
  public function getstopword($kata_stopwords='')
  {
    $stopword = NlpStopword::where('kata_stopwords', $kata_stopwords)->orderBy('id', 'desc')->first();
    //return $sensors;
    if($stopword != NULL){
      // return response()->json(compact('stopword'));
      return 1;
    }else{
      $stopword = FALSE;
      // return response()->json(compact('stopword'));
      return 0;
    } 
    
  }

  //getstemquestion
  public function getstemquestion($kata='')
  {
    $stopword = NlpStemquestion::where('stem_question', $kata)->orderBy('id', 'desc')->first();
    //return $sensors;
    
    if($stopword != NULL){
      return response()->json(compact('stopword'));
      // return 1;
    }else{
      $stopword = FALSE;
      // return response()->json(compact('stopword'));
      return 0;
    } 
  }

  public function getpertanyaanuser($pertanyaan='')
  {
    $pertanyaan = NlpPertanyaanuser::where('pertanyaan', $pertanyaan)->orderBy('id', 'desc')->first();
    //return $sensors;
    return response()->json(compact('pertanyaan'));
  }

  public function postpertanyaanuser(Request $request)
  {
      $question = new NlpPertanyaanuser;
  
      $question->pertanyaan = $request->pertanyaan;
    
      $question->save();
  }

  public function poststemuser(Request $request)
  {
      $question = new NlpStemquestion;
  
      $question->stem_question = $request->stem_question;
    
      $question->save();
  }

  // post threshold antares
  public function postthreshold(Request $request)
  {
    $client_aws = new Client();

         $isi = array('m2m:cin' =>  array(
            'con' => "{\"th01_min\":\"".$request->th01_min."\",\"th01_max\":\"".$request->th01_max."\",\"th02_min\":\"".$request->th02_min."\",\"th02_max\":\"".$request->th02_max."\",\"th03_min\":\"".$request->th03_min."\",\"th03_max\":\"".$request->th03_max."\"}" ));
         $isi = json_encode($isi);

        //POST DATA 

         $post_antares = $client_aws->post('https://platform.antares.id:8443/~/antares-cse/antares-id/GardenCanggih/simpanthreshold_01', [
                'http_errors' => FALSE,
                'body' => $isi,
                'headers'  => [
                    'X-M2M-Origin' => '40459b3e0e98f6c3:65773a486d3e1c28',
                    'Content-Type' => 'application/json;ty=4',
                    'accept' => 'application/json'
                ],
                  
        ]);

        return "Berhasil";
  }

  public function notifon()
  {
      $perangkat = Perangkat::find(1);
  
      $perangkat->notifikasi = 1;
    
      $perangkat->save();
  }

  public function notifoff()
  {
      $perangkat = Perangkat::find(1);
  
      $perangkat->notifikasi = 0;
    
      $perangkat->save();
  }


}
