<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\SensorChatService;
use App\Http\Requests\SensorChatCreateRequest;
use App\Http\Requests\SensorChatUpdateRequest;

class SensorChatsController extends Controller
{
    public function __construct(SensorChatService $sensor_chat_service)
    {
        $this->service = $sensor_chat_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sensor_chats = $this->service->paginated();
        return view('sensor_chats.index')->with('sensor_chats', $sensor_chats);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $sensor_chats = $this->service->search($request->search);
        return view('sensor_chats.index')->with('sensor_chats', $sensor_chats);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sensor_chats.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\SensorChatCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SensorChatCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('sensor_chats.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('sensor_chats.index'))->with('message', 'Failed to create');
    }

    /**
     * Display the sensor_chat.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sensor_chat = $this->service->find($id);
        return view('sensor_chats.show')->with('sensor_chat', $sensor_chat);
    }

    /**
     * Show the form for editing the sensor_chat.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sensor_chat = $this->service->find($id);
        return view('sensor_chats.edit')->with('sensor_chat', $sensor_chat);
    }

    /**
     * Update the sensor_chats in storage.
     *
     * @param  \Illuminate\Http\SensorChatUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SensorChatUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->with('message', 'Failed to update');
    }

    /**
     * Remove the sensor_chats from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('sensor_chats.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('sensor_chats.index'))->with('message', 'Failed to delete');
    }
}
