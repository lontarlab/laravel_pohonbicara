<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\NlpKatadasarService;
use App\Http\Requests\NlpKatadasarCreateRequest;
use App\Http\Requests\NlpKatadasarUpdateRequest;

class NlpKatadasarsController extends Controller
{
    public function __construct(NlpKatadasarService $nlp_katadasar_service)
    {
        $this->service = $nlp_katadasar_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $nlp_katadasars = $this->service->paginated();
        return view('nlp_katadasars.index')->with('nlp_katadasars', $nlp_katadasars);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $nlp_katadasars = $this->service->search($request->search);
        return view('nlp_katadasars.index')->with('nlp_katadasars', $nlp_katadasars);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('nlp_katadasars.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\NlpKatadasarCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NlpKatadasarCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('nlp_katadasars.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('nlp_katadasars.index'))->with('message', 'Failed to create');
    }

    /**
     * Display the nlp_katadasar.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $nlp_katadasar = $this->service->find($id);
        return view('nlp_katadasars.show')->with('nlp_katadasar', $nlp_katadasar);
    }

    /**
     * Show the form for editing the nlp_katadasar.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $nlp_katadasar = $this->service->find($id);
        return view('nlp_katadasars.edit')->with('nlp_katadasar', $nlp_katadasar);
    }

    /**
     * Update the nlp_katadasars in storage.
     *
     * @param  \Illuminate\Http\NlpKatadasarUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NlpKatadasarUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->with('message', 'Failed to update');
    }

    /**
     * Remove the nlp_katadasars from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('nlp_katadasars.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('nlp_katadasars.index'))->with('message', 'Failed to delete');
    }
}
