<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Spam;
use App\Models\Zona;
use App\Models\Dma;
use App\Models\PelangganDrd; 
use App\Models\Waterbalance;

use DB;

class NrwsController extends Controller
{

    public function gisdma(Request $request)
    {
      // Contoh DMA sementara 1400001 dan 1900001
      $dmas = Dma::where('lat', '!=', null)->where('long', '!=', null)->get();
      return view('nrws.gisdma',compact('dmas'));
    }

    public function waterbalance(Request $request)
    {

      // Siapkan dropdown untuk filter
      $spams = Spam::select('name', DB::raw("CONCAT(`kode`, ' - ', `name`) AS name"),'id')->get();
      $zonas = Zona::select('name', DB::raw("CONCAT(`kode`, ' - ', `name`) AS name"),'id')->get();
      $dmas = Dma::select('name', DB::raw("CONCAT(`kode`, ' - ', `name`) AS name"),'id')->get();
      // Inisiasi/memastikan saat dropdown belum dipilih nilai harus 0
      $spamselect = collect(new Spam);
      $spamselect->id = null;
      $zonaselect = collect(new Zona);
      $zonaselect->id = null;
      $dmaselect = collect(new Dma);
      $dmaselect->id = null;

      $meterawal = 0;
      $meterakhir = 0;
      $totalpemakaian = 0;
      $totalkubikinput = 1000 / 2;
      $totalkubikrevenuewater = 870 / 2;
      // dd($request->dma_id);
      if($request->dma_id != null) {

        $dmaselect = DMA::find($request->dma_id);
          // Mining dari tabel measurements data DMA tersebut
        $measurements = DB::select("SELECT COALESCE(YEAR(m.date), '0') as year,
          COALESCE(MONTH(m.date) - 1, '0') as month,
          COALESCE(DAY(m.date), '0') as day,
          COALESCE(HOUR(m.date), '0') as hour,
          COALESCE(MINUTE(m.date),'0') as minute,
          ROUND(CASE WHEN d.label='Flow' THEN m.value END, 2) AS flow,
          ROUND(CASE WHEN d.label='Pressure' THEN m.value END, 2) AS pressure,
          m.meter_index
          FROM loggers l LEFT JOIN devices d ON l.id = d.logger_id LEFT JOIN measurements m ON d.id=m.device_id
          WHERE l.dma_id = '".$request->dma_id."' ORDER BY date DESC, device_id LIMIT 544;");
          // Itungan kasar
          $start_year = '2018';
          $start_month = '1';
          $start_date = '18';
          $end_year = '2018';
          $end_month = '1';
          $end_date = '28';
          
          foreach ($measurements as $index => $data) {
            if($data->flow){
              if(($data->year==$start_year)and($data->month==$start_month)and($data->day==$start_date)) { $meterawal=$data->meter_index; }
              if(($data->year==$end_year)and($data->month==$end_month)and($data->day==$end_date)) { $meterakhir=$data->meter_index; }
            }
          }

           $totalkubikasi = ($meterakhir - $meterawal)*1.5;
           //   $i=Dma::find($request->dma_id)->pelanggans->count()-1;
           //    // Mining tabel billing, hitung pemakaian DRD untuk DMA tersebut
           //  $stankini=0;
           //  $stanlalu=0;
           //  for($i;$i>=0;$i--)
           //  {
           //   $stankini=Dma::find($request->dma_id)->pelanggans[$i]->drd[0]->rek_stankini+$stankini;
           //   $stanlalu=Dma::find($request->dma_id)->pelanggans[$i]->drd[0]->rek_stanlalu+$stanlalu;
           // }
           // $totalpemakaian = $stankini-$stanlalu;
            $tm_drd=DB::select("SELECT
              SUM(tm_drd_awal.rek_stankini-tm_drd_awal.rek_stanlalu) AS konsumsi_drd
              FROM
              dmas
              JOIN pelanggan_dmas ON dmas.id=pelanggan_dmas.dma_id
              JOIN tm_drd_awal ON pelanggan_dmas.pel_no=tm_drd_awal.pel_no
              WHERE tm_drd_awal.rek_bln=4 AND tm_drd_awal.rek_thn=2018 and dmas.id='".$request->dma_id."'
              GROUP BY dmas.id ;");
            foreach ($tm_drd as $index => $data)
            {
              $totalpemakaian=$data->konsumsi_drd;
            } 

            // if($totalpemakaian == 0){
            //   $totalpemakaian = 1;
            // }

            // if($totalkubikasi == 0){
            //   $totalkubikasi = 1;
            // }
            $input = "Logger";

            // if($totalpemakaian == 0){
            //     $pemakaian = Waterbalance::where('dma_id',$request->dma_id)->first();
            //     $totalpemakaian = $pemakaian->air_berekening;  
            //     $input = "Manual";
            //     // dd($totalpemakaian);            
            // }

            // handling 
            // insert data bulan dan tahun baru (yang baru di searching)

            // insert new dma baru jika request ada 


            // volume input
            if($totalkubikasi == 0){
                $kubikasi = Waterbalance::where('dma_id',$request->dma_id)->first();
                $totalkubikasi = $kubikasi->volume_input;
                $input = "Manual";
                
            }

            $waterbalance= Waterbalance::where('dma_id', $request->dma_id)->firstOrFail();
            $waterbalance = ($waterbalance->id != null ? $waterbalance : 0);
       
          return view('nrws.waterbalance',compact('spams','spamselect', 'zonas','zonaselect','dmas','dmaselect','totalkubikinput','totalkubikrevenuewater','totalkubikasi','totalpemakaian', 'waterbalance', 'input', 'input2'));


      } else {
        return view('nrws.waterbalance',compact('spams','spamselect', 'zonas','zonaselect','dmas','dmaselect'));

      }

    }

  public function chartmeasurement(Request $request)
  {
    // Siapkan dropdown untuk filter
    $spams = Spam::select('name', DB::raw("CONCAT(`kode`, ' - ', `name`) AS name"),'id')->get();
    $zonas = Zona::select('name', DB::raw("CONCAT(`kode`, ' - ', `name`) AS name"),'id')->get();
    $dmas = Dma::select('name', DB::raw("CONCAT(`kode`, ' - ', `name`) AS name"),'id')->get();
    // Inisiasi/memastikan saat dropdown belum dipilih nilai harus 0
    $spamselect = collect(new Spam);
    $spamselect->id = null;
    $zonaselect = collect(new Zona);
    $zonaselect->id = null;
    $dmaselect = collect(new Dma);
    $dmaselect->id = -1;
    // Dropdown Position untuk handling Logger non-lokasi (jdu,prv dll) coming soon
    // $positionselect= collect(new Logger);
    // $positionselect=null;

  if($request->spam_id > null) {
      $spamselect = SPAM::find($request->spam_id);
          // Mining dari tabel measurements data DMA tersebut
      $measurements = DB::select("SELECT COALESCE(YEAR(m.date), '0') as year,
       COALESCE(MONTH(m.date) - 1, '0') as month,
       COALESCE(DAY(m.date), '0') as day,
       COALESCE(HOUR(m.date), '0') as hour,
       COALESCE(MINUTE(m.date),'0') as minute,
       ROUND(CASE WHEN d.label='Flow' THEN m.value END, 2) AS flow,
       ROUND(CASE WHEN d.label='Pressure' THEN m.value END, 2) AS pressure,
       ROUND(CASE WHEN d.label='Totalizer' THEN m.value END, 2) AS totalizer,
       m.meter_index
       FROM loggers l LEFT JOIN devices d ON l.id = d.logger_id LEFT JOIN measurements m ON d.id=m.device_id
       WHERE l.spam_id = '".$request->spam_id."' ORDER BY date DESC, device_id LIMIT 544;");
      $pressures = "";
      $flows = "";
      $totalizers="";
      $flow_temp=0;
      $totalizer_temp=0;
      $meterindex_temp=0;
      foreach ($measurements as $index => $data) {
            // Jika ada value pressure (brarti row pressure)
        if($data->pressure){
          $pressures = $pressures .  "[Date.UTC(".$data->year.",".$data->month.",".$data->day.",".$data->hour.",".$data->minute."),".$data->pressure."],";
              // Masukan flow & meter (dari temp sebelumnya) ke row pressure
          $data->flow = $flow_temp;
          $data->meter_index = $meterindex_temp;
        } else {
          if ($data->flow) {
            // Jika tidak ada value pressure (brarti row flow)
            $flows = $flows . "[Date.UTC(".$data->year.",".$data->month.",".$data->day.",".$data->hour.",".$data->minute."),".$data->flow."],";
            // Simpan flow & meter ke temp untuk diinject ke row pressure di iterasi berikutnya
            $flow_temp = $data->flow;
            $meterindex_temp = $data->meter_index;
            // Hapus row flow yang ini/sekarang
            unset($measurements[$index]);
          } else {
            // Jika tidak ada value pressure (brarti row flow)
            $totalizers = $totalizers . "[Date.UTC(".$data->year.",".$data->month.",".$data->day.",".$data->hour.",".$data->minute."),".$data->totalizer."],";
            // Simpan flow & meter ke temp untuk diinject ke row pressure di iterasi berikutnya
            $totalizer_temp = $data->totalizer;
            $meterindex_temp = $data->meter_index;
            // Hapus row flow yang ini/sekarang
            unset($measurements[$index]);
          }
       }
     }
     // Trim/hilangkan koma sisa terakhir
     $pressures = rtrim($pressures,", ");
     $flows = rtrim($flows,", ");
     // Re-indexing array $measurements yang bolong bolong
     $measurements=array_values($measurements);
     
     return view('nrws.chartmeasurement_spam',compact('spams','spamselect', 'zonas','zonaselect','dmas','dmaselect','pressures','flows','measurements'));
  }

  if($request->dma_id > null) {
      $dmaselect = DMA::find($request->dma_id);
          // Mining dari tabel measurements data DMA tersebut
      $measurements = DB::select("SELECT COALESCE(YEAR(m.date), '0') as year,
       COALESCE(MONTH(m.date) - 1, '0') as month,
       COALESCE(DAY(m.date), '0') as day,
       COALESCE(HOUR(m.date), '0') as hour,
       COALESCE(MINUTE(m.date),'0') as minute,
       ROUND(CASE WHEN d.label='Flow' THEN m.value END, 2) AS flow,
       ROUND(CASE WHEN d.label='Pressure' THEN m.value END, 2) AS pressure,
       ROUND(CASE WHEN d.label='Totalizer' THEN m.value END, 2) AS totalizer,
       m.meter_index
       FROM loggers l LEFT JOIN devices d ON l.id = d.logger_id LEFT JOIN measurements m ON d.id=m.device_id
       WHERE l.dma_id = '".$request->dma_id."' ORDER BY date DESC, device_id LIMIT 544;");
      $pressures = "";
      $flows = "";
      $totalizers="";
      $flow_temp=0;
      $totalizer_temp=0;
      $meterindex_temp=0;
      foreach ($measurements as $index => $data) {
            // Jika ada value pressure (brarti row pressure)
        if($data->pressure){
          $pressures = $pressures .  "[Date.UTC(".$data->year.",".$data->month.",".$data->day.",".$data->hour.",".$data->minute."),".$data->pressure."],";
              // Masukan flow & meter (dari temp sebelumnya) ke row pressure
          $data->flow = $flow_temp;
          $data->meter_index = $meterindex_temp;
        } else {
          if ($data->flow) {
            // Jika tidak ada value pressure (brarti row flow)
            $flows = $flows . "[Date.UTC(".$data->year.",".$data->month.",".$data->day.",".$data->hour.",".$data->minute."),".$data->flow."],";
            // Simpan flow & meter ke temp untuk diinject ke row pressure di iterasi berikutnya
            $flow_temp = $data->flow;
            $meterindex_temp = $data->meter_index;
            // Hapus row flow yang ini/sekarang
            unset($measurements[$index]);
          } else {
            // Jika tidak ada value pressure (brarti row flow)
            $totalizers = $totalizers . "[Date.UTC(".$data->year.",".$data->month.",".$data->day.",".$data->hour.",".$data->minute."),".$data->totalizer."],";
            // Simpan flow & meter ke temp untuk diinject ke row pressure di iterasi berikutnya
            $totalizer_temp = $data->totalizer;
            $meterindex_temp = $data->meter_index;
            // Hapus row flow yang ini/sekarang
            unset($measurements[$index]);
          }
          
       }
     }

     // Trim/hilangkan koma sisa terakhir
     $pressures = rtrim($pressures,", ");
     $flows = rtrim($flows,", ");
     // Re-indexing array $measurements yang bolong bolong
     $measurements=array_values($measurements);

     return view('nrws.chartmeasurement',compact('spams','spamselect', 'zonas','zonaselect','dmas','dmaselect','pressures','flows','measurements'));
  }

   return view('nrws.chartmeasurement',compact('spams','spamselect', 'zonas','zonaselect','dmas','dmaselect'));
 }

}
