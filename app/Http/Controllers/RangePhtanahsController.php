<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\RangePhtanahService;
use App\Http\Requests\RangePhtanahCreateRequest;
use App\Http\Requests\RangePhtanahUpdateRequest;

class RangePhtanahsController extends Controller
{
    public function __construct(RangePhtanahService $range_phtanah_service)
    {
        $this->service = $range_phtanah_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $range_phtanahs = $this->service->paginated();
        return view('range_phtanahs.index')->with('range_phtanahs', $range_phtanahs);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $range_phtanahs = $this->service->search($request->search);
        return view('range_phtanahs.index')->with('range_phtanahs', $range_phtanahs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('range_phtanahs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\RangePhtanahCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RangePhtanahCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('range_phtanahs.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('range_phtanahs.index'))->with('message', 'Failed to create');
    }

    /**
     * Display the range_phtanah.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $range_phtanah = $this->service->find($id);
        return view('range_phtanahs.show')->with('range_phtanah', $range_phtanah);
    }

    /**
     * Show the form for editing the range_phtanah.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $range_phtanah = $this->service->find($id);
        return view('range_phtanahs.edit')->with('range_phtanah', $range_phtanah);
    }

    /**
     * Update the range_phtanahs in storage.
     *
     * @param  \Illuminate\Http\RangePhtanahUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RangePhtanahUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->with('message', 'Failed to update');
    }

    /**
     * Remove the range_phtanahs from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('range_phtanahs.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('range_phtanahs.index'))->with('message', 'Failed to delete');
    }
}
