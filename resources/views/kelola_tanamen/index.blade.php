@extends('dashboard')

@section('content')
   <div class="row">
        <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
          {!! Form::open(['route' => 'kelola_tanamen.search','class' => 'pull-right raw-margin-top-24 raw-margin-left-24']) !!}
            {!! csrf_field() !!}
            
          {!! Form::close() !!}
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('kelola_tanamen.create') !!}">Add New</a>
            <h1 class="pull-left"><span class="fa fa-tree"></span> Kelola Tanaman</h1>
        </div>
    </div>
<div class="container">
    <div class="row">
        @if($kelola_tanamen->isEmpty())
            <div class="well text-center">No pendaftarans found.</div>
        @else
    <div class="row raw-margin-top-24">
        <div class="col-md-11">
            <table class="display table table-striped" id="tabel">
                <thead>
                   <th> Foto</th>
                    <th>Jenis Tanaman</th> 
                    <th>Nama Tanaman</th>
                    <th>Longitute</th>
                    <th>Latitude</th>
                    <th>Aksi</th>
                </thead>
                <tbody>
                @foreach($kelola_tanamen as $kelola_tanaman)
                    <tr>
                         <td><img class="img-thumbnail" src="{{ url($kelola_tanaman->foto->url('thumb')) }}"></td>
                        <td>{{ $kelola_tanaman->jenis_tanaman }}</td>
                        <td>{{ $kelola_tanaman->nama_tanaman }}</td>
                        <td>{{ $kelola_tanaman->longitute }}</td>
                        <td>{{ $kelola_tanaman->latitude }}</td>
                       
             
                        <td>
                        <a style="margin:1px;" class="btn btn-warning btn-xs pull-right raw-margin-right-16" href="{!! route('kelola_tanamen.edit', [$kelola_tanaman->id]) !!}"><span class="fa fa-edit"></span> Ubah</a>
                        <form method="post" action="{!! route('kelola_tanamen.destroy', [$kelola_tanaman->id]) !!}">
                        {!! csrf_field() !!}
                        {!! method_field('DELETE') !!}
                        <button style="margin:1px;" class="btn btn-danger btn-xs pull-right" type="submit" onclick="return confirm('Are you sure you want to delete this user?')"><i class="fa fa-trash"></i> Hapus</button></td>
                     
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="row">
              
            </div>
        @endif
    </div>
</div>
</div>
@stop
@section('javascript')
<script type="text/javascript">
    $(document).ready( function () {
    $('.display').DataTable();
    } );
</script>
@stop