@extends('dashboard')

@section('content')
   <div class="row">
            <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
                 <div class="pull-right">
                  </div>
            <h1 class="pull-left"><span class="fa fa-laptop"></span> Manajemen Pohon Bicara</h1>
            </div>
        </div>
<div class="container">
    <div class="row">
        @if($kelola_tanamen->isEmpty())
            <div class="well text-center">No pendaftarans found.</div>
        @else
    <div class="row raw-margin-top-24">
        <div class="col-md-11">
            <table class="display table table-striped">
                <thead>
                    <th>Foto</th>
                    <th>Jenis Tanaman</th>
                    <th>Nama Tanaman</th>
                    <th width="290px">Action</th>
                </thead>
                <tbody>
                @foreach($kelola_tanamen as $kelola_tanaman)
                    <tr>
                        <td><img class="img-thumbnail" src="{{ url($kelola_tanaman->foto->url('thumb')) }}"></td>
                        <td>{{ $kelola_tanaman->jenis_tanaman }}</td>
                        <td>{{ $kelola_tanaman->nama_tanaman }}</td>
                        <td><a class="btn btn-warning btn-xs" href="{!! route('kelola_tanamen.show', [$kelola_tanaman->id]) !!}"><h5> Lihat Pohon Bicara</h5></a>
                        </td>

                       
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="row">
              
            </div>
        @endif
    </div>
</div>
</div>
@stop
@section('javascript')
<script type="text/javascript">
    $(document).ready( function () {
    $('.display').DataTable();
    } );
</script>
@stop
