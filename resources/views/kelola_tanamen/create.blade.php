@extends('dashboard')

@section('content')
<div class="">
    {{ Session::get('message') }}
</div>

 <div class="row">
            <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
                 <div class="pull-right">
                  </div>
            <h1 class="pull-left"><span class="fa fa-tree"></span> Kelola Tanaman</h1>
            </div>
        </div>
  	  {!! Form::open(['route' => 'kelola_tanamen.store', 'files' => true]) !!}
              {!! csrf_field() !!}

			<input type="hidden" name="user_id"  class="form-control" value="{{Auth::user()->id}}" required>
    
              <div class="raw-margin-top-24">
                  @input_maker_label('Jenis Tanaman')
                  @input_maker_create('jenis_tanaman', ['type' => 'string'])
              </div>
              <div class="raw-margin-top-24">
                  @input_maker_label('Nama Tanaman')
                  @input_maker_create('nama_tanaman', ['type' => 'string'])
              </div>
              <div class="raw-margin-top-24">
                  @input_maker_label('Longitute')
                  @input_maker_create('longitute', ['type' => 'string'])
              </div>
              <div class="raw-margin-top-24">
                  @input_maker_label('Latitude *')
                  @input_maker_create('latitude', ['type' => 'text'])
              </div>
               <div class="raw-margin-top-24">
                  <label for="">Nama Perangkat</label>
                    <select class="form-control input-sm" name="perangkat_id"> 
                         @foreach($perangkats as $perangkat)
                           <option value="{{$perangkat->id}}">{{$perangkat->keterangan}}     </option>
                         @endforeach
                    </select>
              </div>
              <div class="raw-margin-top-24">
                <input type="file" name="foto">
              </div>
              <div class="raw-margin-top-24">
                  <a class="btn btn-default pull-left" href="{!! route('kelola_tanamen.index') !!}">Cancel</a>
                  <button class="btn btn-primary pull-right" type="submit">Create</button>
              </div>
<br/></br/><br/></br/><br/></br/>
          {!! Form::close() !!}
      </div>


    </div>
</div>

</div>
@stop