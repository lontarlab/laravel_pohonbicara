@extends('dashboard')

@section('content')
<div class="row">
            <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
                 <div class="pull-right">
                  </div>
            <h1 class="pull-left"><span class="fa fa-tree"></span> Kelola Tanaman</h1>
            </div>
        </div>

<div class="row">
  <div class="col-md-6 raw-margin-bottom-24 raw-margin-left-24">

    {!! Form::model($kelola_tanaman, ['route' => ['kelola_tanamen.update', $kelola_tanaman->id], 'method' => 'patch', 'files' =>true]) !!}
     {!! csrf_field() !!}    {!! method_field('PATCH') !!}

               
                     @form_maker_object($kelola_tanaman,[
                    'jenis_tanaman'=>['alt_name'=>'jenis_tanaman'],
                    'nama_tanaman'=>['alt_name'=>'nama_tanaman'],
                    'longitute'=>['alt_name'=>'Longitute'],
                    'latitude'=>['alt_name'=>'latitude'],
                    ])

                <div class="raw-margin-top-24">
                  <label for="">Nama Perangkat</label>
                    <select class="form-control input-sm" name="perangkat_id"> 
                         @foreach($perangkats as $perangkat)
                           <option value="{{$perangkat->id}}">{{$perangkat->keterangan}}     </option>
                         @endforeach
                    </select>
              </div><br>
			         <div class="form-group">
                       <label class="control-label" for="foto">Foto</label><br />
                       <img class="img-thumbnail" src="{{ url($kelola_tanaman->foto->url('thumb')) }}">
                       <input type="file" name="foto">
                     </div>

                  <div class="raw-margin-top-24">
                      <a class="btn btn-default pull-left" href="{!! route('kelola_tanamen.index') !!}">Cancel</a>
                      <button class="btn btn-primary pull-right" type="submit">Save</button>
                  </div>

            {!! Form::close() !!}
          </div>
      </div>
  </div>

@stop
