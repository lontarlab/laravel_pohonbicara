@extends('dashboard')



@section('content')

<div class="">
    {{ Session::get('message') }}
</div>
<div class="row">
            <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
                 <div class="pull-right">
                  </div>
             @foreach($datas as $key => $data)
            <h1 class="pull-left"><span class="fa fa-laptop"></span> Informasi Tanaman {{$data->nama_tanaman}}</</h1>
            @endforeach
            </div>
        </div>

<br><div id="exTab2" class="container">
<ul class="nav nav-tabs">
			<li class="active">
        	<a  href="#1" data-toggle="tab">Tanaman</a></li>
			<li><a href="#2" data-toggle="tab">Data Sensor</a></li>
			<li><a href="#3" data-toggle="tab">Grafik</a></li>
			<li><a href="#4" data-toggle="tab">SPK Suhu</a></li>
			<li><a href="#5" data-toggle="tab">SPK Kelembapan Udara</a></li>
		    <li><a href="#6" data-toggle="tab">SPK Kelembapan Tanah</a></li>
		    <li><a href="#7" data-toggle="tab">SPK Cahaya</a></li>
			<li><a href="#8" data-toggle="tab">SPK Tanaman</a></li>
			
		</ul>
			<div class="tab-content ">
			  <div class="tab-pane active" id="1">
				<div class="row">
					   <div class="col-md-11">
					     <br> <div class="panel panel-primary">
					      <div class="panel-heading"></div>
  								<div class="panel-body">
  									<a href="/getantares/{{$id}}" type="button"  class="btn btn-primary pull-right ">Update Data Sensor Terbaru</a>
    									
    								<br><br><div class="row">
							         <p>@foreach($sensors1 as $key => $sensor)
							           <div class="col-md-3">
							             <a href="#" class="btn btn-sq-lg btn-danger">
							             <img src="{{ url('img/waktu.png') }}" width=100><br>
							              Waktu Sensor :{{$sensor->waktu}}</a>
							           </div>
							        	<div class="col-md-2">
								            <a href="#" class="btn btn-sq-lg btn-primary">				
								            <img src="{{ url('img/phtanah.png') }}" width=100><br>
								              Keasaman Tanah       : {{$sensor->ph_tanah}}</a>
							            </div>
							            <div class="col-md-2">
								            <a href="#" class="btn btn-sq-lg btn-success">
								            <img src="{{ url('img/suhu.png') }}" width=100><br>
								                 Suhu Tanaman  : {{$sensor->suhu}} </a>
							        	</div>
							        	<div class="col-md-2">
								            <a href="#" class="btn btn-sq-lg btn-info">
								            <img src="{{ url('img/kelembapan.png') }}" width=130><br>
								              Kelembapan   :  {{$sensor->kelembapan}} </a>
							            </div>
							            <div class="col-md-2">
								             <a href="#" class="btn btn-sq-lg btn-warning">
								             <img src="{{ url('img/cahaya2.png') }}" width=100><br>
								              Intensitas Cahaya   : {{$sensor->cahaya}}</a>
							        	</div>				
						            </p>@endforeach
								</div>
  							</div>
  						<div class="panel-heading"></div>
					</div>
			    </div>
			    			 @foreach($datas as $key => $data)
					        <div class="row">
					        	<div class="col-md-4">
					          <br> <div class="panel panel-primary">
					          	<div class="panel-heading"></div>
  									<div class="panel-body">
    								<img src="{{ url('img/hd2.jpg') }}" width=300><br>
    								

  									</div>
  								<div class="panel-heading"></div>
								</div>
					        </div>

					        	<div class="col-md-7">
					          <br> <div class="panel panel-primary">
					          	<div class="panel-heading"></div>
  									<div class="panel-body">
    									<ul class="list-group">
    									<li class="list-group-item list-group-item-success">Nama Pengelola 						: {{$data->name}}</li>
    									<li class="list-group-item list-group-item-success">Jenis Tanaman 						: {{$data->jenis_tanaman}}</li>
		 								 <li class="list-group-item list-group-item-info">Nama Tanaman 						   : {{$data->nama_tanaman}} </li>
		  								<br>
    									@endforeach
    									
  									</div>
  								<div class="panel-heading"></div>
								</div>
					        </div>

					        </div>
					  </div>
				</div>
		<!-- Data Sensors -->
				<div class="tab-pane" id="2">
			<div class="row">
				<div class="col-md-11">				
			           <br> <table class="display table table-striped">
                <thead>
                	
                    <th>Waktu Sensors</th>
                    <th>Waktu Pengiriman</th>
                    <th>Total Delay Detik </th>
                    <th>Konversi  Delay </th>
                    <th>Kelembapan Tanah</th>
                    <th>Kelembapan</th>
                    <th>Suhu</th>
                    <th>Cahaya</th>
                    
                    
                    <!-- <th width="150px">Action</th> -->
                </thead>
                <tbody>
                @foreach($sensors as $sensor)
                    <tr>

                        <td>{{ $sensor->waktu  }}</td>
                        <td>{{$sensor->created_at }}</td>
                         <?php  $jam1   = substr($sensor->waktu, 10, 3) * 3600;
                        $menit1 = substr($sensor->waktu, 14, 2) * 60;
                        $detik1 = substr($sensor->waktu, 17, 2);
                        $waktu1 = $jam1 + $menit1 + $detik1; 

                        $jam2  = substr($sensor->created_at, 10, 3) * 3600;
                        $menit2 = substr($sensor->created_at, 14, 2) * 60;
                        $detik2 = substr($sensor->created_at, 17, 2);
                        $waktu2 = $jam2 + $menit2 + $detik2;
                        $totalDetik = abs($waktu2 - $waktu1);
                        $jam = abs($totalDetik / 3600);
                        $modMenit = $totalDetik % 3600;
                        $menit    = abs($modMenit / 60);
                        $detik   = abs($menit / 60);
                        ?>
                        <td>{{ $totalDetik }} detik</td>
                        <td> {{ number_format($jam, 0) }} jam 
                          {{ number_format($menit, 0)  }} menit
                            {{ number_format($detik, 0)   }} Detik </td>
                        <td>{{ $sensor->ph_tanah }}</td>
                        <td>{{ $sensor->kelembapan }}</td>
                        <td>{{ $sensor->suhu }}</td>
                        <td>{{ $sensor->cahaya }}</td>
                       
                      
                       @endforeach
                     
			                </tbody>			            
			            </table>
							</div>				
						</div>
				</div>

<!-- Grafik -->
				<div class="tab-pane" id="3">
					<div class="row">
						<div class="col-md-8">
				    <script src="{{ url('chart/highcharts.js') }}"></script>
				     @foreach($sensors as $key => $sensor)
				        <?php $sensors1[] = intval($sensor->ph_tanah); ?>
				        <?php $sensors2[] = intval($sensor->kelembapan); ?>
				        <?php $sensors3[] = intval($sensor->cahaya); ?>
				        <?php $sensors4[] = intval($sensor->suhu); ?>
				        <?php $sensors5[] = $sensor->tanggal; ?>
				        <?php $sensors6[] = $sensor->waktu; ?>
				     @endforeach
            
                	<div class="row">	
                		  <div class="col-md-3">
                			<h2> Lihat Grafik</h2>
                		   </div>	
                		   <div class="col-md-3">
                				<br><a href="{!! url('/grafikhari') !!}"><button type="submit" name="submit" class="btn btn-info btn-fill btn-wd btn" value="Submit">Filter Grafik Perhari</button></a>
                		   </div>
                		   <div class="col-md-3">
                				<br><a href="{!! url('/grafikbulan') !!}"><button type="submit" name="submit" class="btn btn-info btn-fill btn-wd btn" value="Submit">Filter Grafik Perbulan</button></a>
                		   </div>
                		   <div class="col-md-3">
                				<br><a href="{!! url('/grafiktahun') !!}"><button type="submit" name="submit" class="btn btn-info btn-fill btn-wd btn" value="Submit">Filter Grafik Pertahun</button></a>
                		   </div>
				  	</div>
            
              
						<div class="row">
  							<div class="col-md-11 raw-margin-bottom-24 raw-margin-left-24">
                             	<div id="semua_Sensor" style="width: 1000px; height: 400px; margin: 0 auto"></div>
                            <script>
										Highcharts.chart('semua_Sensor', {
									    chart: {
									        type: 'areaspline'
									    },
									    title: {
									        text: 'Grafik Data Sensor Tanaman'
									    },
									    legend: {
									        layout: 'vertical',
									        align: 'left',
									        verticalAlign: 'top',
									        x: 150,
									        y: 100,
									        floating: true,
									        borderWidth: 1,
									        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
									    },
									    xAxis: {
									        categories: <?= json_encode($sensors6); ?>,
									        plotBands: [{ // visualize the weekend
									            from: 4.5,
									            to: 6.5,
									            color: 'rgba(68, 170, 213, .2)'
									        }]
									    },
									    yAxis: {
									        title: {
									            text: 'Range Data Sensor'
									        }
									    },
									    tooltip: {
									        shared: true,
									        valueSuffix: ''
									    },
									    credits: {
									        enabled: false
									    },
									    plotOptions: {
									        areaspline: {
									            fillOpacity: 0.5
									        }
									    },
									     series: [{
										  name: 'Kelembapan',
									        marker: {
									            symbol: 'square'
									        },
									        data: <?= json_encode($sensors1); ?>,

									    }, {
									        name: 'Suhu',
									        marker: {
									            symbol: 'square'
									        },
									        data:<?= json_encode($sensors2); ?>,
									     },{ name: 'Intensitas Cahaya',
									        marker: {
									            symbol: 'square'
									        },
									        data:<?= json_encode($sensors3); ?>,

									    	},{ name: 'PH Tanah',
									        marker: {
									            symbol: 'square'
									        },
									        data:<?= json_encode($sensors4); ?>,
									    }]
									});
						</script>
                    </div>
                </div>
            

            <div class="row">
                    <div class="col-md-6">
                             <div id="suhu" style="width: 500px; height: 400px; margin: 0 auto">
									<br>
                            		<script>
										Highcharts.chart('suhu', {
									    chart: {
									        type: 'spline'
									    },
									    title: {
									        text: 'Grafik Suhu Tanaman'
									    },
									    subtitle: {
									        text: ''
									    },
									    xAxis: {
									        categories:<?= json_encode($sensors5); ?>,
									    },
									    yAxis: {
									        title: {
									            text: 'Suhu'
									        },
									        labels: {
									            formatter: function () {
									                return this.value + '°';
									            }
									        }
									    },
									    tooltip: {
									        crosshairs: true,
									        shared: true
									    },
									    plotOptions: {
									        spline: {
									            marker: {
									                radius: 4,
									                lineColor: '#666666',
									                lineWidth: 1
									            }
									        }
									    },
									    series: [{
									        name: 'Suhu Tanaman',
									        marker: {
									            symbol: 'square'
									        },
									        data:<?= json_encode($sensors4); ?>,
									    }, {
									        
									    }]
									});
									</script>
                    	</div>
                    </div>

                   <div class="col-md-6">
                    <div id="kelembapan" style="width: 500px; height: 400px; margin: 0 auto">
									<br>
                                <script>
                                	 Highcharts.chart('kelembapan', {
									    chart: {
									        type: 'area',
									        spacingBottom: 30
									    },
									    title: {
									        text: 'Grafik Data Kelembapan'
									    },
									    subtitle: {
									        text: '',
									        floating: true,
									        align: 'right',
									        verticalAlign: 'bottom',
									        y: 15
									    },
									    legend: {
									        layout: 'vertical',
									        align: 'left',
									        verticalAlign: 'top',
									        x: 150,
									        y: 100,
									        floating: true,
									        borderWidth: 1,
									        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
									    },
									    xAxis: {
									        categories: <?= json_encode($sensors6); ?>,
									    },
									    yAxis: {
									        title: {
									            text: 'Y-Axis'
									        },
									        labels: {
									            formatter: function () {
									                return this.value;
									            }
									        }
									    },
									    tooltip: {
									        formatter: function () {
									            return '<b>' + this.series.name + '</b><br/>' +
									                this.x + ': ' + this.y;
									        }
									    },
									    plotOptions: {
									        area: {
									            fillOpacity: 0.5
									        }
									    },
									    credits: {
									        enabled: false
									    },
									    series: [{
									        name: 'Kelembapan',
									        data: <?= json_encode($sensors2); ?>,
									    }, {
									       
									    }]
									});
                                </script>
                  </div>
                </div>
             
               <div class="row">
               		<div class="col-md-6">
                    <div id="ph_tanah" style="width: 500px; height: 400px; margin: 0 auto">
									<br>
                                <script>
                                	 Highcharts.chart('ph_tanah', {
									    chart: {
									        type: 'area',
									        spacingBottom: 30
									    },
									    title: {
									        text: 'Grafik Data Keasaman'
									    },
									    subtitle: {
									        text: '',
									        floating: true,
									        align: 'right',
									        verticalAlign: 'bottom',
									        y: 15
									    },
									    legend: {
									        layout: 'vertical',
									        align: 'left',
									        verticalAlign: 'top',
									        x: 150,
									        y: 100,
									        floating: true,
									        borderWidth: 1,
									        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
									    },
									    xAxis: {
									        categories: <?= json_encode($sensors6); ?>,
									    },
									    yAxis: {
									        title: {
									            text: 'Y-Axis'
									        },
									        labels: {
									            formatter: function () {
									                return this.value;
									            }
									        }
									    },
									    tooltip: {
									        formatter: function () {
									            return '<b>' + this.series.name + '</b><br/>' +
									                this.x + ': ' + this.y;
									        }
									    },
									    plotOptions: {
									        area: {
									            fillOpacity: 0.5
									        }
									    },
									    credits: {
									        enabled: false
									    },
									    series: [{
									        name: 'Keasaman Tanah',
									        data: <?= json_encode($sensors1); ?>,
									    }, {
									       
									    }]
									});
                                </script>
                  </div>
                </div>

                    <div class="col-md-6">
                             <div id="cahaya" style="width: 500px; height: 400px; margin: 0 auto">
									<br>
                            		<script>
										Highcharts.chart('cahaya', {
									    chart: {
									        type: 'spline'
									    },
									    title: {
									        text: 'Grafik Data Cahaya'
									    },
									    subtitle: {
									        text: ''
									    },
									    xAxis: {
									        categories:<?= json_encode($sensors6); ?>,
									    },
									    yAxis: {
									        title: {
									            text: 'Intensitas Cahaya'
									        },
									        labels: {
									            formatter: function () {
									                return this.value + '°';
									            }
									        }
									    },
									    tooltip: {
									        crosshairs: true,
									        shared: true
									    },
									    plotOptions: {
									        spline: {
									            marker: {
									                radius: 4,
									                lineColor: '#666666',
									                lineWidth: 1
									            }
									        }
									    },
									    series: [{
									        name: 'Cahaya Tanaman',
									        marker: {
									            symbol: 'square'
									        },
									        data:<?= json_encode($sensors3); ?>,
									    }, {
									        
									    }]
									});
									</script>
                    	</div>
                    </div>
				 </div>
			  </div>
		   </div>
		</div>
	</div>
        
  <!-- Notifikasi SPK SUHU -->
  <div class="tab-pane" id="4">
      <div class="row">
        <div class="col-md-11">
                  <br><table class="display table table-striped">
                      <thead>
                                  
                        <th>Waktu Sensor</th>
                          <th>Suhu</th>
                          <th>Kriteria Satu</th>
                          <th>Kriteria Dua</th>
                          <th>Kriteria Tiga</th>
                          <th>Analisis Kriteria</th>
                      </thead>
                      <tbody>
                      
                      @foreach($sensors as $sensor)
                       
                          <tr>
                            <td>{{ $sensor->waktu }}</td>
                              <td>{{ $sensor->suhu }}</td>
                              <td> 
                                @if($sensor->suhu >= $kreteria[0]->rendah)
                                 0
                                @elseif($kreteria[1]->rendah <= $sensor->suhu && $sensor->suhu <= $kreteria[1]->rendah)
                                 $kecil = abs($kreteria[1]->rendah - $sensor->suhu / $kreteria[1]->rendah - $kreteria[0]->rendah)
                                 {{ number_format($kecil, 2) }} 
                                @else
                                1
                                @endif
                              </td>
                              <td>  
                                @if($sensor->suhu <= $kreteria[0]->sedang)
                                0
                                @elseif($kreteria[0]->sedang <= $sensor->suhu && $sensor->suhu <= $kreteria[1]->sedang)
                               {{ number_format($sensor->suhu  - $kreteria[0]->sedang    / $kreteria[1]->sedang - $kreteria[0]->sedang , 2)}}
                                @elseif($kreteria[0]->sedang <= $sensor->suhu && $sensor->suhu <= $kreteria[2]->sedang)
                                 {{ number_format($kreteria[2]->sedang - $sensor->suhu / $kreteria[2]->sedang - $kreteria[1]->sedang)}} 
                                 @elseif($sensor->suhu >= $kreteria[2]->sedang)
                                  0 
                                 @else
                                  1
                                 @endif
                              </td>
                              <td>
                                @if($sensor->suhu <= $kreteria[0]->tinggi)
                                0
                                @elseif(30 <= $sensor->suhu && $sensor->suhu <= 35)
                                {{ number_format($sensor->suhu - $kreteria[0]->tinggi / $kreteria[1]->tinggi - $kreteria[0]->tinggi,2)}}
                                @else
                                1
                                @endif
                              </td>
                              <td>
                                 @if($sensor->suhu > 0 && $sensor->suhu <= 10)
                                <h4><span class="label label-warning">Suhu Sangat Rendah</span></h4>
                                 @elseif($sensor->suhu > 10 && $sensor->suhu <= 15)
                                <h5><span class="label label-warning">Suhu agak Rendah</span></h5>
                                 @elseif($sensor->suhu > 15 && $sensor->suhu  <= 20 )
                                <h4><span class="label label-success">Suhu Mulai sedang </span></h4>
                                 @elseif($sensor->suhu > 20 && $sensor->suhu <= 25)
                                  <h4><span class="label label-success">Suhu sedang </span></h4>
                                 @elseif($sensor->suhu  > 25 && $sensor->suhu <= 30)
                                  <h4><span class="label label-primary ">Suhu Mulai Panas </span></h4>
                                 @elseif($sensor->suhu  > 30 && $sensor->suhu <= 35)
                                  <h4><span class="label label-danger">Suhu Panas </span></h4>
                                 @elseif($sensor->suhu > 35 && $sensor->suhu <= 40)
                                  <h4><span class="label label-danger">Suhu Sangat Panas </span></h4>
                                 @endif

                              </td>
                                                         
                          </tr>
                        
                      @endforeach
                      </tbody>                  
                  </table>
              </div>        
            </div>
          </div>
<!-- Notifikasi SPK Kelembapan Udara-->
  <div class="tab-pane" id="5">
      <div class="row">
        <div class="col-md-11">
                  <br><table class="display table table-striped">
                      <thead>       
                        <th>Waktu Sensor</th>
                          <th>Kelembapan</th>
                          <th>Kriteria Satu</th>
                          <th>Kriteria Dua</th>
                          <th>Kriteria Tiga</th>
                          <th>Analisis Kriteria</th>
                      </thead>
                      <tbody>
                      
                      @foreach($sensors as $sensor)
                       
                          <tr>
                            <td>{{ $sensor->waktu }}</td>
                              <td>{{ $sensor->kelembapan }}</td>
                              <td> 
                                @if($sensor->kelembapan >= $kreteria_phtanah[0]->rendah)
                                 0
                                @elseif($kreteria_phtanah[1]->rendah <= $sensor->kelembapan && $sensor->kelembapan <= $kreteria_phtanah[1]->rendah)
                                   {{ number_format($kreteria_phtanah[1]->rendah - $sensor->kelembapan / $kreteria_phtanah[1]->rendah - $kreteria_phtanah[0]->rendah, 2) }}
                                @else
                                1
                                @endif
                              </td>
                              <td>  
                                @if($sensor->kelembapan <= $kreteria_phtanah[0]->sedang)
                                0
                                @elseif($kreteria_phtanah[0]->sedang <= $sensor->kelembapan && $sensor->kelembapan <= $kreteria_phtanah[1]->sedang)
                                 {{ number_format($sensor->kelembapan  - $kreteria_phtanah[0]->sedang    / $kreteria_phtanah[1]->sedang - $kreteria_phtanah[0]->sedang,2) }}                               
                                @elseif($kreteria_phtanah[0]->sedang <= $sensor->ph_tanah && $sensor->ph_tanah <= $kreteria_phtanah[2]->sedang)
                                 {{ number_format($kreteria_phtanah[2]->sedang - $sensor->ph_tanah / $kreteria_phtanah[2]->sedang - $kreteria_phtanah[1]->sedang,2) }} 
                                 @elseif($sensor->ph_tanah >= $kreteria_phtanah[2]->sedang)
                                  0 
                                 @else
                                  1
                                 @endif
                              </td>
                              <td>
                                @if($sensor->kelembapan <= $kreteria_phtanah[0]->tinggi)
                                0
                                @elseif(30 <= $sensor->kelembapan && $sensor->kelembapan <= 35)
                                {{$sensor->kelembapan - $kreteria_phtanah[0]->tinggi / $kreteria_phtanah[1]->tinggi - $kreteria_phtanah[0]->tinggi}}
                                @else
                                1
                                @endif
                              </td>
                              <td>
                                 @if($sensor->kelembapan > 0 && $sensor->kelembapan <= 20)
                                  <h4><span class="label label-warning"> Kelembapan Udara Sangat Kering</span></h4>
                                 @elseif($sensor->kelembapan > 20 && $sensor->kelembapan <= 30)
                                  <h4><span class="label label-warning"> Kelembapan Udara Kering</span></h4>
                                 @elseif($sensor->kelembapan > 30 && $sensor->kelembapan <= 50)
                                  <h4><span class="label label-success"> Kelembapan Udara Normal</span></h4>
                                 @elseif($sensor->kelembapan > 50 && $sensor->kelembapan  <= 70 )
                                  <h4><span class="label label-primary"> Kelembapan Udara Sedikit Tinggi</span></h4>
                                 @elseif($sensor->kelembapan > 70 && $sensor->kelembapan <= 85)
                                  <h4><span class="label label-danger"> Kelembapan Udara Tinggi</span></h4>
                                 @elseif($sensor->kelembapan  > 85 && $sensor->kelembapan <= 100)
                                  <h4><span class="label label-danger"> Kelembapan Udara Sangat Tinggi</span></h4>
                                 @endif

                              </td>
                                                         
                          </tr>
                        
                      @endforeach
                      </tbody>                  
                  </table>
              </div>        
            </div>
             </div>


<!-- Notifikasi SPK KELEMBAPAB TANAH -->
  <div class="tab-pane" id="6">
      <div class="row">
        <div class="col-md-11">
                 <br><table class="display table table-striped">
                      <thead>
                                  
                        <th>Waktu Sensor</th>
                          <th>Kelembapan</th>
                          <th>Kriteria Satu</th>
                          <th>Kriteria Dua</th>
                          <th>Kriteria Tiga</th>
                          <th>Analisis Kriteria</th>
                      </thead>
                      <tbody>
                      
                      @foreach($sensors as $sensor)
                       
                          <tr>
                            <td>{{ $sensor->waktu }}</td>
                              <td>{{ $sensor->ph_tanah }}</td>
                              <td> 
                                @if($sensor->ph_tanah >= $kreteria_kelembapan[0]->rendah)
                                 0
                                @elseif($kreteria_kelembapan[1]->rendah <= $sensor->kelembapan && $sensor->ph_tanah <= $kreteria_kelembapan[1]->rendah)
                                    {{ $kreteria_kelembapan[1]->rendah - $sensor->ph_tanah / $kreteria_kelembapan[1]->rendah - $kreteria_kelembapan[0]->rendah }}
                                @else
                                1
                                @endif
                              </td>
                              <td>  
                                @if($sensor->ph_tanah <= $kreteria_kelembapan[0]->sedang)
                                0
                                @elseif($kreteria_kelembapan[0]->sedang <= $sensor->ph_tanah && $sensor->ph_tanah <= $kreteria_kelembapan[1]->sedang)
                                 {{ number_format($sensor->ph_tanah  - $kreteria_kelembapan[0]->sedang    / $kreteria_kelembapan[1]->sedang - $kreteria_kelembapan[0]->sedang,2) }}                               
                                @elseif($kreteria_kelembapan[0]->sedang <= $sensor->kelembapan && $sensor->ph_tanah <= $kreteria_kelembapan[2]->sedang)
                                  {{ number_format($kreteria_kelembapan[2]->sedang - $sensor->ph_tanah / $kreteria_kelembapan[2]->sedang - $kreteria_kelembapan[1]->sedang,2) }} 
                                 @elseif($sensor->ph_tanah >= $kreteria_kelembapan[2]->sedang)
                                  0 
                                 @else
                                  1
                                 @endif
                              </td>
                              <td>
                                @if($sensor->ph_tanah <= $kreteria_kelembapan[0]->tinggi)
                                0
                                @elseif(30 <= $sensor->ph_tanah && $sensor->ph_tanah <= 35)
                              {{ number_format($sensor->ph_tanah - $kreteria_kelembapan[0]->tinggi / $kreteria_kelembapan[1]->tinggi - $kreteria_kelembapan[0]->tinggi,2)}}
                                @else
                                1
                                @endif
                              </td>
                              <td>
                                 @if($sensor->ph_tanah >= 0 && $sensor->ph_tanah <= 20)
                                <h4><span class="label label-warning"> Kelembapan Tanah Sangat Kering</span></h4>
                                 @elseif($sensor->ph_tanah > 20 && $sensor->ph_tanah <= 30)
                                 <h4><span class="label label-warning"> Kelembapan Tanah Kering</span></h4>
                                 @elseif($sensor->ph_tanah > 30 && $sensor->ph_tanah <= 50)
                                 <h4><span class="label label-primary"> Kelembapan Tanah Agak Kering</span></h4>
                                 @elseif($sensor->ph_tanah > 50 && $sensor->ph_tanah  <= 70 )
                                  <h4><span class="label label-success">  Kelembapan Tanah Sedikit Basah</span></h4>
                                
                                 @elseif($sensor->ph_tanah > 70 && $sensor->ph_tanah <= 85)
                                  <h4><span class="label label-danger">   Kelembapan Tanah Basah</span></h4>
                                
                                 @elseif($sensor->ph_tanah > 85 && $sensor->ph_tanah <= 1030)
                                  <h4><span class="label label-danger">   Kelembapan Tanah Sangat  Basah</span></h4>
                                
                                 @endif

                              </td>
                                                         
                          </tr>
                        
                      @endforeach
                      </tbody>                  
                  </table>
                </div>
            </div>
             </div>

<!-- Notifikasi SPK PH Cahaya -->
  <div class="tab-pane" id="7">
      <div class="row">
        <div class="col-md-11">
                  <br><table class="display table table-striped">
                      <thead>       
                        <th>Waktu Sensor</th>
                          <th>Intensitas Cahaya</th>
                          <th>Kriteria Satu</th>
                          <th>Kriteria Dua</th>
                          <th>Analisis Kriteria</th>
                      </thead>
                      <tbody>
                      
                      @foreach($sensors as $sensor)
                       
                          <tr>
                            <td>{{ $sensor->waktu }}</td>
                              <td>{{ $sensor->cahaya }}</td>
                              <td> 
                                @if($sensor->cahaya >= 1)
                                 1
                                 @else 
                                 0
                                @endif
                              </td>
                              <td>  
                                @if($sensor->cahaya <= 0)
                                1
                                @else
                                0
                                 @endif
                              </td>
                              <td>
                                 @if($sensor->cahaya >= 1)
                                  <h4><span class="label label-success"> Cahaya Terang</span></h4>
                                 @elseif($sensor->cahaya >= 0)
                                 <h4><span class="label label-danger"> Cahaya Gelap</span></h4>
                                 @endif

                              </td>                            
                          </tr>
                      @endforeach
                      </tbody>                  
                  </table>
              </div>        
            </div>
             </div>

			       <!-- Notifikasi SPK PH Cahaya -->
 <div class="tab-pane" id="8">
			<div class="row">
				<div class="col-md-11">				
			           <br> <table class="display table table-striped">
			                <thead>			    
			                	<th>Waktu Sensor</th>
			                    <th>Suhu</th>
			                    <th>Kelembapan Udara</th>
			                    <th>Kelembapan Tanah</th>
			                    <th>Cahaya</th>
			                    <th>Analisis SPK Tanaman</th>
			                </thead>
			                <tbody>
			               
			                @foreach($sensors as $sensor)
			                    <tr>
			                    	<td>{{ $sensor->waktu }}</td>
			                    	<td>{{ $sensor->suhu }}</td>
			                        <td>{{ $sensor->kelembapan }}</td>
			                        <td>{{ $sensor->ph_tanah }}</td>
			                        <td>{{ $sensor->cahaya }}</td>
			                        <td> 

			                        	  <!-- Tanaman Normal -->
			                        	  @if($sensor->suhu > 20 &&  $sensor->suhu < 35 && $sensor->kelembapan >= 50 && $sensor->kelembapan < 70 && $sensor->ph_tanah >= 40 && $sensor->ph_tanah <= 70  )
			                        	  <h4><span class="label label-success">Kelembapan Tanah Normal  Suhu dan Kelembapan Udara normal</span></h4>
			                        	  <!-- Jangan Siram Tanaman   -->
			                        	 @elseif( $sensor->ph_tanah < 39)
			                        	 <h4><span class="label label-danger"> kelembapan tanah kering, segera siram tanaman</span></h4>
			                        	  <!-- Siram Tanaman  -->
			                        	  @elseif($sensor->ph_tanah > 80)
			                        	   <h4><span class="label label-warning"> 
			                        	  Kelembapan tanah masih sangat basah, disarankan tidak untuk menyiram tanaman</span></h4>

			                        	  @endif
			                        </td>	                       
			                    </tr>
			                @endforeach
			                </tbody>			            
			            </table>
							</div>				
						</div>
				</div>

@stop
@section('javascript')
<script type="text/javascript">
    $(document).ready( function () {
    $('.display').DataTable({
   "order": [[ 0, 'desc' ]]
});
    } );
</script>
@stop