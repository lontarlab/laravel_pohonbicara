@extends('dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
          {!! Form::open(['route' => 'pendaftarans.search','class' => 'pull-right raw-margin-top-24 raw-margin-left-24']) !!}
            {!! csrf_field() !!}
          {!! Form::close() !!}
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('pendaftarans.create') !!}">Add New</a>
            <h1 class="pull-left"><span class="fa fa-pencil"></span> Pedaftaran</h1>
        </div>
    </div>
<div class="container">
    <div class="row">
        @if($pendaftarans->isEmpty())
            <div class="well text-center">No pendaftarans found.</div>
        @else
    <div class="row raw-margin-top-24">
        <div class="col-md-11">
            <table class="display table table-striped">
                <thead>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Telepon</th>
                    <th>Alamat</th>
                    <th width="170px" class="">Aktivasi</th>
                </thead>
                <tbody>
                @foreach($pendaftarans as $pendaftaran)
                    <tr>
                        <td>{{ $pendaftaran->name }}</td>
                        <td>{{ $pendaftaran->email }}</td>
                        <td>{{ $pendaftaran->phone }}</td>
                        <td>{{ $pendaftaran->alamat }}</td>
                        <td>
                                @if($pendaftaran->status == 'Belum Terverifikasi')
                                <a class="btn btn-warning btn-xs" href="{!! route('pendaftarans.verify', [$pendaftaran->id]) !!}"><i class="fa fa-check"></i> Verify</a>
                                @else
                                <a class="btn btn-default btn-xs" href="#"><i class="fa fa-check"></i> Verified</a>
                                @endif
                            
                                <!-- <a style="margin:1px;" class="btn btn-warning btn-xs pull-right raw-margin-right-16" href="{!! route('pendaftarans.edit', [$pendaftaran->id]) !!}"><span class="fa fa-edit"></span> Ubah</a>
                                <form method="post" action="{!! route('pendaftarans.destroy', [$pendaftaran->id]) !!}"> -->
                                    {!! csrf_field() !!}
                                    {!! method_field('DELETE') !!}
                                <button style="margin:1px;" class="btn btn-danger btn-xs pull-right" type="submit" onclick="return confirm('Are you sure you want to delete this user?')"><i class="fa fa-trash"></i> Hapus</button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            </form>
            <div class="row">
                {!! $pendaftarans; !!}
            </div>
        @endif
    </div>
</div>
</div>
@stop
@section('javascript')
<script type="text/javascript">
    $(document).ready( function () {
    $('.display').DataTable();
    } );
</script>
@stop