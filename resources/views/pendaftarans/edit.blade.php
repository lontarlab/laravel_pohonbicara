<div class="">
    {{ Session::get('message') }}
</div>

<div class="container">

    {!! Form::model($pendaftaran, ['route' => ['pendaftarans.update', $pendaftaran->id], 'method' => 'patch']) !!}

    @form_maker_object($pendaftaran, FormMaker::getTableColumns('pendaftarans'))

    {!! Form::submit('Update') !!}

    {!! Form::close() !!}
</div>
