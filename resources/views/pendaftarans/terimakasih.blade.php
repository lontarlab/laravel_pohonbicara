@extends('layouts.master')

@section('app-content')

<section class="bg-primary">
    <div class="container">
        <br />
        <div class="jumbotron" style="color:#555;">
                <h2 class="section-heading">Terima Kasih</h2>
                <p class="text-faded">Pendaftaran telah masuk, silakan selesaikan awal pembayaran untuk aktivasi</p>
                <a href="/" class="page-scroll btn btn-default btn-xl sr-button">Kembali ke Beranda</a>
                <br/></br>
            </div>
    </div>
</section>
        </div>

    </div>
</div>

@stop
