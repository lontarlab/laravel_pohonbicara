<div class="">
    {{ Session::get('message') }}
</div>

<div class="container">

    {!! Form::open(['route' => 'pendaftarans.store']) !!}

    @form_maker_table("pendaftarans")

    {!! Form::submit('Save') !!}

    {!! Form::close() !!}

</div>