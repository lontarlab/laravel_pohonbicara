@extends('layouts.master')

@section('app-content')
<br><div class="container">
<section class="bg-primary">
    <div class="container" style="margin-left:60px;">
        <div class="col-md-5">
          <img src="{!! URL::to('/img/portfolio/thumbnails/4.jpg') !!}" style="width:100%;"/>
        </div>
        <div class="col-md-5">
          <h2 class="section-heading">Pendaftaran Online</h2>
          <p class="text-faded">Pemasangan Smart Farming <b></b> dengan deskripsi :</p>
          <p class="text-faded"></p>
        </div>
    </div>
</section>
<section class="bg-success">
    <div class="row">
        <div class="col-md-8 raw-margin-bottom-24 raw-margin-top-24 raw-margin-left-24">
               {!! Form::open(['url' => 'pendaftarans/registerstore']) !!}
                {!! csrf_field() !!}
                <div class="raw-margin-top-0">
                    @input_maker_label('Nama Lengkap *')
                    @input_maker_create('name', ['type' => 'string'])
                </div>

                <div class="raw-margin-top-24">
                    @input_maker_label('Email *')
                    @input_maker_create('email', ['type' => 'string'])
                </div>
                
                <div class="raw-margin-top-24">
                      @input_maker_label('Jenis Kelamin *')
                      {{ Form::select('gender', ['Laki-laki' => 'Laki-laki', 'Perempuan' => 'Perempuan'], null, ['class' => 'form-control','placeholder' => 'Pilih Gender']) }}
                 </div>
                   
                <div class="raw-margin-top-24">
                    @input_maker_label('Telepon *')
                    @input_maker_create(' phone', ['type' => 'string'])
                </div>

                <div class="raw-margin-top-24">
                    @input_maker_label('Alamat *')
                    @input_maker_create('alamat', ['type' => 'string'])
                </div>
            
                <div class="raw-margin-top-24">
                    @input_maker_create('status', ['type' => 'hidden', 'default_value' => 'Belum Terverifikasi'])
                </div>

                  <div class="raw-margin-top-24 raw-margin-bottom-24">
                        <a class="btn btn-default pull-left" href="{!! route('pendaftarans.index') !!}">Cancel</a>
                        <button class="btn btn-primary pull-right" type="submit">Register</button>
                </div>
    <br/></br/><br/></br/><br/></br/>
                {!! Form::close() !!}
            </div>

        </div>
    </section>
    </div>
</div>
@stop
