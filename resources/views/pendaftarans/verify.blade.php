@extends('dashboard')

@section('content')
    <div class="row">
      {!! Form::open(['url' => 'pendaftarans/store_verify']) !!}
      {!! csrf_field() !!}
      <div class="portlet-body form raw-margin-left-24">
          <!-- BEGIN FORM-->
          <form class="form-horizontal">
              <div class="form-body">
                  <center><h2 class="margin-bottom-20"> Verifikasi Pemasangan Pohon Bicara </h2></center>
                  <h3 class="form-section">Info Pelanggan</h3>
                   
                  {!!  Form::hidden('pendaftaran_id', $pendaftaran->id,['class' => 'form-control'] ) !!}
                  {!! Form::close() !!}
                  <div class="row">
                      <div class="col-md-5">
                          <div class="form-group">
                              <label class="control-label col-md-4">Nama:</label>
                              <div class="col-md-6">
                                  <p class="form-control-static" style="padding:0px;">
                                    {!! $pendaftaran->name !!}
                                      @input_maker_create('name', ['type' => 'hidden', 'class' => 'readonly'], $pendaftaran)
                                  </p>
                              </div>
                          </div>
                      </div>
                      <!--/span-->
                      <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-label col-md-4">Alamat:</label>
                            <div class="col-md-6">
                                <p class="form-control-static" style="padding:0px;">
                                  @input_maker_create('alamat', ['type' => 'hidden'], $pendaftaran)
                                  {!! $pendaftaran->alamat !!}
                                </p>
                            </div>
                        </div>
                      </div>
                      <!--/span-->
                  </div>
                  <!--/row-->
                  <div class="row">
                      <div class="col-md-5">
                          <div class="form-group">
                              <label class="control-label col-md-4">Email:</label>
                              <div class="col-md-6">
                                  <p class="form-control-static" style="padding:0px;">
                                      @input_maker_create('email', ['type' => 'hidden'], $pendaftaran)
                                      {!! $pendaftaran->email !!}
                                  </p>
                              </div>
                          </div>
                      </div>
                      <!--/span-->
                      <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-label col-md-4">Phone:</label>
                            <div class="col-md-6">
                                <p class="form-control-static" style="padding:0px;">
                                  @input_maker_create('phone', ['type' => 'hidden'], $pendaftaran)
                                  {!! $pendaftaran->phone !!}
                                </p>
                            </div>
                        </div>
                      </div>
          
                       <!--/row-->
                  <div class="row">
                      <div class="col-md-5">
                          <div class="form-group">
                              <label class="control-label col-md-4">gender:</label>
                              <div class="col-md-6">
                                  <p class="form-control-static" style="padding:0px;">
                                      @input_maker_create('gender', ['type' => 'hidden'], $pendaftaran)
                                      {!! $pendaftaran->gender !!}
                                  </p>
                              </div>
                          </div>
                      </div>
                      <!--/span-->
                      <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-label col-md-4">Keterangan :</label>
                            <div class="col-md-6">
                                <p class="form-control-static" style="padding:0px;">
                                  @input_maker_create('status', ['type' => 'hidden'], $pendaftaran)
                                  {!! $pendaftaran->status !!}
                                </p>
                            </div>
                        </div>
                      </div>
                      
                    <div class="row">
                      <div class="col-md-5">
                      </div>
                      <div class="col-md-5">
                           <button class="btn btn-primary pull-right" type="submit">Verify</button>&nbsp;
                          {!! link_to(URL::previous(), 'Back', ['class' => 'btn btn-default pull-right', 'style'=>'margin-right:5px' ]) !!}
                      </div>
                    </div>
                       
          </form>
          <!-- END FORM-->
      </div>
      {!! Form::close() !!}
    </div>


@stop
