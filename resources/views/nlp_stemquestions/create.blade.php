@extends('dashboard')

@section('content')
<div class="">
    {{ Session::get('message') }}
</div>

 <div class="row">
            <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
                 <div class="pull-right">
                  </div>
            <h1 class="pull-left"><span class="fa fa-tree"></span> NLP Stemquestion</h1>
            </div>
        </div>

<div class="container">

    {!! Form::open(['route' => 'nlp_stemquestions.store']) !!}

    @form_maker_table("nlp_stemquestions")

    {!! Form::submit('Save') !!}

    {!! Form::close() !!}

</div>
@stop