@extends('dashboard')
@section('content')
<div class="container">

    <div class="">
        {{ Session::get('message') }}
    </div>

    <div class="row">
        <div class="" >
            {!! Form::open(['route' => 'nlp_stemquestions.search', 'class' => 'pull-right raw-margin-top-24 raw-margin-left-24']) !!}
            <input class="form-control form-inline pull-right" name="search" placeholder="Search">
            {!! Form::close() !!}
        </div>
       
        <h1 class="pull-left">Nlp Stemquestions</h1>
        <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('nlp_stemquestions.create') !!}">Add New</a>
    </div>

    <div class="row">
        @if($nlp_stemquestions->isEmpty())
            <div class="well text-center">No nlpStemquestions found.</div>
        @else
            <table class="table">
                <thead>
                    <th>Stem Questions</th>
                    <th>Jawaban</th>
                    <th>Action</th>
                </thead>
                <tbody>
                @foreach($nlp_stemquestions as $nlp_stemquestion)
                    <tr>
                        <td>
                            <a href="{!! route('nlp_stemquestions.edit', [$nlp_stemquestion->id]) !!}">{{ $nlp_stemquestion->stem_question }}</a>
                        </td>
                        <td>
                            <a>{{ $nlp_stemquestion->jawaban }}</a>
                        </td>
                        
                        <td>
                            <a style="margin:1px;" class="btn btn-warning btn-xs pull-right raw-margin-right-16" href="{!! route('nlp_stemquestions.edit', [$nlp_stemquestion->id]) !!}"><span class="fa fa-pencil"></span> Ubah</a>
                             <form method="post" action="{!! route('nlp_stemquestions.destroy', [$nlp_stemquestion->id]) !!}">
                                {!! csrf_field() !!}
                                {!! method_field('DELETE') !!}
                            <button style="margin:1px;" class="btn btn-danger btn-xs pull-right" type="submit" onclick="return confirm('Are you sure you want to delete this nlp_stemquestion?')"><i class="fa fa-trash"></i> Hapus</button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="row">
                {!! $nlp_stemquestions; !!}
            </div>
        @endif
    </div>
</div>
@stop
@section('javascript')
<script type="text/javascript">
    $(document).ready( function () {
    $('.display').DataTable();
    } );
</script>
@stop