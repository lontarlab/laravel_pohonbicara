@extends('dashboard')

@section('content')

  <div class="row">
        <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
          {!! Form::open(['route' => 'range_suhus.search','class' => 'pull-right raw-margin-top-24 raw-margin-left-24']) !!}
            {!! csrf_field() !!}
          {!! Form::close() !!}
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('range_suhus.create') !!}">Add New</a>
            <h1 class="pull-left"><span class="fa fa-star"></span> Range Kriteria Suhu</h1>
        </div>
    </div>
<div class="container">
   <br><div class="row">
      <div class="col-md-5">
              <img src="{{ url('img/grafik_suhu.png') }}" width=500><br>       
      </div>   
       <div class="col-md-6">
        <li class="list-group-item list-group-item-success">1. Kategori rendah kurva naik turun diisi  data terendah hingga data menuju ke sedang</li>
        <li class="list-group-item list-group-item-info">2. Kategori sedang  kurva segitiga  diisi dengan data sedang rendah,  data sedang menengah dan data sedang meninggi</li>
        <li class="list-group-item list-group-item-warning">3. Kategori tinggi  kurva naik turun diisi dengan data tinggi terendah hingga data menuju ke paling tinggi</li>
        </div>
  </div>

   <div class="row">
        @if($range_suhus->isEmpty())
            <div class="well text-center">No pendaftarans found.</div>
        @else
     <div class="row raw-margin-top-24">
        <div class="col-md-11">
            <table class="display table table-striped">
                <thead>
                    <th>Range Rendah</th>
                    <th>Range Sedang</th>
                    <th>Range Tinggi</th>
                    <th width="400px">Action</th>
                </thead>
                <tbody>
                @foreach($range_suhus as $range_suhu)
                    <tr>
                        <td>{{ $range_suhu->rendah}}</td>
                        <td>{{ $range_suhu->sedang}}</td>
                        <td>{{ $range_suhu->tinggi}}</td>
                        <td>
                            <a href="{!! route('range_suhus.edit', [$range_suhu->id]) !!}"><i class="fa fa-pencil"></i> Edit</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

           
        @endif
    </div>
</div>
</div>
@stop
@section('javascript')
<script type="text/javascript">
    $(document).ready( function () {
    $('.display').DataTable();
    } );
</script>
@stop