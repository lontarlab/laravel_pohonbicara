@extends('dashboard')

@section('content')
      <script src="{{ url('chart/highcharts.js') }}"></script>
     @foreach($sensors as $key => $sensor)
        <?php $sensors1[] = intval($sensor->ph_tanah); ?>
        <?php $sensors2[] = intval($sensor->kelembapan); ?>
        <?php $sensors3[] = intval($sensor->cahaya); ?>
        <?php $sensors4[] = intval($sensor->suhu); ?>
        <?php $sensors6[] = $sensor->waktu; ?>
     @endforeach
 <div class="row">
            <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
               
                	<div class="row">	
                		<div class="col-md-4">
           			 		<h1><span class="fa fa-bar-chart"></span> Grafik Tanaman</h1>
           				</div>		
                		   <div class="col-md-2">
                				<!-- <br><a href="{!! url('/grafikhari') !!}"><button type="submit" name="submit" class="btn btn-info btn-fill btn-wd btn" value="Submit">Filter Grafik Perhari</button></a> -->
                		   </div>
                		   <div class="col-md-2">
                				<br><a href="{!! url('/grafikbulan') !!}"><button type="submit" name="submit" class="btn btn-info btn-fill btn-wd btn" value="Submit">Filter Grafik Perbulan</button></a>
                		   </div>
                		   <div class="col-md-2">
                				<br><a href="{!! url('/grafiktahun') !!}"><button type="submit" name="submit" class="btn btn-info btn-fill btn-wd btn" value="Submit">Filter Grafik Pertahun</button></a>
                		   </div>
				  	</div>
                	
           		
            </div>
        </div>

			<div class="row">
  				<div class="col-md-6 raw-margin-bottom-24 raw-margin-left-24">
                             <div id="semua_Sensor" style="width: 1000px; height: 400px; margin: 0 auto"></div>
                            <script>
										Highcharts.chart('semua_Sensor', {
									    chart: {
									        type: 'areaspline'
									    },
									    title: {
									        text: 'Grafik Data Sensor Tanaman'
									    },
									    legend: {
									        layout: 'vertical',
									        align: 'left',
									        verticalAlign: 'top',
									        x: 150,
									        y: 100,
									        floating: true,
									        borderWidth: 1,
									        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
									    },
									    xAxis: {
									        categories: <?= json_encode($sensors6); ?>,
									        plotBands: [{ // visualize the weekend
									            from: 4.5,
									            to: 6.5,
									            color: 'rgba(68, 170, 213, .2)'
									        }]
									    },
									    yAxis: {
									        title: {
									            text: 'Range Data Sensor'
									        }
									    },
									    tooltip: {
									        shared: true,
									        valueSuffix: ''
									    },
									    credits: {
									        enabled: false
									    },
									    plotOptions: {
									        areaspline: {
									            fillOpacity: 0.5
									        }
									    },
									     series: [{
										  name: 'Kelembapan Udara',
									        marker: {
									            symbol: 'square'
									        },
									        data: <?= json_encode($sensors1); ?>,

									    }, {
									        name: 'Suhu',
									        marker: {
									            symbol: 'square'
									        },
									        data:<?= json_encode($sensors2); ?>,
									     },{ name: 'Intensitas Cahaya',
									        marker: {
									            symbol: 'square'
									        },
									        data:<?= json_encode($sensors3); ?>,

									    	},{ name: 'Kelembapan Tanah',
									        marker: {
									            symbol: 'square'
									        },
									        data:<?= json_encode($sensors4); ?>,
									    }]
									});
						</script>
                    </div>
                </div>

            <div class="row">
                    <div class="col-md-6">
                             <div id="suhu" style="width: 500px; height: 400px; margin: 0 auto">
									<br>
                            		<script>
										Highcharts.chart('suhu', {
									    chart: {
									        type: 'spline'
									    },
									    title: {
									        text: 'Grafik Suhu Tanaman'
									    },
									    subtitle: {
									        text: ''
									    },
									    xAxis: {
									        categories: <?= json_encode($sensors6); ?>,
									    },
									    yAxis: {
									        title: {
									            text: 'Suhu'
									        },
									        labels: {
									            formatter: function () {
									                return this.value + '°';
									            }
									        }
									    },
									    tooltip: {
									        crosshairs: true,
									        shared: true
									    },
									    plotOptions: {
									        spline: {
									            marker: {
									                radius: 4,
									                lineColor: '#666666',
									                lineWidth: 1
									            }
									        }
									    },
									    series: [{
									        name: 'Suhu Tanaman',
									        marker: {
									            symbol: 'square'
									        },
									        data:<?= json_encode($sensors4); ?>,
									    }, {
									        
									    }]
									});
									</script>
                    	</div>
                    </div>

                   <div class="col-md-6">
                    <div id="kelembapan" style="width: 500px; height: 400px; margin: 0 auto">
									<br>
                                <script>
                                	 Highcharts.chart('kelembapan', {
									    chart: {
									        type: 'area',
									        spacingBottom: 30
									    },
									    title: {
									        text: 'Grafik Data Kelembapan Udara'
									    },
									    subtitle: {
									        text: '',
									        floating: true,
									        align: 'right',
									        verticalAlign: 'bottom',
									        y: 15
									    },
									    legend: {
									        layout: 'vertical',
									        align: 'left',
									        verticalAlign: 'top',
									        x: 150,
									        y: 100,
									        floating: true,
									        borderWidth: 1,
									        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
									    },
									    xAxis: {
									        categories: <?= json_encode($sensors6); ?>,
									    },
									    yAxis: {
									        title: {
									            text: 'Y-Axis'
									        },
									        labels: {
									            formatter: function () {
									                return this.value;
									            }
									        }
									    },
									    tooltip: {
									        formatter: function () {
									            return '<b>' + this.series.name + '</b><br/>' +
									                this.x + ': ' + this.y;
									        }
									    },
									    plotOptions: {
									        area: {
									            fillOpacity: 0.5
									        }
									    },
									    credits: {
									        enabled: false
									    },
									    series: [{
									        name: 'Kelembapan',
									        data: <?= json_encode($sensors2); ?>,
									    }, {
									       
									    }]
									});
                                </script>
                  </div>
                </div>
             
               <div class="row">
               		<div class="col-md-6">
                    <div id="ph_tanah" style="width: 500px; height: 400px; margin: 0 auto">
									<br>
                                <script>
                                	 Highcharts.chart('ph_tanah', {
									    chart: {
									        type: 'area',
									        spacingBottom: 30
									    },
									    title: {
									        text: 'Grafik Kelembapan Tanah'
									    },
									    subtitle: {
									        text: '',
									        floating: true,
									        align: 'right',
									        verticalAlign: 'bottom',
									        y: 15
									    },
									    legend: {
									        layout: 'vertical',
									        align: 'left',
									        verticalAlign: 'top',
									        x: 150,
									        y: 100,
									        floating: true,
									        borderWidth: 1,
									        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
									    },
									    xAxis: {
									        categories: <?= json_encode($sensors6); ?>,
									    },
									    yAxis: {
									        title: {
									            text: 'Y-Axis'
									        },
									        labels: {
									            formatter: function () {
									                return this.value;
									            }
									        }
									    },
									    tooltip: {
									        formatter: function () {
									            return '<b>' + this.series.name + '</b><br/>' +
									                this.x + ': ' + this.y;
									        }
									    },
									    plotOptions: {
									        area: {
									            fillOpacity: 0.5
									        }
									    },
									    credits: {
									        enabled: false
									    },
									    series: [{
									        name: 'Kelembapan Tanah',
									        data: <?= json_encode($sensors1); ?>,
									    }, {
									       
									    }]
									});
                                </script>
                  </div>
                </div>

                    <div class="col-md-6">
                             <div id="cahaya" style="width: 500px; height: 400px; margin: 0 auto">
									<br>
                            		<script>
										Highcharts.chart('cahaya', {
									    chart: {
									        type: 'spline'
									    },
									    title: {
									        text: 'Grafik Data Cahaya'
									    },
									    subtitle: {
									        text: ''
									    },
									    xAxis: {
									        categories:<?= json_encode($sensors6); ?>,
									    },
									    yAxis: {
									        title: {
									            text: 'Intensitas Cahaya'
									        },
									        labels: {
									            formatter: function () {
									                return this.value + '°';
									            }
									        }
									    },
									    tooltip: {
									        crosshairs: true,
									        shared: true
									    },
									    plotOptions: {
									        spline: {
									            marker: {
									                radius: 4,
									                lineColor: '#666666',
									                lineWidth: 1
									            }
									        }
									    },
									    series: [{
									        name: 'Cahaya Tanaman',
									        marker: {
									            symbol: 'square'
									        },
									        data:<?= json_encode($sensors3); ?>,
									    }, {
									        
									    }]
									});
									</script>
                    	</div>
                    </div>

                   
                                                    

@stop
