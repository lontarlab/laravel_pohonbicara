@extends('dashboard')

@section('content')
      <script src="{{ url('chart/highcharts.js') }}"></script>
     @foreach($sensors as $key => $sensor)
      <?php
            $jam1   = substr($sensor->waktu, 10, 3) * 3600;
            $menit1 = substr($sensor->waktu, 14, 2) * 60;
            $detik1 = substr($sensor->waktu, 17, 2);
           
            $waktu1 = $jam1 + $menit1 + $detik1; 
           
            $jam2  = substr($sensor->created_at, 10, 3) * 3600;
            $menit2 = substr($sensor->created_at, 14, 2) * 60;
            $detik2 = substr($sensor->created_at, 17, 2);

            $waktu2 = $jam2 + $menit2 + $detik2;
      
         $sensors1[] =  intval($waktu1); 
         $sensors2[] = intval($waktu2); 
         $sensors3[] = $sensor->waktu; 
        ?>
     @endforeach
 <div class="row">
            <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
               
                	<div class="row">	
                		<div class="col-md-5">
           			 		<h1><span class="fa fa-bar-chart"></span> Grafik Delay Waktu</h1>
           				</div>		
                		   <div class="col-md-2">
                			<!-- 	<br><a href="{!! url('/grafikhari2') !!}"><button type="submit" name="submit" class="btn btn-info btn-fill btn-wd btn" value="Submit">Filter Grafik Perhari</button></a> -->
                		   </div>
                		   <div class="col-md-2">
                				<br><a href="{!! url('/grafikbulan2') !!}"><button type="submit" name="submit" class="btn btn-info btn-fill btn-wd btn" value="Submit">Filter Grafik Perbulan</button></a>
                		   </div>
                		   <div class="col-md-2">
                				<br><a href="{!! url('/grafiktahun2') !!}"><button type="submit" name="submit" class="btn btn-info btn-fill btn-wd btn" value="Submit">Filter Grafik Pertahun</button></a>
                		   </div>
				  	</div>
                	
           		
            </div>
        </div>
			<div class="row">
  				<div class="col-md-6 raw-margin-bottom-24 raw-margin-left-24">
                             <div id="semua_Sensor" style="width: 1000px; height: 400px; margin: 0 auto"></div>
                            <script>
										Highcharts.chart('semua_Sensor', {
									    chart: {
									        type: 'areaspline'
									    },
									    title: {
									        text: 'Grafik Delay Waktu Sensors'
									    },
									    legend: {
									        layout: 'vertical',
									        align: 'left',
									        verticalAlign: 'top',
									        x: 150,
									        y: 100,
									        floating: true,
									        borderWidth: 1,
									        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
									    },
									    xAxis: {
									        categories: <?= json_encode($sensors3); ?>,
									        plotBands: [{ // visualize the weekend
									            from: 4.5,
									            to: 6.5,
									            color: 'rgba(68, 170, 213, .2)'
									        }]
									    },
									    yAxis: {
									        title: {
									            text: 'Range Detik Sensor'
									        }
									    },
									    tooltip: {
									        shared: true,
									        valueSuffix: ''
									    },
									    credits: {
									        enabled: false
									    },
									    plotOptions: {
									        areaspline: {
									            fillOpacity: 0.5
									        }
									    },
									     series: [{
										  name: 'Waktu Sensor Pada  Alat',
									        marker: {
									            symbol: 'square'
									        },
									        data: <?= json_encode($sensors1); ?>,

									    }, {
									        name: 'Waktu Sensors Pada Aplikasi',
									        marker: {
									            symbol: 'square'
									        },
									        data:<?= json_encode($sensors2); ?>,
									     
									    	
									    }]
									});
						</script>
                    </div>
                </div>
@stop