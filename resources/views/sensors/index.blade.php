
@extends('dashboard')
@section('content')

<div class="">
    {{ Session::get('message') }}
</div>
<div class="row">
       <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
         <div class="pull-right">
                  </div>
             
            <h1 class="pull-left"><span class="fa fa-laptop"></span> Informasi Data Sensors </</h1>
          
            </div>
        </div>
<div class="row">
  <div class="col-md-12">
  <a class="weatherwidget-io" href="https://forecast7.com/en/n6d92107d62/bandung/" data-label_1="BANDUNG" data-label_2="WEATHER" data-theme="original" >BANDUNG WEATHER</a>
<script>
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
</script>
</div>
</div>
<br><div id="exTab2" class="container">
<ul class="nav nav-tabs">
      <li class="active">
      <a  href="#satu" data-toggle="tab">Data Sensors</a></li>
      <li><a href="#4" data-toggle="tab">SPK Suhu</a></li>
      <li><a href="#5" data-toggle="tab">SPK Kelembapan Udara</a></li>
      <li><a href="#6" data-toggle="tab">SPK Kelembapan Tanah</a></li>
      <li><a href="#7" data-toggle="tab">SPK Cahaya</a></li>
       <li><a href="#8" data-toggle="tab">SPK Tanaman</a></li>
      
    </ul>
      <div class="tab-content ">
        <div class="tab-pane active" id="satu">
           <div class="row">
             <div class="col-md-11">
               <br> <table class="display table table-striped">
                <thead>
                    <th>Waktu Sensors</th>
                    <th>Waktu Kirim</th>
                    <th>Delay Detik </th>
                    <th>Konversi</th>
                    <th>Suhu</th>
                    <th>Kelembapan Udara</th>
                    <th>Kelembapan Tanah</th>
                    <th>Cahaya</th>
                    <!-- <th width="150px">Action</th> -->
                </thead>
                <tbody>
                @foreach($sensors as $sensor)
                    <tr>
                        <td>{{ $sensor->waktu }}</td>
                        <td>{{ substr($sensor->created_at ,10) }}</td>
                         <?php  $jam1   = substr($sensor->waktu, 10, 3) * 3600;
                        $menit1 = substr($sensor->waktu, 14, 2) * 60;
                        $detik1 = substr($sensor->waktu, 17, 2);
                        $waktu1 = $jam1 + $menit1 + $detik1; 

                        $jam2  = substr($sensor->created_at, 10, 3) * 3600;
                        $menit2 = substr($sensor->created_at, 14, 2) * 60;
                        $detik2 = substr($sensor->created_at, 17, 2);
                        $waktu2 = $jam2 + $menit2 + $detik2;
                        $totalDetik = abs($waktu2 - $waktu1);
                        $jam = abs($totalDetik / 3600);
                        $modMenit = $totalDetik % 3600;
                        $menit    = abs($modMenit / 60);
                        $detik   = abs($modMenit % 60);
                        $menit1 = number_format($menit, 2);
                        if($menit > 10 ){
                             $menit2  = substr($menit, 0, 2);
                           }elseif($menit >= 0) {
                            $menit2  = substr($menit, 0, 1);
                           }
                        
                        ?>
                        <td>{{ $totalDetik }} detik</td>
                        <td>{{ number_format($jam, 0) }} jam 
                            {{ $menit2 }} menit
                            {{ number_format($detik, 0)   }} detik </td>
                        <td>{{ $sensor->suhu }}</td>
                        <td>{{ $sensor->kelembapan }}</td>
                        <td>{{ $sensor->ph_tanah }}</td>
                        <td>{{ $sensor->cahaya }}</td>
                       @endforeach
                      </tbody>                  
                  </table>
              </div> 
            </div>
         </div>

   
        
  <!-- Notifikasi SPK SUHU -->
  <div class="tab-pane" id="4">
      <div class="row">
        <div class="col-md-11">
                  <br><table class="display table table-striped">
                      <thead>
                                  
                        <th>Waktu Sensor</th>
                          <th>Suhu</th>
                          <th>Kriteria Satu</th>
                          <th>Kriteria Dua</th>
                          <th>Kriteria Tiga</th>
                          <th>Analisis Kriteria</th>
                      </thead>
                      <tbody>
                      
                      @foreach($sensors as $sensor)
                       
                          <tr>
                            <td>{{ $sensor->waktu }}</td>
                              <td>{{ $sensor->suhu }}</td>
                              <td> 
                                @if($sensor->suhu >= $kreteria[0]->rendah)
                                 0
                                @elseif($kreteria[1]->rendah <= $sensor->suhu && $sensor->suhu <= $kreteria[1]->rendah)
                                 $kecil = abs($kreteria[1]->rendah - $sensor->suhu / $kreteria[1]->rendah - $kreteria[0]->rendah)
                                 {{ number_format($kecil, 2) }} 
                                @else
                                1
                                @endif
                              </td>
                              <td>  
                                @if($sensor->suhu <= $kreteria[0]->sedang)
                                0
                                @elseif($kreteria[0]->sedang <= $sensor->suhu && $sensor->suhu <= $kreteria[1]->sedang)
                               {{ number_format(  $kreteria[0]->sedang - $sensor->suhu    / $kreteria[1]->sedang - $kreteria[0]->sedang , 2)}}
                                @elseif($kreteria[0]->sedang <= $sensor->suhu && $sensor->suhu <= $kreteria[2]->sedang)
                                 {{ number_format($kreteria[2]->sedang - $sensor->suhu / $kreteria[2]->sedang - $kreteria[1]->sedang)}} 
                                 @elseif($sensor->suhu >= $kreteria[2]->sedang)
                                  0 
                                 @else
                                  1
                                 @endif
                              </td>
                              <td>
                                @if($sensor->suhu <= $kreteria[0]->tinggi)
                                0
                                @elseif(30 <= $sensor->suhu && $sensor->suhu <= 35)
                                {{ number_format($kreteria[0]->tinggi - $sensor->suhu / $kreteria[1]->tinggi - $kreteria[0]->tinggi,2)}}
                                @else
                                1
                                @endif
                              </td>
                              <td>
                                 @if($sensor->suhu > 0 && $sensor->suhu <= 10)
                                <h4><span class="label label-warning">Suhu Sangat Rendah</span></h4>
                                 @elseif($sensor->suhu > 10 && $sensor->suhu <= 15)
                                <h5><span class="label label-war">Suhu agak Rendah</span></h5>
                                 @elseif($sensor->suhu > 15 && $sensor->suhu  <= 20 )
                                <h4><span class="label label-success">Suhu Mulai sedang </span></h4>
                                 @elseif($sensor->suhu > 20 && $sensor->suhu <= 25)
                                  <h4><span class="label label-success">Suhu sedang </span></h4>
                                 @elseif($sensor->suhu  > 25 && $sensor->suhu <= 30)
                                  <h4><span class="label label-primary ">Suhu Mulai Panas </span></h4>
                                 @elseif($sensor->suhu  > 30 && $sensor->suhu <= 35)
                                  <h4><span class="label label-da">Suhu Panas </span></h4>
                                 @elseif($sensor->suhu > 35 && $sensor->suhu <= 40)
                                  <h4><span class="label label-danger">Suhu Sangat Panas </span></h4>
                                 @endif

                              </td>
                                                         
                          </tr>
                        
                      @endforeach
                      </tbody>                  
                  </table>
              </div>        
            </div>
          </div>

<!-- Notifikasi SPK Kelembapan Udara-->
  <div class="tab-pane" id="5">
      <div class="row">
        <div class="col-md-11">
                  <br><table class="display table table-striped">
                      <thead>       
                        <th>Waktu Sensor</th>
                          <th>Kelembapan</th>
                          <th>Kriteria Satu</th>
                          <th>Kriteria Dua</th>
                          <th>Kriteria Tiga</th>
                          <th>Analisis Kriteria</th>
                      </thead>
                      <tbody>
                      
                      @foreach($sensors as $sensor)
                       
                          <tr>
                            <td>{{ $sensor->waktu }}</td>
                              <td>{{ $sensor->kelembapan }}</td>
                              <td> 
                                @if($sensor->kelembapan >= $kreteria_phtanah[0]->rendah)
                                 0
                                @elseif($kreteria_phtanah[1]->rendah <= $sensor->kelembapan && $sensor->kelembapan <= $kreteria_phtanah[1]->rendah)
                                   {{ number_format($kreteria_phtanah[1]->rendah - $sensor->kelembapan / $kreteria_phtanah[1]->rendah - $kreteria_phtanah[0]->rendah, 2) }}
                                @else
                                1
                                @endif
                              </td>
                              <td>  
                                @if($sensor->kelembapan <= $kreteria_phtanah[0]->sedang)
                                0
                                @elseif($kreteria_phtanah[0]->sedang <= $sensor->kelembapan && $sensor->kelembapan <= $kreteria_phtanah[1]->sedang)
                                 {{ number_format($sensor->kelembapan  - $kreteria_phtanah[0]->sedang    / $kreteria_phtanah[1]->sedang - $kreteria_phtanah[0]->sedang,2) }}                               
                                @elseif($kreteria_phtanah[0]->sedang <= $sensor->phtanah && $sensor->phtanah <= $kreteria_phtanah[2]->sedang)
                                 {{ number_format($kreteria_phtanah[2]->sedang - $sensor->phtanah / $kreteria_phtanah[2]->sedang - $kreteria_phtanah[1]->sedang,2) }} 
                                 @elseif($sensor->phtanah >= $kreteria_phtanah[2]->sedang)
                                  0 
                                 @else
                                  1
                                 @endif
                              </td>
                              <td>
                                @if($sensor->kelembapan <= $kreteria_phtanah[0]->tinggi)
                                0
                                @elseif(30 <= $sensor->kelembapan && $sensor->kelembapan <= 35)
                                {{$sensor->kelembapan - $kreteria_phtanah[0]->tinggi / $kreteria_phtanah[1]->tinggi - $kreteria_phtanah[0]->tinggi}}
                                @else
                                1
                                @endif
                              </td>
                              <td>
                                 @if($sensor->kelembapan > 0 && $sensor->kelembapan <= 20)
                                  <h4><span class="label label-warning"> Kelembapan Udara Sangat Kering</span></h4>
                                 @elseif($sensor->kelembapan > 20 && $sensor->kelembapan <= 30)
                                  <h4><span class="label label-warning"> Kelembapan Udara Kering</span></h4>
                                 @elseif($sensor->kelembapan > 30 && $sensor->kelembapan <= 50)
                                  <h4><span class="label label-success"> Kelembapan Udara Normal</span></h4>
                                 @elseif($sensor->kelembapan > 50 && $sensor->kelembapan  <= 70 )
                                  <h4><span class="label label-primary"> Kelembapan Udara Sedikit Tinggi</span></h4>
                                 @elseif($sensor->kelembapan > 70 && $sensor->kelembapan <= 85)
                                  <h4><span class="label label-danger"> Kelembapan Udara Tinggi</span></h4>
                                 @elseif($sensor->kelembapan  > 85 && $sensor->kelembapan <= 100)
                                  <h4><span class="label label-danger"> Kelembapan Udara Sangat Tinggi</span></h4>
                                 @endif

                              </td>
                                                         
                          </tr>
                        
                      @endforeach
                      </tbody>                  
                  </table>
              </div>        
            </div>
             </div>

<!-- Notifikasi SPK KELEMBAPAB TANAH -->
  <div class="tab-pane" id="6">
      <div class="row">
        <div class="col-md-11">
                 <br><table class="display table table-striped">
                      <thead>
                                  
                        <th>Waktu Sensor</th>
                          <th>Kelembapan</th>
                          <th>Kriteria Satu</th>
                          <th>Kriteria Dua</th>
                          <th>Kriteria Tiga</th>
                          <th>Analisis Kriteria</th>
                      </thead>
                      <tbody>
                      
                      @foreach($sensors as $sensor)
                       
                          <tr>
                            <td>{{ $sensor->waktu }}</td>
                              <td>{{ $sensor->ph_tanah }}</td>
                              <td> 
                                @if($sensor->ph_tanah >= $kreteria_kelembapan[0]->rendah)
                                 0
                                @elseif($kreteria_kelembapan[1]->rendah <= $sensor->kelembapan && $sensor->ph_tanah <= $kreteria_kelembapan[1]->rendah)
                                    {{ $kreteria_kelembapan[1]->rendah - $sensor->ph_tanah / $kreteria_kelembapan[1]->rendah - $kreteria_kelembapan[0]->rendah }}
                                @else
                                1
                                @endif
                              </td>
                              <td>  
                                @if($sensor->ph_tanah <= $kreteria_kelembapan[0]->sedang)
                                0
                                @elseif($kreteria_kelembapan[0]->sedang <= $sensor->ph_tanah && $sensor->ph_tanah <= $kreteria_kelembapan[1]->sedang)
                                 {{ number_format($sensor->ph_tanah  - $kreteria_kelembapan[0]->sedang    / $kreteria_kelembapan[1]->sedang - $kreteria_kelembapan[0]->sedang,2) }}                               
                                @elseif($kreteria_kelembapan[0]->sedang <= $sensor->kelembapan && $sensor->ph_tanah <= $kreteria_kelembapan[2]->sedang)
                                  {{ number_format($kreteria_kelembapan[2]->sedang - $sensor->ph_tanah / $kreteria_kelembapan[2]->sedang - $kreteria_kelembapan[1]->sedang,2) }} 
                                 @elseif($sensor->ph_tanah >= $kreteria_kelembapan[2]->sedang)
                                  0 
                                 @else
                                  1
                                 @endif
                              </td>
                              <td>
                                @if($sensor->ph_tanah <= $kreteria_kelembapan[0]->tinggi)
                                0
                                @elseif(30 <= $sensor->ph_tanah && $sensor->ph_tanah <= 35)
                              {{ number_format($sensor->ph_tanah - $kreteria_kelembapan[0]->tinggi / $kreteria_kelembapan[1]->tinggi - $kreteria_kelembapan[0]->tinggi,2)}}
                                @else
                                1
                                @endif
                              </td>
                              <td>
                                 @if($sensor->ph_tanah >= 0 && $sensor->ph_tanah <= 20)
                                <h4><span class="label label-danger"> Kelembapan Tanah Sangat Kering</span></h4>
                                 @elseif($sensor->ph_tanah > 20 && $sensor->ph_tanah <= 30)
                                 <h4><span class="label label-danger"> Kelembapan Tanah Kering</span></h4>
                                 @elseif($sensor->ph_tanah > 30 && $sensor->ph_tanah <= 50)
                                 <h4><span class="label label-primary"> Kelembapan Tanah Agak Kering</span></h4>
                                 @elseif($sensor->ph_tanah > 50 && $sensor->ph_tanah  <= 70 )
                                  <h4><span class="label label-success">  Kelembapan Tanah Sedikit Basah</span></h4>
                                
                                 @elseif($sensor->ph_tanah > 70 && $sensor->ph_tanah <= 85)
                                  <h4><span class="label label-warning">   Kelembapan Tanah Basah</span></h4>
                                
                                 @elseif($sensor->ph_tanah > 85 && $sensor->ph_tanah <= 1030)
                                  <h4><span class="label label-warning">   Kelembapan Tanah Sangat  Basah</span></h4>
                                
                                 @endif

                              </td>
                                                         
                          </tr>
                        
                      @endforeach
                      </tbody>                  
                  </table>
                </div>
            </div>
             </div>

<!-- Notifikasi SPK PH Cahaya -->
  <div class="tab-pane" id="7">
      <div class="row">
        <div class="col-md-11">
                  <br><table class="display table table-striped">
                      <thead>       
                        <th>Waktu Sensor</th>
                          <th>Intensitas Cahaya</th>
                          <th>Kriteria Satu</th>
                          <th>Kriteria Dua</th>
                          <th>Analisis Kriteria</th>
                      </thead>
                      <tbody>
                      
                      @foreach($sensors as $sensor)
                       
                          <tr>
                            <td>{{ $sensor->waktu }}</td>
                              <td>{{ $sensor->cahaya }}</td>
                              <td> 
                                @if($sensor->cahaya == 1)
                                 1
                                 @else 
                                 0
                                @endif
                              </td>
                              <td>  
                                @if($sensor->cahaya == 0)
                                1
                                @else 
                                0
                                 @endif
                              </td>
                              <td>
                                 @if($sensor->cahaya == 1)
                                  <h4><span class="label label-success"> Cahaya Terang</span></h4>
                                 @elseif($sensor->cahaya == 0)
                                 <h4><span class="label label-danger"> Cahaya Gelap</span></h4>
                                 @endif

                              </td>
                                                         
                          </tr>
                        
                      @endforeach
                      </tbody>                  
                  </table>
              </div>        
            </div>
             </div>

             <!-- Notifikasi SPK PH Cahaya -->
 <div class="tab-pane" id="8">
      <div class="row">
        <div class="col-md-11">       
                 <br> <table class="display table table-striped">
                      <thead>         
                        <th>Waktu Sensor</th>
                          <th>Suhu</th>
                          <th>Kelembapan Udara</th>
                          <th>Kelembapan Tanah</th>
                          <th>Cahaya</th>
                          <th>Analisis SPK Tanaman</th>
                      </thead>
                      <tbody>
                     
                      @foreach($sensors as $sensor)
                          <tr>
                            <td>{{ $sensor->waktu }}</td>
                              <td>{{ $sensor->suhu }} </td>
                              <td>{{ $sensor->kelembapan }} </td>
                              <td>{{ $sensor->ph_tanah }} </td>
                              <td>{{ $sensor->cahaya }}</td>
                              <td> 

                            
                                  <!-- Tanaman Normal -->
                                  @if($sensor->suhu > 20 &&  $sensor->suhu < 35 && $sensor->kelembapan >= 30 && $sensor->kelembapan <= 60 && $sensor->ph_tanah >= 40 && $sensor->ph_tanah < 70  )
                                  <h4><span class="label label-success">Kelembapan Tanah Normal  Suhu dan Kelembapan Udara normal</span></h4>

                                    <!-- Tanaman Normal -->
                                  @elseif($sensor->suhu > 20 &&  $sensor->suhu < 35 && $sensor->kelembapan > 60 && $sensor->kelembapan < 100 && $sensor->ph_tanah >= 40 && $sensor->ph_tanah < 70  )
                                  <h4><span class="label label-success">Kelembapan Udara Tinggi  Suhu dan Kelembapan Tanah normal</span></h4>

                                  <!-- Jangan Siram Tanaman   -->
                                 @elseif( $sensor->ph_tanah < 40)
                                 <h4><span class="label label-danger"> kelembapan tanah kering, segera siram tanaman</span></h4>
                                  <!-- Siram Tanaman  -->
                                  @elseif($sensor->ph_tanah >= 70)
                                   <h4><span class="label label-warning"> 
                                  Kelembapan tanah masih sangat basah, disarankan tidak untuk menyiram tanaman</span></h4>
                                  @endif
                              </td>                        
                          </tr>
                      @endforeach
                      </tbody>                  
                  </table>
              </div>        
            </div>
        </div>
</div>
@stop


@section('javascript')
<script type="text/javascript">
    $(document).ready( function () {
    $('.display').DataTable({
   "order": [[ 0, 'desc' ]]
});
    } );
</script>
@stop