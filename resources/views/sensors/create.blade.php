@extends('dashboard')

@section('content')
<div class="row">
            <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
                 <div class="pull-right">
                  </div>
            <h1 class="pull-left"><span class="fa fa-database"></span> Kelola Tanaman</h1>
            </div>
        </div>

<div class="row">
  <div class="col-md-6 raw-margin-bottom-24 raw-margin-left-24">
  	  {!! Form::open(['route' => 'sensors.store', 'files' => true]) !!}
              {!! csrf_field() !!}

			<input type="hidden" name="user_id"  class="form-control" value="{{Auth::user()->id}}" required>
    		 <div class="raw-margin-top-24">
                  @input_maker_label('tanggal')
                  @input_maker_create('tanggal', ['type' => 'date'])
              </div>
              <div class="raw-margin-top-24">
                  @input_maker_label('waktu')
                  @input_maker_create('waktu', ['type' => 'time'])
              </div>
              <div class="raw-margin-top-24">
                  @input_maker_label('ph_tanah')
                  @input_maker_create('ph_tanah', ['type' => 'string'])
              </div>
              <div class="raw-margin-top-24">
                  @input_maker_label('kelembapan')
                  @input_maker_create('kelembapan', ['type' => 'string'])
              </div>
              <div class="raw-margin-top-24">
                  @input_maker_label('suhu')
                  @input_maker_create('suhu', ['type' => 'string'])
              </div>
              <div class="raw-margin-top-24">
                  @input_maker_label('cahaya')
                  @input_maker_create('cahaya', ['type' => 'string'])
              </div>
             
              <div class="raw-margin-top-24">
                  <a class="btn btn-default pull-left" href="{!! route('sensors.index') !!}">Cancel</a>
                  <button class="btn btn-primary pull-right" type="submit">Create</button>
              </div>
<br/></br/><br/></br/><br/></br/>
          {!! Form::close() !!}
      </div>


    </div>
</div>

</div>
@stop
