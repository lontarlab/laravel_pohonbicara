@extends('dashboard')

@section('content')
<div class="row">
            <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
                 <div class="pull-right">
                  </div>
            <h1 class="pull-left"><span class="fa fa-database"></span> Kelola Tanaman</h1>
            </div>
        </div>

<div class="row">
  <div class="col-md-6 raw-margin-bottom-24 raw-margin-left-24">

    {!! Form::model($sensor, ['route' => ['sensors.update', $sensor->id], 'method' => 'patch', 'files' =>true]) !!}
     {!! csrf_field() !!}    {!! method_field('PATCH') !!}

               
                     @form_maker_object($sensor,[
                    'ph_tanah'=>['alt_name'=>'ph_tanah'],
                    'kelembapan'=>['alt_name'=>'kelembapan'],
                    'cahaya'=>['alt_name'=>'cahaya'],
                    'suhu'=>['alt_name'=>'suhu'],
                    'tanaman_id'=>['alt_name'=>'tanaman_id'],
                    ])
           
        <div class="raw-margin-top-24">
                      <a class="btn btn-default pull-left" href="{!! route('kelola_tanamen.index') !!}">Cancel</a>
                      <button class="btn btn-primary pull-right" type="submit">Save</button>
                  </div>

            {!! Form::close() !!}
          </div>
      </div>
  </div>

@stop