<div class="">
    {{ Session::get('message') }}
</div>

<div class="container">

    {!! Form::open(['route' => 'sensor_chats.store']) !!}

    @form_maker_table("sensor_chats")

    {!! Form::submit('Save') !!}

    {!! Form::close() !!}

</div>