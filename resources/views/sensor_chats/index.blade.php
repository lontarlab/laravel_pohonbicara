<div class="container">

    <div class="">
        {{ Session::get('message') }}
    </div>

    <div class="row">
        <div class="pull-right">
            {!! Form::open(['route' => 'sensor_chats.search']) !!}
            <input class="form-control form-inline pull-right" name="search" placeholder="Search">
            {!! Form::close() !!}
        </div>
        <h1 class="pull-left">SensorChats</h1>
        <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('sensor_chats.create') !!}">Add New</a>
    </div>

    <div class="row">
        @if($sensor_chats->isEmpty())
            <div class="well text-center">No sensorChats found.</div>
        @else
            <table class="table">
                <thead>
                    <th>Name</th>
                    <th width="50px">Action</th>
                </thead>
                <tbody>
                @foreach($sensor_chats as $sensor_chat)
                    <tr>
                        <td>
                            <a href="{!! route('sensor_chats.edit', [$sensor_chat->id]) !!}">{{ $sensor_chat->name }}</a>
                        </td>
                        <td>
                            <a href="{!! route('sensor_chats.edit', [$sensor_chat->id]) !!}"><i class="fa fa-pencil"></i> Edit</a>
                            <form method="post" action="{!! route('sensor_chats.destroy', [$sensor_chat->id]) !!}">
                                {!! csrf_field() !!}
                                {!! method_field('DELETE') !!}
                                <button type="submit" onclick="return confirm('Are you sure you want to delete this sensor_chat?')"><i class="fa fa-trash"></i> Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="row">
                {!! $sensor_chats; !!}
            </div>
        @endif
    </div>
</div>