<div class="">
    {{ Session::get('message') }}
</div>

<div class="container">

    {!! Form::model($sensor_chat, ['route' => ['sensor_chats.update', $sensor_chat->id], 'method' => 'patch']) !!}

    @form_maker_object($sensor_chat, FormMaker::getTableColumns('sensor_chats'))

    {!! Form::submit('Update') !!}

    {!! Form::close() !!}
</div>
