@extends('dashboard')
@section('content')
<div class="container">

    <div class="">
        {{ Session::get('message') }}
    </div>

    
    <div class="row">
        <div class="" >
            {!! Form::open(['route' => 'nlp_stopwords.search', 'class' => 'pull-right raw-margin-top-24 raw-margin-left-24']) !!}
            <input class="form-control form-inline pull-right" name="search" placeholder="Search">
            {!! Form::close() !!}
        </div>
       
        <h1 class="pull-left">Nlp Stopwords</h1>
        <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('nlp_stopwords.create') !!}">Add New</a>
    </div>

    <div class="row">
        @if($nlp_stopwords->isEmpty())
            <div class="well text-center">No nlpStopwords found.</div>
        @else
            <table class="table">
                <thead>
                    <th>Stopword</th>
                    <th>Action</th>
                </thead>
                <tbody>
                @foreach($nlp_stopwords as $nlp_stopword)
                    <tr>
                        <td>
                            <a href="{!! route('nlp_stopwords.edit', [$nlp_stopword->id]) !!}">{{ $nlp_stopword->kata_stopwords }}</a>
                        </td>
                        
                        <td>
                            <a style="margin:1px;" class="btn btn-warning btn-xs pull-right raw-margin-right-16" href="{!! route('nlp_stopwords.edit', [$nlp_stopword->id]) !!}"><span class="fa fa-pencil"></span> Ubah</a>
                             <form method="post" action="{!! route('nlp_stopwords.destroy', [$nlp_stopword->id]) !!}">
                                {!! csrf_field() !!}
                                {!! method_field('DELETE') !!}
                            <button style="margin:1px;" class="btn btn-danger btn-xs pull-right" type="submit" onclick="return confirm('Are you sure you want to delete this nlp_stopwords?')"><i class="fa fa-trash"></i> Hapus</button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="row">
                {!! $nlp_stopwords; !!}
            </div>
        @endif
    </div>
</div>
@stop
@section('javascript')
<script type="text/javascript">
    $(document).ready( function () {
    $('.display').DataTable();
    } );
</script>
@stop