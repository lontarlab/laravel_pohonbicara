@if ($user=Auth::user())
<div class="container sidebar-wrapper">
    <div class="toggle-wrapper">
        <button type="button" class="sidebar-toggle" data-toggle="collapse" data-target="#accordion">
            <span class="sr-only">Main Menu</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <div class="panel-group collapse" id="accordion">

      <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{!! url('dashboard') !!}"><span class="fa fa-home"></span> Beranda</a>
            </div>
             
      </div>


      @if (in_array($user->roles->first()->name, ['member']))
      <div class="panel panel-default">
            <div class="panel-heading">
                <a data-toggle="collapse" data-parent="#accordion" href="#Pengguna"><span class="fa fa-user"></span> Manajamen Pengguna</a>
            </div>
            <div id="Pengguna" class="panel-collapse collapse">
                <div class="panel-body">
                  <ul class="nav nav-sidebar">
                    <li>
                        <a href="{!! url('/user/settings') !!}"><span class="fa fa-user"></span>Data Pengguna</a>
                    </li>
                  </ul>
                </div>
            </div>
      </div>

       <div class="panel panel-default">
            <div class="panel-heading">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapsecontent"><span class="fa fa-tree"></span> Manajamen Tanaman</a>
            </div>
            <div id="collapsecontent" class="panel-collapse collapse">
                <div class="panel-body">
                  <ul class="nav nav-sidebar">
                    <li>
                        <a href="{!! url('kelola_tanamen') !!}"><span class="fa fa-tree"></span>Kelola Tanaman</a>
                    </li>
                    <li>
                        <a href="{!! url('sensors') !!}"><span class="fa fa-database"></span>Data Sensor</a>
                    </li>
                    <li>
                        <a href="{!! url('grafik') !!}"><span class="fa fa-bar-chart"></span> Grafik Sensor</a>
                    </li>
                    <li>
                        <a href="{!! url('grafik_waktu') !!}"><span class="fa fa-server"></span>Grafik Delay Sensors</a>
                    </li>
                  
                  </ul>
                </div>
            </div>
      </div>
         <div class="panel panel-default">
            <div class="panel-heading">
                <a data-toggle="collapse" data-parent="#accordion" href="#pohonbicara"><span class="fa fa-laptop"></span> Manajemen Pohon Bicara</a>
            </div>
            <div id="pohonbicara" class="panel-collapse collapse">
                <div class="panel-body">
                  <ul class="nav nav-sidebar">
                    <li>
                        <a href="{!! url('pohonbicara') !!}"><span class="fa fa-laptop"></span>Pohon Bicara</a>
                    </li>
                    <!--  <li>
                        <a href="{!! url('contents') !!}"><span class="fa fa-bar-chart"></span>Notifikasi Line </a>
                    </li> -->
                  </ul>
                </div>
            </div>
      </div>
      @endif
        @if (in_array($user->roles->first()->name, ['admin']))
           <div class="panel panel-default">
            <div class="panel-heading">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapsecontent"><span class="fa fa-newspaper-o"></span> Manajemen Konten</a>
            </div>
            <div id="collapsecontent" class="panel-collapse collapse">
                <div class="panel-body">
                  <ul class="nav nav-sidebar">
                    <li>
                        <a href="{!! url('contents') !!}"><span class="fa fa-file-text"></span> Artikel</a>
                    </li>
                    <!-- <li>
                        <a href="{!! url('comments') !!}"><span class="fa fa-comment-o"></span> Komentar</a>
                    </li> -->
                    <li>
                        <a href="{!! url('categories') !!}"><span class="fa fa-paperclip"></span> Referensi - Kategori</a>
                    </li>
                    <!-- <li>
                        <a href="{!! url('topics') !!}"><span class="fa fa-quote-right"></span> Referensi - Topik</a>
                    </li>
                     <li>
                        <a href="{!! url('admin/users') !!}"><span class="fa fa-users"></span> Users</a>
                    </li>
                    <li>
                        <a href="{!! url('offlinewriters') !!}"><span class="fa fa-male"></span> Referensi - Offline Penulis</a>
                    </li> -->
                  </ul>
                </div>
            </div>
      </div>

        <div class="panel panel-default">
              <div class="panel-heading">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapsePengguna"><span class="fa fa-cog"></span> Pengaturan Admin</a>
              </div>
              <div id="collapsePengguna"" class="panel-collapse collapse">
                  <div class="panel-body">
                  <ul class="nav nav-sidebar">
                    <li>
                        <a href="{!! url('companies/1/edit') !!}"><span class="fa fa-building"></span> Pengaturan Umum</a>
                    </li>
                    <li>
                        <a href="{!! url('admin/roles') !!}"><span class="fa fa-lock"></span> Roles</a>
                    </li>
                    <li>
                        <a href="{!! url('logsystems') !!}"><span class="fa fa-database"></span> Log Sistem</a>
                    </li>
                  </ul>
                  </div>
              </div>
        </div>

        <div class="panel panel-default">
              <div class="panel-heading">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapsesystemadmin"><span class="fa fa-user"></span> Manajemen Pengguna</a>
              </div>
              <div id="collapsesystemadmin" class="panel-collapse collapse">
                  <div class="panel-body">
                  <ul class="nav nav-sidebar">
                    <li>
                        <a href="{!! url('pendaftarans') !!}"><span class="fa fa-building"></span> Aktivasi Pendaftaran</a>
                    </li>
                    <li>
                        <a href="{!! url('admin/users') !!}"><span class="fa fa-users"></span> Users</a>
                    </li>
                    
                  </ul>
                  </div>
              </div>
        </div>
      
      <div class="panel panel-default">
            <div class="panel-heading">
                <a data-toggle="collapse" data-parent="#accordion" href="#tanamanadmin"><span class="fa fa-tree"></span> Manajamen Tanaman</a>
            </div>
            <div id="tanamanadmin" class="panel-collapse collapse">
                <div class="panel-body">
                  <ul class="nav nav-sidebar">
                    <li>
                        <a href="{!! url('tanaman') !!}"><span class="fa fa-tree"></span>Kelola Tanaman</a>
                    </li>
                    <li>
                        <a href="{!! url('sensor') !!}"><span class="fa fa-database"></span>Data Sensor</a>
                    </li>
                    <li>
                        <a href="{!! url('admingrafik') !!}"><span class="fa fa-bar-chart"></span> Grafik Sensor</a>
                    </li>
                    <li>
                        <a href="{!! url('grafik_waktu') !!}"><span class="fa fa-server"></span>Grafik Delay Waktu</a>
                    </li>
                  
                  </ul>
                </div>
            </div>
      </div>

       <div class="panel panel-default">
            <div class="panel-heading">
                <a data-toggle="collapse" data-parent="#accordion" href="#pohonbicara"><span class="fa fa-laptop"></span> Manajemen Pohon Bicara</a>
            </div>
            <div id="pohonbicara" class="panel-collapse collapse">
                <div class="panel-body">
                  <ul class="nav nav-sidebar">
                    <li>
                        <a href="{!! url('manajemenpohonbicara') !!}"><span class="fa fa-laptop"></span>Pohon Bicara</a>
                    </li>
                    <!--  <li>
                        <a href="{!! url('contents') !!}"><span class="fa fa-bar-chart"></span>Notifikasi SPK</a>
                    </li> -->
                  </ul>
                </div>
            </div>
      </div>

         <div class="panel panel-default">
            <div class="panel-heading">
                <a data-toggle="collapse" data-parent="#accordion" href="#spk"><span class="fa fa-flask"></span> SPK Tanaman</a>
            </div>
            <div id="spk" class="panel-collapse collapse">
                <div class="panel-body">
                  <ul class="nav nav-sidebar">
                    <li>
                        <a href="{!! url('range_suhus') !!}"><span class="fa fa-flask"></span>Kriteria Suhu</a>
                    </li>
                     <li>
                       <a href="{!! url('range_kelembapans') !!}"><span class="fa fa-flask"></span>Kriteria Kelembapan Tanah</a>
                    </li>
                   <!--  <li>
                       <a href="{!! url('range_cahayas') !!}"><span class="fa fa-laptop"></span>Data Kriteria Cahaya</a>
                    </li> -->
                    <li>
                       <a href="{!! url('range_phtanahs') !!}"><span class="fa fa-flask"></span>Kriteria Kelembapan Udara</a>
                    </li>
                  </ul>
                </div>
            </div>
      </div>

      <div class="panel panel-default">
            <div class="panel-heading">
                <a data-toggle="collapse" data-parent="#accordion" href="#nlp"><span class="fa fa-language"></span> NLP</a>
            </div>
            <div id="nlp" class="panel-collapse collapse">
                <div class="panel-body">
                  <ul class="nav nav-sidebar">
                    <li>
                        <a href="{!! url('nlp_katadasars') !!}"><span class="fa fa-flask"></span>NLP Kata Dasar</a>
                    </li>
                     <li>
                       <a href="{!! url('nlp_pertanyaanusers') !!}"><span class="fa fa-flask"></span>Pertanyaan User</a>
                    </li>
               
                    <li>
                       <a href="{!! url('nlp_stemquestions') !!}"><span class="fa fa-flask"></span>NLP Stem Questions</a>
                    </li>
                    <li>
                       <a href="{!! url('nlp_stopwords') !!}"><span class="fa fa-flask"></span>NLP Stop Words</a>
                    </li>
                  </ul>
                </div>
            </div>
      </div>

        @endif

    </div>
</div>
@endif
