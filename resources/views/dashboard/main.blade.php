@extends('dashboard')

@section('content')

@if (Auth::user())
<style type="text/css">

.square, .btn {
border-radius: 0px!important;
}

.white {
color: #fff!important;
}

div.user-menu-container .thumbnail {
width:100%;
min-height:200px;
border: 0px!important;
padding: 0px;
border-radius: 0;
border: 0px!important;
}

/* -- Custom classes for the snippet, won't effect any existing bootstrap classes of your site, but can be reused. -- */

.user-pad {
padding: 20px 25px;
}

.no-pad {
padding-right: 0;
padding-left: 0;
padding-bottom: 0;
}

.user-details {
background: #eee;
min-height: 333px;
}

.user-image {
max-height:200px;
overflow:hidden;
}

.overview h3 {
font-weight: 300;
margin-top: 15px;
margin: 10px 0 0 0;
}

.overview h4 {
font-weight: bold!important;
font-size: 40px;
margin-top: 0;
color: #aaa;
}

/* -- media query for user profile image -- */
@media (max-width: 767px) {
.user-image {
    max-height: 400px;
}
}
/* Button dashboard */
.btn-sq-lg {
  width: 130px !important;
  height: 130px !important;
  margin:4px;
  border-radius: 10% !important;
}

</style>

<div class="container">
    <div class="row user-menu-container">
        <div class="col-md-7 user-details" style="border-radius:0 0 12px 12px !important">
            <div class="row bg-primary white">
                <div class="col-md-3 user-pad">
                    <div class="user-image">
                        <img src="{{ url($user->meta->avatar->url('small')) }}" class="img-thumbnail img-circle">
                    </div>
                </div>
                <div class="col-md-9 no-pad">
                    <div class="user-pad">
                        <h3 style="margin-top:0px; font-size:14px;">Selamat Datang Kembali, {{ Auth::user()->name }}</h3>
                        <h4 class="white"><i class="fa fa-envelope"></i> {{ $user['email'] }}</h4>
                        <h4 class="white"><i class="fa fa-phone"></i> {{ $user['meta']['phone'] }}</h4>
                        <a class="btn btn-default" href="{!! url('user/settings') !!}">Edit Profile</a>
                    </div>
                </div>
            </div>
              <div class="row overview">
                <div class="col-md-4 user-pad text-center">
                <a href="{!! url('tanaman') !!}"class="btn btn-sq-lg btn-primary">
                  <i class="fa fa-tree fa-5x"></i><br/>
                   Manajemen<br> Tanaman
              </a>
              </div>
              <div class="col-md-4 user-pad text-center">
                   <a href="{!! url('admingrafik') !!}" class="btn btn-sq-lg btn-info">
                 <i class="fa fa-bar-chart fa-5x"></i><br/>
                Grafik<br> Data Sensor
                 </a>
               </div>
               <div class="col-md-4 user-pad text-center">
                    <a href="{!! url('manajemenpohonbicara') !!}"class="btn btn-sq-lg btn-danger">
                  <i class="fa fa-laptop fa-5x"></i><br/>
                  Manajemen <br>Pohon Bicara
              </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

@stop
