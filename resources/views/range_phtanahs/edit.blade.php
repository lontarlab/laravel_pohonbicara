@extends('dashboard')

@section('content')
<div class="row">
            <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
                 <div class="pull-right">
                  </div>
            <h1 class="pull-left"><span class="fa fa-database"></span> Edit Kriteria PH Tanah </h1>
            </div>
        </div>

<br><div class="row">
  <div class="col-md-10 raw-margin-bottom-24 raw-margin-left-24">

    {!! Form::model($range_phtanah, ['route' => ['range_phtanahs.update', $range_phtanah->id], 'method' => 'patch']) !!}

   @if($range_phtanah->rendah == 0)
          @input_maker_create('rendah', ['type' => 'hidden'], $range_phtanah)
    @else
      <div class="raw-margin-top-24">
          @input_maker_label('Kriteria Rendah')
          @input_maker_create('rendah', ['type' => 'string'], $range_phtanah)
      </div>
    @endif
    <div class="raw-margin-top-24">
          @input_maker_label('Kriteria Sedang')
          @input_maker_create('sedang', ['type' => 'string'], $range_phtanah)
      </div>
    @if($range_phtanah->tinggi == 0)
          @input_maker_create('tinggi', ['type' => 'hidden'], $range_phtanah)
    @else
       <div class="raw-margin-top-24">
          @input_maker_label('Kriteria Tinggi')
          @input_maker_create('tinggi', ['type' => 'string'], $range_phtanah)
      </div>
    @endif
       <br>
    {!! Form::submit('Update') !!}

    {!! Form::close() !!}
</div>
</div>
@stop