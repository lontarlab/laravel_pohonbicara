@extends('dashboard')

@section('content')
<div class="row">
            <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
                 <div class="pull-right">
                 <form id="" class="pull-right raw-margin-top-24 raw-margin-left-24" method="post" action="/perangkats/search">
                    {!! csrf_field() !!}
                     <input class="form-control form-inline pull-right" name="search" placeholder="Search">
                 {!! Form::close() !!}
                 <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('perangkats.create') !!}">Tambah</a>
                  </div>
            <h1 class="pull-left"><span class="fa fa-server"></span> Kelola Perangkat</h1>
            </div>
        </div>

     <div class="row">
        @if($perangkats->isEmpty())
            <div class="well text-center">No Perangakat found.</div>
        @else
    <div class="row raw-margin-top-24">
        <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                    <th>Nama platform</th>
                    <th>Header Origin</th>
                    <th>Keterangan</th>
                    <th width="150px">Action</th>
                </thead>
                <tbody>
                @foreach($perangkats as $perangkat)
                    <tr>
                        <td>{{ $perangkat->platform }}</td>
                        <td>{{ $perangkat->origin }}</td>
                        <td>{{ $perangkat->keterangan }}</td>
                        <td>
                        
                        <a style="margin:1px;" class="btn btn-warning btn-xs pull-right raw-margin-right-16" href="{!! route('perangkats.edit', [$perangkat->id]) !!}"><span class="fa fa-edit"></span> Ubah</a>
                        <form method="post" action="{!! route('perangkats.destroy', [$perangkat->id]) !!}">
                        {!! csrf_field() !!}
                        {!! method_field('DELETE') !!}
                        <button style="margin:1px;" class="btn btn-danger btn-xs pull-right" type="submit" onclick="return confirm('Yakin untuk menghapus ?')"><i class="fa fa-trash"></i> Hapus</button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="row">
              
            </div>
        @endif
    </div>
</div>
@stop

