@extends('dashboard')

@section('content')
<div class="row">
            <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
                 <div class="pull-right"></div>
            <h1 class="pull-left"><span class="fa fa-server"></span> Kelola Perangkat</h1>
            </div>
        </div>

<div class="row">
  <div class="col-md-6 raw-margin-bottom-24 raw-margin-left-24">
  	  {!! Form::open(['route' => 'perangkats.store', 'files' => true]) !!}
              {!! csrf_field() !!}

			<input type="hidden" name="user_id"  class="form-control" value="{{Auth::user()->id}}" required>
    		 <div class="raw-margin-top-24">
                  @input_maker_label('platform')
                  @input_maker_create('platform', ['type' => 'text'])
              </div>
              <div class="raw-margin-top-24">
                  @input_maker_label('origin')
                  @input_maker_create('origin', ['type' => 'text'])
              </div>
              <div class="raw-margin-top-24">
                  @input_maker_label('content')
                  @input_maker_create('content', ['type' => 'text'])
              </div>
              <div class="raw-margin-top-24">
                  @input_maker_label('keterangan')
                  @input_maker_create('keterangan', ['type' => 'text'])
              </div>
             
              <div class="raw-margin-top-24">
                  <a class="btn btn-default pull-left" href="{!! route('perangkats.index') !!}">Cancel</a>
                  <button class="btn btn-primary pull-right" type="submit">Create</button>
              </div>
<br/></br/><br/></br/><br/></br/>
          {!! Form::close() !!}
      </div>


    </div>
</div>

</div>
@stop
