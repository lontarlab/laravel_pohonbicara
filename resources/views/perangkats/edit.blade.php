@extends('dashboard')

@section('content')
<div class="row">
            <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
                 <div class="pull-right"></div>
            <h1 class="pull-left"><span class="fa fa-server"></span> Kelola Perangkat</h1>
            </div>
        </div>
<div class="row">
  <div class="col-md-6 raw-margin-bottom-24 raw-margin-left-24">

    {!! Form::model($perangkat, ['route' => ['perangkats.update', $perangkat->id], 'method' => 'patch', 'files' =>true]) !!}
     {!! csrf_field() !!}    {!! method_field('PATCH') !!}

               
                     @form_maker_object($perangkat,[
                    'platform'=>['alt_name'=>'platform'],
                    'origin'=>['alt_name'=>'origin'],
                    'content'=>['alt_name'=>'content'],
                    'keterangan'=>['alt_name'=>'keterangan'],
                    ])
           
        <div class="raw-margin-top-24">
                      <a class="btn btn-default pull-left" href="{!! route('perangkats.index') !!}">Cancel</a>
                      <button class="btn btn-primary pull-right" type="submit">Save</button>
                  </div>

            {!! Form::close() !!}
          </div>
      </div>
  </div>

@stop
