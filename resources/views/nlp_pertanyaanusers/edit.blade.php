@extends('dashboard')

@section('content')
<div class="row">
            <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
                 <div class="pull-right">
                  </div>
            <h1 class="pull-left"><span class="fa fa-tree"></span> Edit NLP Pertanyaan User</h1>
            </div>
        </div>

<div class="row">
  <div class="col-md-6 raw-margin-bottom-24 raw-margin-left-24">

	<div class="">
	    {{ Session::get('message') }}
	</div>

	<div class="container">

	    {!! Form::model($nlp_pertanyaanuser, ['route' => ['nlp_pertanyaanusers.update', $nlp_pertanyaanuser->id], 'method' => 'patch']) !!}

	    @form_maker_object($nlp_pertanyaanuser, FormMaker::getTableColumns('nlp_pertanyaanusers'))

	    {!! Form::submit('Update') !!}

	    {!! Form::close() !!}
	</div>
	</div>
</div>

@stop
