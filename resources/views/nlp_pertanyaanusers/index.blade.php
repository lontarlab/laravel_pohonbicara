@extends('dashboard')
@section('content')
<div class="container">

    <div class="">
        {{ Session::get('message') }}
    </div>

    <div class="row">
        <div class="" >
            {!! Form::open(['route' => 'nlp_pertanyaanusers.search', 'class' => 'pull-right raw-margin-top-24 raw-margin-left-24']) !!}
            <input class="form-control form-inline pull-right" name="search" placeholder="Search">
            {!! Form::close() !!}
        </div>
       
        <h1 class="pull-left">Nlp Pertanyaan User</h1>
        <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('nlp_pertanyaanusers.create') !!}">Add New</a>
    </div>

    <div class="row">
        @if($nlp_pertanyaanusers->isEmpty())
            <div class="well text-center">No nlpPertanyaanusers found.</div>
        @else
            <table class="table">
                <thead>
                    <th>Pertanyaan</th>
                    <th>Jawaban</th>
                    <th>Action</th>
                </thead>
                <tbody>
                @foreach($nlp_pertanyaanusers as $nlp_pertanyaanuser)
                    <tr>
                        <td>
                            <a href="{!! route('nlp_pertanyaanusers.edit', [$nlp_pertanyaanuser->id]) !!}">{{ $nlp_pertanyaanuser->pertanyaan }}</a>
                        </td>
                        <td>
                            <a>{{ $nlp_pertanyaanuser->jawaban }}</a>
                        </td>
                       
                        <td>
                            <a style="margin:1px;" class="btn btn-warning btn-xs pull-right raw-margin-right-16" href="{!! route('nlp_pertanyaanusers.edit', [$nlp_pertanyaanuser->id]) !!}"><span class="fa fa-pencil"></span> Ubah</a>
                             <form method="post" action="{!! route('nlp_pertanyaanusers.destroy', [$nlp_pertanyaanuser->id]) !!}">
                                {!! csrf_field() !!}
                                {!! method_field('DELETE') !!}
                            <button style="margin:1px;" class="btn btn-danger btn-xs pull-right" type="submit" onclick="return confirm('Are you sure you want to delete this nlp_pertanyaanuser?')"><i class="fa fa-trash"></i> Hapus</button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="row">
                {!! $nlp_pertanyaanusers; !!}
            </div>
        @endif
    </div>
</div>
@stop
@section('javascript')
<script type="text/javascript">
    $(document).ready( function () {
    $('.display').DataTable();
    } );
</script>
@stop