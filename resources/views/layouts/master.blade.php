<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">

        <title>Pohon Bicara</title>

        <link rel="icon" type="image/ico" href="">

        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ url('css/font-awesome.min.css') }}" type="text/css">

        <!-- Local -->
        <link rel="stylesheet" href="{{ url('css/raw.min.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ url('css/app.css') }}" type="text/css">
        <link rel="stylesheet" href="{{ url('css/map_canvas.css') }}" type="text/css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">


        @yield('stylesheets')
    </head>
    <body>

        @include("layouts.navigation")

        <div class="app-wrapper container-fluid raw-margin-top-50 row">
            @yield("app-content")
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @include('partials.errors')
                    @include('partials.message')
                </div>
            </div>
        </div>

        <div class="pull-left raw100 navbar navbar-fixed-bottom">
            <div class="pull-left footer">
                <p class="raw-margin-left-20">Smart Urban Agliculture &copy; {!! date('Y'); !!} All rights reserved.</p>
            </div>
        </div>

        <script type="text/javascript">
            var _token = '{!! Session::token() !!}';
            var _url = '{!! url("/") !!}';
        </script>
        @yield("pre-javascript")
        <script src="{{ url('js/jquery-3.1.1.min.js') }}"></script>
        <script src="{{ url('js/bootstrap.min.js') }}"></script>
        <script src="{{ url('js/all.js') }}"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf8" src=" https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
        <script src="//cdn.datatables.net/plug-ins/1.10.11/sorting/date-eu.js" type="text/javascript"></script>
        @yield("javascript") 
    </body>
</html>
