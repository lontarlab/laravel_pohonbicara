@extends('dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
            <form id="" class="pull-right raw-margin-top-24 raw-margin-left-24" method="post" action="/admin/roles/search">
            {!! csrf_field() !!}
              <input class="form-control form-inline pull-right" name="search" placeholder="Search">
            {!! Form::close() !!}
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! url('admin/roles/create') !!}">Tambah Baru</a>
            <h1 class="pull-left"><span class="fa fa-lock"></span> Roles / Akses</h1>
        </div>
    </div>

    <div class="row raw-margin-top-24">
        <div class="col-md-12">
            <table class="table table-striped">

                <thead>
                    <th>Name</th>
                    <th>Label</th>
                    <th width="350px" class="text-right">Aksi</th>
                </thead>
                <tbody>
                    @foreach($roles as $role)
                        <tr>
                            <td>{{ $role->name }}</td>
                            <td>{{ $role->label }}</td>
                            <td>
                                <a style="margin:1px;" class="btn btn-warning btn-xs pull-right" href="{!! url('admin/roles/'.$role->id.'/edit') !!}"><i class="fa fa-pencil"></i> Ubah</a>
                                <form method="post" action="{!! url('admin/roles/'.$role->id) !!}">
                                    {!! csrf_field() !!}
                                    {!! method_field('DELETE') !!}
                                    <button style="margin:1px;" class="btn btn-danger btn-xs pull-right" type="submit" onclick="return confirm('Are you sure you want to delete this role?')"><i class="fa fa-trash"></i> Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach

                </tbody>

            </table>
        </div>
    </div>

@stop
