@extends('dashboard')

@section('content')
<div class="row">
            <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
                 <div class="pull-right">
                  </div>
            <h1 class="pull-left"><span class="fa fa-database"></span> Edit Kriteria Cahaya </h1>
            </div>
        </div>

<br><div class="row">
  <div class="col-md-10 raw-margin-bottom-24 raw-margin-left-24">
    {!! Form::model($range_cahaya, ['route' => ['range_cahayas.update', $range_cahaya->id], 'method' => 'patch']) !!}

    @form_maker_object($range_cahaya, FormMaker::getTableColumns('range_cahayas'))

    {!! Form::submit('Update') !!}

    {!! Form::close() !!}
  </div>
</div>
@stop