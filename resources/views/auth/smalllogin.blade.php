<div class="row">
        <form method="POST" action="{!! url('login') !!}">
            {!! csrf_field() !!}
            <div class="col-md-12 raw-margin-top-24">
                <label>Email</label>
                <input class="form-control" type="email" name="email" value="{{ old('email') }}">
            </div>
            <div class="col-md-12 raw-margin-top-24">
                <label>Password</label>
                <input class="form-control" type="password" name="password" id="password">
            </div>
            <div class="col-md-12 raw-margin-top-24">
                <label>
                    Remember Me <input type="checkbox" name="remember">
                </label>
            </div>
            <div class="col-md-6">
              <br /> <button class="btn btn-primary" type="submit">Login</button>
                    <button class="btn btn-info" href="{!! url('/register') !!}">Register</button>
            </div>
            
            <div class="col-md-6">
                <br /> <button class="btn btn-primary pull-right" href="{!! url('password/reset') !!}">Forgot Password</button>
            </div>
        </form>
</div>
