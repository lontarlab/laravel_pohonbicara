@extends('dashboard')
@section('content')
<div class="container">

    <div class="">
        {{ Session::get('message') }}
    </div>

    <div class="row">
        <div class="" >
            {!! Form::open(['route' => 'nlp_katadasars.search', 'class' => 'pull-right raw-margin-top-24 raw-margin-left-24']) !!}
            <input class="form-control form-inline pull-right" name="search" placeholder="Search">
            {!! Form::close() !!}
        </div>
       
        <h1 class="pull-left">Nlp Kata dasar</h1>
        <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('nlp_katadasars.create') !!}">Add New</a>
    </div>

    <div class="row">
        @if($nlp_katadasars->isEmpty())
            <div class="well text-center">Kata Dasar tidak ditemukan</div>
        @else
            <table class="table">
                <thead>
                    <th>Kata Dasar</th>
                    <th>Tipe Kata Dasar</th>
                    <th>Action</th>
                </thead>
                <tbody>
                @foreach($nlp_katadasars as $nlp_katadasar)
                    <tr>
                        <td>
                            <a href="{!! route('nlp_katadasars.edit', [$nlp_katadasar->id]) !!}">{{ $nlp_katadasar->katadasar }}</a>
                        </td>
                        <td>
                            <a>{{ $nlp_katadasar->tipe_katadasar }}</a>
                        </td>
                        <!-- <td>
                            <a href="{!! route('nlp_katadasars.edit', [$nlp_katadasar->id]) !!}"><i class="fa fa-pencil"></i> Edit</a>
                            <form method="post" action="{!! route('nlp_katadasars.destroy', [$nlp_katadasar->id]) !!}">
                                {!! csrf_field() !!}
                                {!! method_field('DELETE') !!}
                                <button type="submit" onclick="return confirm('Are you sure you want to delete this nlp_katadasar?')"><i class="fa fa-trash"></i> Delete</button>
                            </form>
                        </td> -->
                        <td>
                            <a style="margin:1px;" class="btn btn-warning btn-xs pull-right raw-margin-right-16" href="{!! route('nlp_katadasars.edit', [$nlp_katadasar->id]) !!}"><span class="fa fa-pencil"></span> Ubah</a>
                             <form method="post" action="{!! route('nlp_katadasars.destroy', [$nlp_katadasar->id]) !!}">
                                {!! csrf_field() !!}
                                {!! method_field('DELETE') !!}
                            <button style="margin:1px;" class="btn btn-danger btn-xs pull-right" type="submit" onclick="return confirm('Are you sure you want to delete this nlp_katadasar?')"><i class="fa fa-trash"></i> Hapus</button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="row">
                {!! $nlp_katadasars; !!}
            </div>
        @endif
    </div>
</div>
@stop
@section('javascript')
<script type="text/javascript">
    $(document).ready( function () {
    $('.display').DataTable();
    } );
</script>
@stop