@extends('dashboard')

@section('content')
<div class="row">
            <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
                 <div class="pull-right">
                  </div>
            <h1 class="pull-left"><span class="fa fa-database"></span> Edit Kriteria Suhu </h1>
            </div>
        </div>

<br><div class="row">
  <div class="col-md-10 raw-margin-bottom-24 raw-margin-left-24">
    {!! Form::model($range_kelembapan, ['route' => ['range_kelembapans.update', $range_kelembapan->id], 'method' => 'patch']) !!}

    @if($range_kelembapan->rendah == 0)
          @input_maker_create('rendah', ['type' => 'hidden'], $range_kelembapan)
    @else
      <div class="raw-margin-top-24">
          @input_maker_label('Kriteria Rendah')
          @input_maker_create('rendah', ['type' => 'string'], $range_kelembapan)
      </div>
    @endif
    <div class="raw-margin-top-24">
          @input_maker_label('Kriteria Sedang')
          @input_maker_create('sedang', ['type' => 'string'], $range_kelembapan)
      </div>
    @if($range_kelembapan->tinggi == 0)
          @input_maker_create('tinggi', ['type' => 'hidden'], $range_kelembapan)
    @else
       <div class="raw-margin-top-24">
          @input_maker_label('Kriteria Tinggi')
          @input_maker_create('tinggi', ['type' => 'string'], $range_kelembapan)
      </div>
    @endif
       <br>

    {!! Form::submit('Update') !!}

    {!! Form::close() !!}
</div>
@stop