@if ($user=Auth::user())
<div class="container sidebar-wrapper">
    <div class="toggle-wrapper">
        <button type="button" class="sidebar-toggle" data-toggle="collapse" data-target="#accordion">
            <span class="sr-only">Main Menu</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <div class="panel-group collapse" id="accordion">

      <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{!! url('dashboard') !!}"><span class="fa fa-home"></span> Beranda Pribadi</a>
            </div>
      </div>

      @if (in_array($user->roles->first()->name, ['admin','member']))
  

      <div class="panel panel-default">
            <div class="panel-heading">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapsecontent"><span class="fa fa-newspaper-o"></span> Manajemen Konten</a>
            </div>
            <div id="collapsecontent" class="panel-collapse collapse">
                <div class="panel-body">
                  <ul class="nav nav-sidebar">
                    <li>
                        <a href="{!! url('contents') !!}"><span class="fa fa-file-text"></span> Artikel</a>
                    </li>
                    <li>
                        <a href="{!! url('comments') !!}"><span class="fa fa-comment-o"></span> Komentar</a>
                    </li>
                    <li>
                        <a href="{!! url('categories') !!}"><span class="fa fa-paperclip"></span> Referensi - Kategori</a>
                    </li>
                    <li>
                        <a href="{!! url('topics') !!}"><span class="fa fa-quote-right"></span> Referensi - Topik</a>
                    </li>
                    <li>
                        <a href="{!! url('offlinewriters') !!}"><span class="fa fa-male"></span> Referensi - Offline Penulis</a>
                    </li>
                  </ul>
                </div>
            </div>
      </div>
      @endif

      @if (in_array($user->roles->first()->name, ['admin']))
      <div class="panel panel-default">
            <div class="panel-heading">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapsesystemadmin"><span class="fa fa-cloud"></span> Admin Sistem</a>
            </div>
            <div id="collapsesystemadmin" class="panel-collapse collapse">
                <div class="panel-body">
                <ul class="nav nav-sidebar">
                  <li>
                      <a href="{!! url('companies/1/edit') !!}"><span class="fa fa-building"></span> Pengaturan Umum</a>
                  </li>
                  <li>
                      <a href="{!! url('admin/users') !!}"><span class="fa fa-users"></span> Users</a>
                  </li>
                  <li>
                      <a href="{!! url('admin/roles') !!}"><span class="fa fa-lock"></span> Roles</a>
                  </li>
                  <li>
                      <a href="{!! url('logsystems') !!}"><span class="fa fa-database"></span> Log Sistem</a>
                  </li>
                </ul>
                </div>
            </div>
      </div>

      @endif

    </div>
</div>
@endif
