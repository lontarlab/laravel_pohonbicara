<?php

use App\Models\Content;
use Illuminate\Database\Seeder;

class ContentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Content::create([
          'name' => 'Welcome to JerbeeStarter',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Prepare your Laravel apps fast with additional commands, generators and templates from LaraCogs.',
          'user_id' => 1,
          'category_id' => 1,
      ]);
      Content::create([
          'name' => 'JerbeeStarter (2018)',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Prepare your Laravel apps fast with additional commands, generators and templates from LaraCogs.',
          'user_id' => 1,
          'category_id' => 1,
      ]);
      Content::create([
          'name' => 'Starter Kit',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Accelerate your app development with starter kit.',
          'user_id' => 1,
          'category_id' => 2,
      ]);
      Content::create([
          'name' => 'Solid Package',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Laravel 5.* extended with LaraCogs package.',
          'user_id' => 1,
          'category_id' => 2,
      ]);
      Content::create([
          'name' => 'Dashboard',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Simple default dashboard for your backend homepage.',
          'user_id' => 1,
          'category_id' => 2,
      ]);
      Content::create([
          'name' => 'Responsive',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'CRUD generator equipped with responsive layout.',
          'user_id' => 1,
          'category_id' => 2,
      ]);
      Content::create([
          'name' => 'Sample Portfolio 1',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Sample #1',
          'user_id' => 1,
          'category_id' => 3,
      ]);
      Content::create([
          'name' => 'Sample Portfolio 2',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Sample #2',
          'user_id' => 1,
          'category_id' => 3,
      ]);
      Content::create([
          'name' => 'Sample Portfolio 3',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Sample #3',
          'user_id' => 1,
          'category_id' => 3,
      ]);
      Content::create([
          'name' => 'Sample Portfolio 4',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Sample #4',
          'user_id' => 1,
          'category_id' => 3,
      ]);
      Content::create([
          'name' => 'Sample Portfolio 5',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Sample #5',
          'user_id' => 1,
          'category_id' => 3,
      ]);
      Content::create([
          'name' => 'Sample Portfolio 6',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Sample #6',
          'user_id' => 1,
          'category_id' => 3,
      ]);
    }
}
