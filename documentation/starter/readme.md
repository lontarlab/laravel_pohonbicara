## JerbeeStarter

JerbeeStarter is blank Laravel with additional (modified) files, developed as a starter kit for every internal Laravel  based web development project in PT Jerbee Indonesia.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
JerbeeStarter framework is also open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
