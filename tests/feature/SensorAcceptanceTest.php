<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class SensorAcceptanceTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->Sensor = factory(App\Models\Data\Sensor::class)->make([
            '=id_sensor' => '1',
		' tanggal' => '2018-06-08',
		' waktu' => '06:59:18',
		' ph_tanah' => 'tempora',
		'kelembapan' => 'ut',
		'suhu' => 'accusantium',
		'cahaya' => 'eaque',
		'id_tanaman' => '1',
		' id_user' => '1',

        ]);
        $this->SensorEdited = factory(App\Models\Data\Sensor::class)->make([
            '=id_sensor' => '1',
		' tanggal' => '2018-06-08',
		' waktu' => '06:59:18',
		' ph_tanah' => 'tempora',
		'kelembapan' => 'ut',
		'suhu' => 'accusantium',
		'cahaya' => 'eaque',
		'id_tanaman' => '1',
		' id_user' => '1',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'data/sensors');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('sensors');
    }

    public function testCreate()
    {
        $response = $this->actor->call('GET', 'data/sensors/create');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'data/sensors', $this->Sensor->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('data/sensors/'.$this->Sensor->id.'/edit');
    }

    public function testEdit()
    {
        $this->actor->call('POST', 'data/sensors', $this->Sensor->toArray());

        $response = $this->actor->call('GET', '/data/sensors/'.$this->Sensor->id.'/edit');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('sensor');
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'data/sensors', $this->Sensor->toArray());
        $response = $this->actor->call('PATCH', 'data/sensors/1', $this->SensorEdited->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertDatabaseHas('data_sensors', $this->SensorEdited->toArray());
        $this->assertRedirectedTo('/');
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'data/sensors', $this->Sensor->toArray());

        $response = $this->call('DELETE', 'data/sensors/'.$this->Sensor->id);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('data/sensors');
    }

}
