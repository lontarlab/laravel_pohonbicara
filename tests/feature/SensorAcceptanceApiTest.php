<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class SensorAcceptanceApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->Sensor = factory(App\Models\Data\Sensor::class)->make([
            '=id_sensor' => '1',
		' tanggal' => '2018-06-08',
		' waktu' => '06:59:18',
		' ph_tanah' => 'recusandae',
		'kelembapan' => 'quibusdam',
		'suhu' => 'ea',
		'cahaya' => 'odit',
		'id_tanaman' => '1',
		' id_user' => '1',

        ]);
        $this->SensorEdited = factory(App\Models\Data\Sensor::class)->make([
            '=id_sensor' => '1',
		' tanggal' => '2018-06-08',
		' waktu' => '06:59:18',
		' ph_tanah' => 'recusandae',
		'kelembapan' => 'quibusdam',
		'suhu' => 'ea',
		'cahaya' => 'odit',
		'id_tanaman' => '1',
		' id_user' => '1',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'api/v1/data/sensors');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'api/v1/data/sensors', $this->Sensor->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['id' => 1]);
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'api/v1/data/sensors', $this->Sensor->toArray());
        $response = $this->actor->call('PATCH', 'api/v1/data/sensors/1', $this->SensorEdited->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertDatabaseHas('data_sensors', $this->SensorEdited->toArray());
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'api/v1/data/sensors', $this->Sensor->toArray());
        $response = $this->call('DELETE', 'api/v1/data/sensors/'.$this->Sensor->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['success' => 'sensor was deleted']);
    }

}
