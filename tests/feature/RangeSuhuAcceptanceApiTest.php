<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RangeSuhuAcceptanceApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->RangeSuhu = factory(App\Models\RangeSuhu::class)->make([
            'id' => '1',
		'rendah' => '1',
		'sedang' => '1',
		'tinggi' => '1',
		'keterangan' => 'ut',

        ]);
        $this->RangeSuhuEdited = factory(App\Models\RangeSuhu::class)->make([
            'id' => '1',
		'rendah' => '1',
		'sedang' => '1',
		'tinggi' => '1',
		'keterangan' => 'ut',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'api/v1/range_suhus');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'api/v1/range_suhus', $this->RangeSuhu->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['id' => 1]);
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'api/v1/range_suhus', $this->RangeSuhu->toArray());
        $response = $this->actor->call('PATCH', 'api/v1/range_suhus/1', $this->RangeSuhuEdited->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertDatabaseHas('range_suhus', $this->RangeSuhuEdited->toArray());
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'api/v1/range_suhus', $this->RangeSuhu->toArray());
        $response = $this->call('DELETE', 'api/v1/range_suhus/'.$this->RangeSuhu->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['success' => 'range_suhu was deleted']);
    }

}
