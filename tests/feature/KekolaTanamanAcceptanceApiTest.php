<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class KekolaTanamanAcceptanceApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->KekolaTanaman = factory(App\Models\KekolaTanaman::class)->make([
            'id' => '1',
		'id_user' => '1',
		'jenis_tanaman' => 'nulla',
		'nama_tanaman' => 'porro',
		'longitute' => 'velit',
		'latitude' => 'quia',

        ]);
        $this->KekolaTanamanEdited = factory(App\Models\KekolaTanaman::class)->make([
            'id' => '1',
		'id_user' => '1',
		'jenis_tanaman' => 'nulla',
		'nama_tanaman' => 'porro',
		'longitute' => 'velit',
		'latitude' => 'quia',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'api/v1/kekola_tanamen');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'api/v1/kekola_tanamen', $this->KekolaTanaman->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['id' => 1]);
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'api/v1/kekola_tanamen', $this->KekolaTanaman->toArray());
        $response = $this->actor->call('PATCH', 'api/v1/kekola_tanamen/1', $this->KekolaTanamanEdited->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertDatabaseHas('kekola_tanamen', $this->KekolaTanamanEdited->toArray());
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'api/v1/kekola_tanamen', $this->KekolaTanaman->toArray());
        $response = $this->call('DELETE', 'api/v1/kekola_tanamen/'.$this->KekolaTanaman->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['success' => 'kekola_tanaman was deleted']);
    }

}
