<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RangePhtanahAcceptanceApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->RangePhtanah = factory(App\Models\RangePhtanah::class)->make([
            'id' => '1',
		'rendah' => '1',
		'sedang' => '1',
		'tinggi' => '1',
		'keterangan' => 'ea',

        ]);
        $this->RangePhtanahEdited = factory(App\Models\RangePhtanah::class)->make([
            'id' => '1',
		'rendah' => '1',
		'sedang' => '1',
		'tinggi' => '1',
		'keterangan' => 'ea',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'api/v1/range_phtanahs');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'api/v1/range_phtanahs', $this->RangePhtanah->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['id' => 1]);
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'api/v1/range_phtanahs', $this->RangePhtanah->toArray());
        $response = $this->actor->call('PATCH', 'api/v1/range_phtanahs/1', $this->RangePhtanahEdited->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertDatabaseHas('range_phtanahs', $this->RangePhtanahEdited->toArray());
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'api/v1/range_phtanahs', $this->RangePhtanah->toArray());
        $response = $this->call('DELETE', 'api/v1/range_phtanahs/'.$this->RangePhtanah->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['success' => 'range_phtanah was deleted']);
    }

}
