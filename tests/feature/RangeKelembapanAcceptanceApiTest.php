<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RangeKelembapanAcceptanceApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->RangeKelembapan = factory(App\Models\RangeKelembapan::class)->make([
            'id' => '1',
		'rendah' => '1',
		'sedang' => '1',
		'tinggi' => '1',
		'keterangan' => 'similique',

        ]);
        $this->RangeKelembapanEdited = factory(App\Models\RangeKelembapan::class)->make([
            'id' => '1',
		'rendah' => '1',
		'sedang' => '1',
		'tinggi' => '1',
		'keterangan' => 'similique',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'api/v1/range_kelembapans');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'api/v1/range_kelembapans', $this->RangeKelembapan->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['id' => 1]);
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'api/v1/range_kelembapans', $this->RangeKelembapan->toArray());
        $response = $this->actor->call('PATCH', 'api/v1/range_kelembapans/1', $this->RangeKelembapanEdited->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertDatabaseHas('range_kelembapans', $this->RangeKelembapanEdited->toArray());
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'api/v1/range_kelembapans', $this->RangeKelembapan->toArray());
        $response = $this->call('DELETE', 'api/v1/range_kelembapans/'.$this->RangeKelembapan->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['success' => 'range_kelembapan was deleted']);
    }

}
