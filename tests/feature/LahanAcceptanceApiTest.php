<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LahanAcceptanceApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->Lahan = factory(App\Models\Kekola\Lahan::class)->make([
            'id' => '1',
		'id_user' => '1',
		'jenis_tanaman' => 'in',
		'nama_tanaman' => 'rem',
		'longitute' => 'earum',
		'latitude' => 'omnis',

        ]);
        $this->LahanEdited = factory(App\Models\Kekola\Lahan::class)->make([
            'id' => '1',
		'id_user' => '1',
		'jenis_tanaman' => 'in',
		'nama_tanaman' => 'rem',
		'longitute' => 'earum',
		'latitude' => 'omnis',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'api/v1/kekola/lahans');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'api/v1/kekola/lahans', $this->Lahan->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['id' => 1]);
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'api/v1/kekola/lahans', $this->Lahan->toArray());
        $response = $this->actor->call('PATCH', 'api/v1/kekola/lahans/1', $this->LahanEdited->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertDatabaseHas('kekola_lahans', $this->LahanEdited->toArray());
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'api/v1/kekola/lahans', $this->Lahan->toArray());
        $response = $this->call('DELETE', 'api/v1/kekola/lahans/'.$this->Lahan->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['success' => 'lahan was deleted']);
    }

}
