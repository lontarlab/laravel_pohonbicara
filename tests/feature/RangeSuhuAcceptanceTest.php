<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RangeSuhuAcceptanceTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->RangeSuhu = factory(App\Models\RangeSuhu::class)->make([
            'id' => '1',
		'rendah' => '1',
		'sedang' => '1',
		'tinggi' => '1',
		'keterangan' => 'qui',

        ]);
        $this->RangeSuhuEdited = factory(App\Models\RangeSuhu::class)->make([
            'id' => '1',
		'rendah' => '1',
		'sedang' => '1',
		'tinggi' => '1',
		'keterangan' => 'qui',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'range_suhus');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('range_suhus');
    }

    public function testCreate()
    {
        $response = $this->actor->call('GET', 'range_suhus/create');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'range_suhus', $this->RangeSuhu->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('range_suhus/'.$this->RangeSuhu->id.'/edit');
    }

    public function testEdit()
    {
        $this->actor->call('POST', 'range_suhus', $this->RangeSuhu->toArray());

        $response = $this->actor->call('GET', '/range_suhus/'.$this->RangeSuhu->id.'/edit');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('range_suhu');
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'range_suhus', $this->RangeSuhu->toArray());
        $response = $this->actor->call('PATCH', 'range_suhus/1', $this->RangeSuhuEdited->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertDatabaseHas('range_suhus', $this->RangeSuhuEdited->toArray());
        $this->assertRedirectedTo('/');
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'range_suhus', $this->RangeSuhu->toArray());

        $response = $this->call('DELETE', 'range_suhus/'.$this->RangeSuhu->id);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('range_suhus');
    }

}
