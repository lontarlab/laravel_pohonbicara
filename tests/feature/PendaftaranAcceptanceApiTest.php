<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class PendaftaranAcceptanceApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->Pendaftaran = factory(App\Models\Pendaftaran::class)->make([
            '=id' => '1',
		'name' => 'optio',
		' email' => 'qui',
		'alamat' => 'expedita',
		' phone' => 'modi',
		' gender' => 'placeat',
		' status' => 'nisi',

        ]);
        $this->PendaftaranEdited = factory(App\Models\Pendaftaran::class)->make([
            '=id' => '1',
		'name' => 'optio',
		' email' => 'qui',
		'alamat' => 'expedita',
		' phone' => 'modi',
		' gender' => 'placeat',
		' status' => 'nisi',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'api/v1/pendaftarans');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'api/v1/pendaftarans', $this->Pendaftaran->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['id' => 1]);
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'api/v1/pendaftarans', $this->Pendaftaran->toArray());
        $response = $this->actor->call('PATCH', 'api/v1/pendaftarans/1', $this->PendaftaranEdited->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertDatabaseHas('pendaftarans', $this->PendaftaranEdited->toArray());
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'api/v1/pendaftarans', $this->Pendaftaran->toArray());
        $response = $this->call('DELETE', 'api/v1/pendaftarans/'.$this->Pendaftaran->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['success' => 'pendaftaran was deleted']);
    }

}
