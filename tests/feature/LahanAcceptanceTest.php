<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LahanAcceptanceTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->Lahan = factory(App\Models\Kekola\Lahan::class)->make([
            'id' => '1',
		'id_user' => '1',
		'jenis_tanaman' => 'sit',
		'nama_tanaman' => 'rerum',
		'longitute' => 'quia',
		'latitude' => 'praesentium',

        ]);
        $this->LahanEdited = factory(App\Models\Kekola\Lahan::class)->make([
            'id' => '1',
		'id_user' => '1',
		'jenis_tanaman' => 'sit',
		'nama_tanaman' => 'rerum',
		'longitute' => 'quia',
		'latitude' => 'praesentium',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'kekola/lahans');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('lahans');
    }

    public function testCreate()
    {
        $response = $this->actor->call('GET', 'kekola/lahans/create');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'kekola/lahans', $this->Lahan->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('kekola/lahans/'.$this->Lahan->id.'/edit');
    }

    public function testEdit()
    {
        $this->actor->call('POST', 'kekola/lahans', $this->Lahan->toArray());

        $response = $this->actor->call('GET', '/kekola/lahans/'.$this->Lahan->id.'/edit');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('lahan');
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'kekola/lahans', $this->Lahan->toArray());
        $response = $this->actor->call('PATCH', 'kekola/lahans/1', $this->LahanEdited->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertDatabaseHas('kekola_lahans', $this->LahanEdited->toArray());
        $this->assertRedirectedTo('/');
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'kekola/lahans', $this->Lahan->toArray());

        $response = $this->call('DELETE', 'kekola/lahans/'.$this->Lahan->id);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('kekola/lahans');
    }

}
