<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class PerangkatAcceptanceApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->Perangkat = factory(App\Models\Perangkat::class)->make([
            'id' => '1',
		'user_id' => '1',
		'platform' => 'fuga fuga accusamus occaecati',
		'origin' => 'aut nemo consectetur architecto',
		'content' => 'rerum ab ex libero',
		'keterangan' => 'sint',

        ]);
        $this->PerangkatEdited = factory(App\Models\Perangkat::class)->make([
            'id' => '1',
		'user_id' => '1',
		'platform' => 'fuga fuga accusamus occaecati',
		'origin' => 'aut nemo consectetur architecto',
		'content' => 'rerum ab ex libero',
		'keterangan' => 'sint',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'api/v1/perangkats');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'api/v1/perangkats', $this->Perangkat->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['id' => 1]);
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'api/v1/perangkats', $this->Perangkat->toArray());
        $response = $this->actor->call('PATCH', 'api/v1/perangkats/1', $this->PerangkatEdited->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertDatabaseHas('perangkats', $this->PerangkatEdited->toArray());
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'api/v1/perangkats', $this->Perangkat->toArray());
        $response = $this->call('DELETE', 'api/v1/perangkats/'.$this->Perangkat->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['success' => 'perangkat was deleted']);
    }

}
