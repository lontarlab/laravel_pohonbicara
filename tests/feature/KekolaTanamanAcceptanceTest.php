<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class KekolaTanamanAcceptanceTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->KekolaTanaman = factory(App\Models\KekolaTanaman::class)->make([
            'id' => '1',
		'id_user' => '1',
		'jenis_tanaman' => 'aut',
		'nama_tanaman' => 'veritatis',
		'longitute' => 'consequuntur',
		'latitude' => 'incidunt',

        ]);
        $this->KekolaTanamanEdited = factory(App\Models\KekolaTanaman::class)->make([
            'id' => '1',
		'id_user' => '1',
		'jenis_tanaman' => 'aut',
		'nama_tanaman' => 'veritatis',
		'longitute' => 'consequuntur',
		'latitude' => 'incidunt',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'kekola_tanamen');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('kekola_tanamen');
    }

    public function testCreate()
    {
        $response = $this->actor->call('GET', 'kekola_tanamen/create');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'kekola_tanamen', $this->KekolaTanaman->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('kekola_tanamen/'.$this->KekolaTanaman->id.'/edit');
    }

    public function testEdit()
    {
        $this->actor->call('POST', 'kekola_tanamen', $this->KekolaTanaman->toArray());

        $response = $this->actor->call('GET', '/kekola_tanamen/'.$this->KekolaTanaman->id.'/edit');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('kekola_tanaman');
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'kekola_tanamen', $this->KekolaTanaman->toArray());
        $response = $this->actor->call('PATCH', 'kekola_tanamen/1', $this->KekolaTanamanEdited->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertDatabaseHas('kekola_tanamen', $this->KekolaTanamanEdited->toArray());
        $this->assertRedirectedTo('/');
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'kekola_tanamen', $this->KekolaTanaman->toArray());

        $response = $this->call('DELETE', 'kekola_tanamen/'.$this->KekolaTanaman->id);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('kekola_tanamen');
    }

}
