<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class KelolaTanamanAcceptanceTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->KelolaTanaman = factory(App\Models\KelolaTanaman::class)->make([
            'id' => '1',
		'id_user' => '1',
		'jenis_tanaman' => 'cumque',
		'nama_tanaman' => 'quia',
		'longitute' => 'mollitia',
		'latitude' => 'vel',

        ]);
        $this->KelolaTanamanEdited = factory(App\Models\KelolaTanaman::class)->make([
            'id' => '1',
		'id_user' => '1',
		'jenis_tanaman' => 'cumque',
		'nama_tanaman' => 'quia',
		'longitute' => 'mollitia',
		'latitude' => 'vel',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'kelola_tanamen');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('kelola_tanamen');
    }

    public function testCreate()
    {
        $response = $this->actor->call('GET', 'kelola_tanamen/create');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'kelola_tanamen', $this->KelolaTanaman->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('kelola_tanamen/'.$this->KelolaTanaman->id.'/edit');
    }

    public function testEdit()
    {
        $this->actor->call('POST', 'kelola_tanamen', $this->KelolaTanaman->toArray());

        $response = $this->actor->call('GET', '/kelola_tanamen/'.$this->KelolaTanaman->id.'/edit');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('kelola_tanaman');
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'kelola_tanamen', $this->KelolaTanaman->toArray());
        $response = $this->actor->call('PATCH', 'kelola_tanamen/1', $this->KelolaTanamanEdited->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertDatabaseHas('kelola_tanamen', $this->KelolaTanamanEdited->toArray());
        $this->assertRedirectedTo('/');
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'kelola_tanamen', $this->KelolaTanaman->toArray());

        $response = $this->call('DELETE', 'kelola_tanamen/'.$this->KelolaTanaman->id);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('kelola_tanamen');
    }

}
