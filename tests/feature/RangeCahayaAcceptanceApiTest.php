<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RangeCahayaAcceptanceApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->RangeCahaya = factory(App\Models\RangeCahaya::class)->make([
            'id' => '1',
		'rendah' => '1',
		'sedang' => '1',
		'tinggi' => '1',
		'keterangan' => 'aperiam',

        ]);
        $this->RangeCahayaEdited = factory(App\Models\RangeCahaya::class)->make([
            'id' => '1',
		'rendah' => '1',
		'sedang' => '1',
		'tinggi' => '1',
		'keterangan' => 'aperiam',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'api/v1/range_cahayas');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'api/v1/range_cahayas', $this->RangeCahaya->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['id' => 1]);
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'api/v1/range_cahayas', $this->RangeCahaya->toArray());
        $response = $this->actor->call('PATCH', 'api/v1/range_cahayas/1', $this->RangeCahayaEdited->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertDatabaseHas('range_cahayas', $this->RangeCahayaEdited->toArray());
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'api/v1/range_cahayas', $this->RangeCahaya->toArray());
        $response = $this->call('DELETE', 'api/v1/range_cahayas/'.$this->RangeCahaya->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['success' => 'range_cahaya was deleted']);
    }

}
