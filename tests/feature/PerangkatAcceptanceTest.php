<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class PerangkatAcceptanceTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->Perangkat = factory(App\Models\Perangkat::class)->make([
            'id' => '1',
		'user_id' => '1',
		'platform' => 'animi delectus reiciendis voluptatem',
		'origin' => 'eius molestias doloremque dolorem',
		'content' => 'sequi rerum quas exercitationem',
		'keterangan' => 'quis',

        ]);
        $this->PerangkatEdited = factory(App\Models\Perangkat::class)->make([
            'id' => '1',
		'user_id' => '1',
		'platform' => 'animi delectus reiciendis voluptatem',
		'origin' => 'eius molestias doloremque dolorem',
		'content' => 'sequi rerum quas exercitationem',
		'keterangan' => 'quis',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'perangkats');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('perangkats');
    }

    public function testCreate()
    {
        $response = $this->actor->call('GET', 'perangkats/create');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'perangkats', $this->Perangkat->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('perangkats/'.$this->Perangkat->id.'/edit');
    }

    public function testEdit()
    {
        $this->actor->call('POST', 'perangkats', $this->Perangkat->toArray());

        $response = $this->actor->call('GET', '/perangkats/'.$this->Perangkat->id.'/edit');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('perangkat');
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'perangkats', $this->Perangkat->toArray());
        $response = $this->actor->call('PATCH', 'perangkats/1', $this->PerangkatEdited->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertDatabaseHas('perangkats', $this->PerangkatEdited->toArray());
        $this->assertRedirectedTo('/');
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'perangkats', $this->Perangkat->toArray());

        $response = $this->call('DELETE', 'perangkats/'.$this->Perangkat->id);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('perangkats');
    }

}
