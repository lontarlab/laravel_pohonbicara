<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RangeCahayaAcceptanceTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->RangeCahaya = factory(App\Models\RangeCahaya::class)->make([
            'id' => '1',
		'rendah' => '1',
		'sedang' => '1',
		'tinggi' => '1',
		'keterangan' => 'enim',

        ]);
        $this->RangeCahayaEdited = factory(App\Models\RangeCahaya::class)->make([
            'id' => '1',
		'rendah' => '1',
		'sedang' => '1',
		'tinggi' => '1',
		'keterangan' => 'enim',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'range_cahayas');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('range_cahayas');
    }

    public function testCreate()
    {
        $response = $this->actor->call('GET', 'range_cahayas/create');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'range_cahayas', $this->RangeCahaya->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('range_cahayas/'.$this->RangeCahaya->id.'/edit');
    }

    public function testEdit()
    {
        $this->actor->call('POST', 'range_cahayas', $this->RangeCahaya->toArray());

        $response = $this->actor->call('GET', '/range_cahayas/'.$this->RangeCahaya->id.'/edit');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('range_cahaya');
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'range_cahayas', $this->RangeCahaya->toArray());
        $response = $this->actor->call('PATCH', 'range_cahayas/1', $this->RangeCahayaEdited->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertDatabaseHas('range_cahayas', $this->RangeCahayaEdited->toArray());
        $this->assertRedirectedTo('/');
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'range_cahayas', $this->RangeCahaya->toArray());

        $response = $this->call('DELETE', 'range_cahayas/'.$this->RangeCahaya->id);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('range_cahayas');
    }

}
