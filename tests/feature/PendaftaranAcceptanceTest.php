<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class PendaftaranAcceptanceTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->Pendaftaran = factory(App\Models\Pendaftaran::class)->make([
            '=id' => '1',
		'name' => 'rerum',
		' email' => 'quia',
		'alamat' => 'tempore',
		' phone' => 'inventore',
		' gender' => 'quibusdam',
		' status' => 'quia',

        ]);
        $this->PendaftaranEdited = factory(App\Models\Pendaftaran::class)->make([
            '=id' => '1',
		'name' => 'rerum',
		' email' => 'quia',
		'alamat' => 'tempore',
		' phone' => 'inventore',
		' gender' => 'quibusdam',
		' status' => 'quia',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'pendaftarans');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('pendaftarans');
    }

    public function testCreate()
    {
        $response = $this->actor->call('GET', 'pendaftarans/create');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'pendaftarans', $this->Pendaftaran->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('pendaftarans/'.$this->Pendaftaran->id.'/edit');
    }

    public function testEdit()
    {
        $this->actor->call('POST', 'pendaftarans', $this->Pendaftaran->toArray());

        $response = $this->actor->call('GET', '/pendaftarans/'.$this->Pendaftaran->id.'/edit');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('pendaftaran');
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'pendaftarans', $this->Pendaftaran->toArray());
        $response = $this->actor->call('PATCH', 'pendaftarans/1', $this->PendaftaranEdited->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertDatabaseHas('pendaftarans', $this->PendaftaranEdited->toArray());
        $this->assertRedirectedTo('/');
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'pendaftarans', $this->Pendaftaran->toArray());

        $response = $this->call('DELETE', 'pendaftarans/'.$this->Pendaftaran->id);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('pendaftarans');
    }

}
