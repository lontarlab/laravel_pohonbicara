<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class KelolaTanamanAcceptanceApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->KelolaTanaman = factory(App\Models\KelolaTanaman::class)->make([
            'id' => '1',
		'id_user' => '1',
		'jenis_tanaman' => 'in',
		'nama_tanaman' => 'quo',
		'longitute' => 'laudantium',
		'latitude' => 'placeat',

        ]);
        $this->KelolaTanamanEdited = factory(App\Models\KelolaTanaman::class)->make([
            'id' => '1',
		'id_user' => '1',
		'jenis_tanaman' => 'in',
		'nama_tanaman' => 'quo',
		'longitute' => 'laudantium',
		'latitude' => 'placeat',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'api/v1/kelola_tanamen');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'api/v1/kelola_tanamen', $this->KelolaTanaman->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['id' => 1]);
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'api/v1/kelola_tanamen', $this->KelolaTanaman->toArray());
        $response = $this->actor->call('PATCH', 'api/v1/kelola_tanamen/1', $this->KelolaTanamanEdited->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertDatabaseHas('kelola_tanamen', $this->KelolaTanamanEdited->toArray());
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'api/v1/kelola_tanamen', $this->KelolaTanaman->toArray());
        $response = $this->call('DELETE', 'api/v1/kelola_tanamen/'.$this->KelolaTanaman->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['success' => 'kelola_tanaman was deleted']);
    }

}
