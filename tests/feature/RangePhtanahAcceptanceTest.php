<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RangePhtanahAcceptanceTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->RangePhtanah = factory(App\Models\RangePhtanah::class)->make([
            'id' => '1',
		'rendah' => '1',
		'sedang' => '1',
		'tinggi' => '1',
		'keterangan' => 'sint',

        ]);
        $this->RangePhtanahEdited = factory(App\Models\RangePhtanah::class)->make([
            'id' => '1',
		'rendah' => '1',
		'sedang' => '1',
		'tinggi' => '1',
		'keterangan' => 'sint',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'range_phtanahs');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('range_phtanahs');
    }

    public function testCreate()
    {
        $response = $this->actor->call('GET', 'range_phtanahs/create');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'range_phtanahs', $this->RangePhtanah->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('range_phtanahs/'.$this->RangePhtanah->id.'/edit');
    }

    public function testEdit()
    {
        $this->actor->call('POST', 'range_phtanahs', $this->RangePhtanah->toArray());

        $response = $this->actor->call('GET', '/range_phtanahs/'.$this->RangePhtanah->id.'/edit');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('range_phtanah');
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'range_phtanahs', $this->RangePhtanah->toArray());
        $response = $this->actor->call('PATCH', 'range_phtanahs/1', $this->RangePhtanahEdited->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertDatabaseHas('range_phtanahs', $this->RangePhtanahEdited->toArray());
        $this->assertRedirectedTo('/');
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'range_phtanahs', $this->RangePhtanah->toArray());

        $response = $this->call('DELETE', 'range_phtanahs/'.$this->RangePhtanah->id);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('range_phtanahs');
    }

}
