<?php

use Tests\TestCase;
use App\Services\SensorService;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class SensorServiceTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
        $this->service = $this->app->make(SensorService::class);
        $this->originalArray = [
            '=id_sensor' => '1',
		' tanggal' => '2018-06-08',
		' waktu' => '06:59:18',
		' ph_tanah' => 'expedita',
		'kelembapan' => 'voluptatum',
		'suhu' => 'magnam',
		'cahaya' => 'rerum',
		'id_tanaman' => '1',
		' id_user' => '1',

        ];
        $this->editedArray = [
            '=id_sensor' => '1',
		' tanggal' => '2018-06-08',
		' waktu' => '06:59:18',
		' ph_tanah' => 'expedita',
		'kelembapan' => 'voluptatum',
		'suhu' => 'magnam',
		'cahaya' => 'rerum',
		'id_tanaman' => '1',
		' id_user' => '1',

        ];
        $this->searchTerm = '';
    }

    public function testAll()
    {
        $response = $this->service->all();
        $this->assertEquals(get_class($response), 'Illuminate\Database\Eloquent\Collection');
        $this->assertTrue(is_array($response->toArray()));
        $this->assertEquals(0, count($response->toArray()));
    }

    public function testPaginated()
    {
        $response = $this->service->paginated(25);
        $this->assertEquals(get_class($response), 'Illuminate\Pagination\LengthAwarePaginator');
        $this->assertEquals(0, $response->total());
    }

    public function testSearch()
    {
        $response = $this->service->search($this->searchTerm, 25);
        $this->assertEquals(get_class($response), 'Illuminate\Pagination\LengthAwarePaginator');
        $this->assertEquals(0, $response->total());
    }

    public function testCreate()
    {
        $response = $this->service->create($this->originalArray);
        $this->assertEquals(get_class($response), 'App\Models\Data\Sensor');
        $this->assertEquals(1, $response->id);
    }

    public function testFind()
    {
        // create the item
        $item = $this->service->create($this->originalArray);

        $response = $this->service->find($item->id);
        $this->assertEquals($item->id, $response->id);
    }

    public function testUpdate()
    {
        // create the item
        $item = $this->service->create($this->originalArray);

        $response = $this->service->update($item->id, $this->editedArray);

        $this->assertDatabaseHas('data_sensors', $this->editedArray);
    }

    public function testDestroy()
    {
        // create the item
        $item = $this->service->create($this->originalArray);

        $response = $this->service->destroy($item->id);
        $this->assertTrue($response);
    }
}
